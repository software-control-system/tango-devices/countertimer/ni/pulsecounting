//=============================================================================
// PulseCountingTypesAndConsts.h
//=============================================================================
// abstraction.......PulseCounting 
// class.............PulseCountingTypesAndConsts
// original author.... S.Gara - NEXEYA
//=============================================================================

#ifndef _PULSECOUNTING_TYPES_AND_CONSTS_H_
#define _PULSECOUNTING_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include "tango.h"
#include <sstream>
#include <map>

#include <yat/utils/XString.h>
#include <NI660Xsl/OutputOperation.h>

#include <yat/memory/DataBuffer.h>
#include <yat/any/Any.h>
#include "NI6602_TypesAndConsts.h"
#include <yat/threading/Thread.h>

namespace PulseCounting_ns
{

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE CountingManager task
// ============================================================================
#define kSTART_MSG             (yat::FIRST_USER_MSG + 1001)
#define kSTOP_MSG              (yat::FIRST_USER_MSG + 1002)
#define kEND_OF_SEQ_MSG        (yat::FIRST_USER_MSG + 1003)
#define kTRIGGER_RECEIVED_MSG  (yat::FIRST_USER_MSG + 1004)
  

//-----------------------------------------------------------------------------
#define kPERIODIC_MSG_PERIOD_MS  100 // default periodic msg period
#define kLAST_BUFFER_DELAY_MS    100 // delay before reading last buffer (external clock / listener mode)


const int SINGLE_DATA = 1;
//- Allowed string delimiters for each property to be parsed
static std::string DELIMS = ":";

//- names for dynamic attributes
#define BUFFER_DEPTH "bufferDepth"
#define BUFFER_DEPTH_LABEL "intermediateBufferDepth"
#define SAMPLES_NUMBER "totalNbPoint"
#define SAMPLES_NUMBER_LABEL "samplesNumber"
#define PULSE_WIDTH_ENABLED "MinPulseWidthEnable"
#define PULSE_WIDTH "MinPulseWidth"
#define DATASET_ENABLED "enableDataset"
#define CURRENT_POINT "currentPoint"

//- boardType strings
#define BT_PXI_6602_STR "PXI-6602"

// ============================================================================
// COUNTER CONFIG - DATA TYPEs
// ============================================================================
//- List of <KEY, value> defining the acquisition configuration
typedef std::pair<const std::string, std::string> CounterKey_pair_t;
typedef std::map<std::string, std::string> CounterKeys_t;
typedef CounterKeys_t::iterator CounterKeys_it_t;

// base data type
typedef double data_t;

// buffer data type
typedef yat::Buffer<data_t> RawData_t;


//- counter string
#define COUNTER_STR "Counter"

//- acquisitionMode strings
#define ACQ_MODE_SCAL_STR "SCALAR"
#define ACQ_MODE_BUFF_STR "BUFFERED"
#define ACQ_MODE_UNDEF "UNKNOWN"

// clock type
#define CLOCK_INTERNAL "Internal"

// max value for timeout in sec
#define kTMO_MAX_SEC 2100

//- boardType
typedef enum
{
	BT_UNKNOWN = -1,
	BT_PXI_6602 = 0
} E_BoardType_t;

// ============================================================================
// Counter mode
// ============================================================================
typedef enum
{
	UNDEFINED_COUNTER_MODE  = 0x0,
	COUNTER_MODE_EVENT      = 0x1,
	COUNTER_MODE_POSITION   = 0x2,
  COUNTER_MODE_DELTATIME  = 0x3,
  COUNTER_MODE_PERIOD     = 0x4
} E_CounterMode_t;

// ============================================================================
// Counting edge
// ============================================================================
typedef enum
{
	UNDEFINED_COUNTING_EDGE  = 0x0,
	COUNTING_EDGE_RISING     = 0x1,
	COUNTING_EDGE_FALLING    = 0x2
} E_CountingEdge_t;

// ============================================================================
// Memory transfer mode
// ============================================================================
typedef enum
{
	UNDEFINED_MEMORY_TRSF  = 0x0,
	MEMORY_TRSF_DMA        = 0x1, // DMA
	MEMORY_TRSF_ITR        = 0x2  // Interruption
} E_MemoryTransfer_t;

// ============================================================================
// Counting direction
// ============================================================================
typedef enum
{
	UNDEFINED_CNT_DIRECTION  = 0x0,
	CNT_DIRECTION_UP         = 0x1, // Increase counter
	CNT_DIRECTION_DOWN       = 0x2, // Decrease counter
	CNT_DIRECTION_EXT        = 0x3  // Direction set by external signal
} E_CountingDirection_t;

// ============================================================================
// Decoding mode (for position counter)
// ============================================================================
typedef enum
{
	UNDEFINED_DECODING_MODE  = 0x0,
	DEC_MODE_X1              = 0x1, 
	DEC_MODE_X2              = 0x2, 
	DEC_MODE_X4              = 0x3,
	DEC_MODE_TWO_PULSE       = 0x4
} E_DecodingMode_t;

// ============================================================================
// Encoder unit (for position counter & dt counter)
// ============================================================================
typedef enum
{
	UNDEFINED_ENCODER_UNIT   = 0x0,
	ENC_UNIT_METER           = 0x1, 
	ENC_UNIT_INCH            = 0x2, 
	ENC_UNIT_DEGREE          = 0x3,
	ENC_UNIT_RADIAN          = 0x4,
	ENC_UNIT_TICK            = 0x5,
  ENC_UNIT_SEC             = 0x6 
} E_EncoderUnit_t;

// ============================================================================
// Zindex phase (for position counter)
// ============================================================================
typedef enum
{
	UNDEFINED_ZINDEX_PHASE  = 0x0,
	ZINDEX_PHASE_AH_BH      = 0x1, // A HIGH B HIGH
	ZINDEX_PHASE_AH_BL      = 0x2, // A HIGH B LOW
	ZINDEX_PHASE_AL_BH      = 0x3, // A LOW B HIGH
	ZINDEX_PHASE_AL_BL      = 0x4  // A LOW B LOW
} E_ZIndexPhase_t;

// ============================================================================
// Encoder mode (for position counter)
// ============================================================================
typedef enum
{
	UNDEFINED_ENCODER_MODE  = 0x0,
	ENC_MODE_LINEAR         = 0x1,
	ENC_MODE_ANGULAR        = 0x2
} E_EncoderMode_t;

// ============================================================================
// Clock type
// ============================================================================
typedef enum
{
	UNDEFINED_CLOCK_TYPE  = 0x0,
	CLK_TPE_FIN         = 0x1,
	CLK_TPE_CTN         = 0x2
} E_ClockType_t;

// ============================================================================
// Acquisition mode
// ============================================================================
typedef enum
{
	UNDEFINED_ACQ_MODE  = 0x0,
	ACQ_MODE_SCAL         = 0x1,
	ACQ_MODE_BUFF         = 0x2
} E_AcquisitionMode_t;

// ============================================================================
// Counter definition
// ============================================================================
typedef struct CounterConfig
{
	//- members --------------------
	// Counter number in board (0 --> 7)
	unsigned int number;

  // Counter number in Device interface (0 --> N)
  // Unique number in Device
  unsigned int ctNumber;

	// Counter name (unique)
	std::string name;

	// Position encoder unit str
	std::string format;

	// Board name
	std::string boardName;

	// Counter mode
	E_CounterMode_t mode;

	// Counting edge
	E_CountingEdge_t edge;

  // 2nd counting edge (for delta time counter)
  E_CountingEdge_t scdEdge;

	// Memory transfer type (for buffered mode)
	E_MemoryTransfer_t memTranfer;

	// Counting direction
	E_CountingDirection_t direction;

	// External Device attribute for initialization (format : xx/yy/zz/attribute)
	std::string initProxy;

  // Absolute initial position
  double absInitPos;

  // Apply initial position to position computation (or not)
  bool applyInitPos;

	// Position decoding mode
	E_DecodingMode_t pos_decodingMode;

	// Position zindex enabled
	bool pos_ZindexEnabled;

	// Position Zindex phase
	E_ZIndexPhase_t pos_ZindexPhase;

	// Position Zindex value
	yat::uint32 pos_ZindexValue;

	// Position encoder mode
	E_EncoderMode_t pos_encoderMode;

	// Position encoder unit
	E_EncoderUnit_t pos_encoderUnit;
  
  // Position encoder unit str
  std::string pos_unitStr;

	// Position encoder distance per pulse (for linear mouvement)
	double pos_distPerPulse;

	// Position encoder pulse per revolution (for angular mouvement)
	yat::uint32 pos_pulsePerRevolution;

	// Min Pulse Width Enabled
	bool minPulseWidthEnabled;

	// Min Pulse Width
	double minPulseWidth; 

	// Delta time counter unit
	E_EncoderUnit_t dt_unit;
  
  // Delta time counter unit str
  std::string dt_unitStr;

	// nexus storage enabled
	bool nexus;

  // dataset (for nexus) name
  std::string dataset;
 

	//- default constructor -----------------------
	CounterConfig ()
		: number(0),
    ctNumber(0),
		name(""),
		format(""),
		boardName(""),
		mode(UNDEFINED_COUNTER_MODE),
		edge(UNDEFINED_COUNTING_EDGE),
    scdEdge(UNDEFINED_COUNTING_EDGE),
		memTranfer(UNDEFINED_MEMORY_TRSF),
		direction(UNDEFINED_CNT_DIRECTION),
		initProxy(""),
    absInitPos(0.0),
    applyInitPos(false),
		pos_decodingMode(UNDEFINED_DECODING_MODE),
		pos_ZindexEnabled(false),
		pos_ZindexPhase(UNDEFINED_ZINDEX_PHASE),
		pos_ZindexValue(0),
		pos_encoderMode(UNDEFINED_ENCODER_MODE),
		pos_encoderUnit(UNDEFINED_ENCODER_UNIT),
    pos_unitStr(" "),
		pos_distPerPulse(yat::IEEE_NAN),
		pos_pulsePerRevolution(0),
		minPulseWidthEnabled(false),
		minPulseWidth(0.0),
    dt_unit(UNDEFINED_ENCODER_UNIT),
    dt_unitStr(""),
		nexus(false),
    dataset("")
	{
	}

	//- destructor -----------------------
	~CounterConfig ()
	{
	}

	//- copy constructor ------------------
	CounterConfig (const CounterConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const CounterConfig & operator= (const CounterConfig& src)
	{
		if (this == & src) 
			return *this;

		number = src.number;
    ctNumber = src.ctNumber;
		name = src.name;
		format = src.format;
		boardName = src.boardName;
		direction = src.direction;
		edge = src.edge;
    scdEdge = src.scdEdge;
		initProxy = src.initProxy;
    absInitPos = src.absInitPos;
    applyInitPos = src.applyInitPos;
		memTranfer = src.memTranfer;
		mode = src.mode;
		pos_decodingMode = src.pos_decodingMode;
		pos_distPerPulse = src.pos_distPerPulse;
		pos_encoderMode = src.pos_encoderMode;
		pos_encoderUnit = src.pos_encoderUnit;
    pos_unitStr = src.pos_unitStr;
		pos_pulsePerRevolution = src.pos_pulsePerRevolution;
		pos_ZindexEnabled = src.pos_ZindexEnabled;
		pos_ZindexPhase = src.pos_ZindexPhase;
		pos_ZindexValue = src.pos_ZindexValue;
		minPulseWidthEnabled = src.minPulseWidthEnabled;
		minPulseWidth = src.minPulseWidth;
    dt_unit = src.dt_unit;
    dt_unitStr = src.dt_unitStr;
		nexus = src.nexus;
    dataset = src.dataset;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout << "CounterConfig::number........." 
			<< number
			<< std::endl; 
		std::cout << "CounterConfig::ctNumber........." 
			<< ctNumber
			<< std::endl; 
		std::cout << "CounterConfig::name........." 
			<< name
			<< std::endl; 
		std::cout << "CounterConfig::boardName........." 
			<< boardName
			<< std::endl; 
		std::cout << "CounterConfig::mode........." 
			<< mode
			<< std::endl; 
		std::cout << "CounterConfig::edge........." 
			<< edge
			<< std::endl; 
		std::cout << "CounterConfig::scdEdge........." 
			<< scdEdge
			<< std::endl; 
		std::cout << "CounterConfig::memTranfer........." 
			<< memTranfer
			<< std::endl; 
		std::cout << "CounterConfig::direction........." 
			<< direction
			<< std::endl; 
		std::cout << "CounterConfig::initProxy........." 
			<< initProxy
			<< std::endl; 
		std::cout << "CounterConfig::absInitPos........." 
			<< absInitPos
			<< std::endl; 
		std::cout << "CounterConfig::applyInitPos........." 
			<< applyInitPos
			<< std::endl;
		std::cout << "CounterConfig::pos_decodingMode........." 
			<< pos_decodingMode
			<< std::endl; 
		std::cout << "CounterConfig::pos_ZindexEnabled........." 
			<< pos_ZindexEnabled
			<< std::endl; 
		std::cout << "CounterConfig::pos_ZindexPhase........." 
			<< pos_ZindexPhase
			<< std::endl; 
		std::cout << "CounterConfig::pos_ZindexValue........." 
			<< pos_ZindexValue
			<< std::endl; 
		std::cout << "CounterConfig::pos_encoderUnit........." 
			<< pos_encoderUnit
			<< std::endl; 
		std::cout << "CounterConfig::pos_unitStr........." 
			<< pos_unitStr
			<< std::endl; 
		std::cout << "CounterConfig::pos_distPerPulse........." 
			<< pos_distPerPulse
			<< std::endl; 
		std::cout << "CounterConfig::pos_pulsePerRevolution........." 
			<< pos_pulsePerRevolution
			<< std::endl;  
		std::cout << "CounterConfig::minPulseWidthEnabled........." 
			<< minPulseWidthEnabled
			<< std::endl; 
		std::cout << "CounterConfig::minPulseWidth........." 
			<< minPulseWidth
			<< std::endl;  
		std::cout << "CounterConfig::dt_unit........." 
			<< dt_unit
			<< std::endl; 
		std::cout << "CounterConfig::dt_unitStr........." 
			<< dt_unitStr
			<< std::endl; 
		std::cout << "CounterConfig::nexus........." 
			<< nexus
			<< std::endl; 
		std::cout << "CounterConfig::dataset........." 
			<< dataset
			<< std::endl; 
		std::cout << "CounterConfig::format........." 
			<< format
			<< std::endl; 
	}

} CounterConfig;

// CounterConfigMap
// key = counter ID in device (0 -> N), value = counter's config
typedef std::map<size_t ,CounterConfig> counterConfigMap_t;

// ============================================================================
// Acquisition definition
// ============================================================================
typedef struct AcquisitionDefinition
{
	//- members --------------------
	// Enable or disable Nexus file generation
	bool nexusFileGeneration;

	// Nexus target path
	std::string nexusTargetPath;

	// Nexus file name
	std::string nexusFileName;

	// flyscan pool
	std::string flyscanSpool;

	// Number of acquisitions pushed in a single Nexus file
  // If measure dim = 0, number of acquisitions = number of scalar values
  // If measure dim = 1, number of acquisitions = number of buffers
	unsigned short nexusNbPerFile;  

  // Nexus measure dimansion (0D or 1D)
  unsigned short nexusMeasDim;

	// acquisition mode
	E_AcquisitionMode_t acqMode; 

	// continuous acquisition
	bool continuousAcquisition;

	// integration time (in s)
	double integrationTime;  

  // frequency in Hz (= 1 / it)
  double frequency;

	// timeout
	double timeout;  

	// Number of samples (whole acquisition buffer)
	long samplesNumber;   

	// buffer depth in number of points (intermediate buffer depth)
	long bufferDepth;

	// start trigger use
	bool startTriggerUse;

	//- timebase scaling
	string timebaseScaling;

  //- polling period (ms)
  double pollingPeriod;


	//- default constructor -----------------------
	AcquisitionDefinition ()
		: nexusFileGeneration(false),
		nexusTargetPath(""),
		nexusFileName(""),
		flyscanSpool(""),
		nexusNbPerFile(100),
    nexusMeasDim(0),
		acqMode(ACQ_MODE_SCAL),
		continuousAcquisition(false),
		integrationTime(1.0),
		frequency(1.0),
		timeout(3),
		samplesNumber(1),
		bufferDepth(1),
		startTriggerUse(true),
		timebaseScaling(""),
    pollingPeriod(kPERIODIC_MSG_PERIOD_MS)
	{
	}

	//- destructor -----------------------
	~AcquisitionDefinition ()
	{
	}

	//- copy constructor ------------------
	AcquisitionDefinition (const AcquisitionDefinition& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const AcquisitionDefinition & operator= (const AcquisitionDefinition& src)
	{
		if (this == & src) 
			return *this;

		nexusFileGeneration = src.nexusFileGeneration;
		nexusTargetPath = src.nexusTargetPath;
		nexusFileName = src.nexusFileName;
		nexusNbPerFile = src.nexusNbPerFile;
    nexusMeasDim = src.nexusMeasDim;
		flyscanSpool = src.flyscanSpool;
		acqMode = src.acqMode;
		continuousAcquisition = src.continuousAcquisition;
		integrationTime = src.integrationTime;
		frequency = src.frequency;
		timeout = src.timeout;
		samplesNumber = src.samplesNumber;
		bufferDepth = src.bufferDepth;
		startTriggerUse = src.startTriggerUse;
		timebaseScaling = src.timebaseScaling;
    pollingPeriod = src.pollingPeriod;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout << "AcquisitionDefinition::nexusFileGeneration........." 
			<< nexusFileGeneration
			<< std::endl; 
		std::cout << "AcquisitionDefinition::nexusTargetPath........." 
			<< nexusTargetPath
			<< std::endl; 
		std::cout << "AcquisitionDefinition::nexusFileName........." 
			<< nexusFileName
			<< std::endl; 
		std::cout << "AcquisitionDefinition::flyscanSpool........." 
			<< flyscanSpool
			<< std::endl; 
		std::cout << "AcquisitionDefinition::nexusNbPerFile........." 
			<< nexusNbPerFile
			<< std::endl;
		std::cout << "AcquisitionDefinition::nexusMeasDim........." 
			<< nexusMeasDim
			<< std::endl;
		std::cout << "AcquisitionDefinition::acqMode........." 
			<< acqMode
			<< std::endl;
		std::cout << "AcquisitionDefinition::continuousAcquisition........." 
			<< continuousAcquisition
			<< std::endl;
		std::cout << "AcquisitionDefinition::integrationTime........." 
			<< integrationTime
			<< std::endl;
		std::cout << "AcquisitionDefinition::frequency........." 
			<< frequency
			<< std::endl;
		std::cout << "AcquisitionDefinition::timeout........." 
			<< nexusNbPerFile
			<< std::endl;
		std::cout << "AcquisitionDefinition::samplesNumber........." 
			<< samplesNumber
			<< std::endl;
		std::cout << "AcquisitionDefinition::bufferDepth........." 
			<< bufferDepth
			<< std::endl;
    std::cout << "AcquisitionDefinition::startTriggerUse........." 
			<< startTriggerUse
			<< std::endl;
		std::cout << "AcquisitionDefinition::timebaseScaling........." 
			<< timebaseScaling
			<< std::endl;
		std::cout << "AcquisitionDefinition::pollingPeriod........." 
			<< pollingPeriod
			<< std::endl;
	}

} AcquisitionDefinition;

// ============================================================================
// Buffered counter definition
// ============================================================================
typedef struct BCEconfig
{
	//- members --------------------
	// Acquisition definition
	AcquisitionDefinition acq;

	// Counter definition
	CounterConfig cnt;

	//- the TANGO host device (for logging)
	Tango::DeviceImpl * hostDevice;

	//- default constructor -----------------------
	BCEconfig ()
	{
	}

	//- destructor -----------------------
	~BCEconfig ()
	{
	}

	//- copy constructor ------------------
	BCEconfig (const BCEconfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const BCEconfig & operator= (const BCEconfig& src)
	{
		if (this == & src) 
			return *this;

		acq = src.acq;
		cnt = src.cnt;
		hostDevice = src.hostDevice;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout << "BCEconfig::acquisition........." 
			<< std::endl; 
		acq.dump();

		std::cout << "BCEconfig::counter........." 
			<< std::endl; 
		cnt.dump();

		std::cout << "BCEconfig::hostDevice........." 
			<< hostDevice << endl;
	}

} BCEconfig;


// ============================================================================
// ClockConfig:  struct containing the clock configuration
// ============================================================================
typedef struct ClockConfig
{
	//- members
	//- Clock type
	E_ClockType_t clockType;

	//- Board name
	string	boardName;

	//- Channel name
	string	channelName;

	//- frequency
	double frequency;

	//- low duration ticks
	unsigned int lowDurationTicks;

	//- high duration ticks
	unsigned int highDurationTicks;

	//- timebase scaling
	string	timebaseScaling;


	//- default constructor -----------------------
	ClockConfig ()
		: clockType(UNDEFINED_CLOCK_TYPE),
		  boardName(""),
		  channelName(""),
		  frequency(0.0),
		  lowDurationTicks(1),
		  highDurationTicks(1),
		  timebaseScaling("")
	{
	}

	//- destructor -----------------------
	~ClockConfig ()
	{
	}

	//- copy constructor ------------------
	ClockConfig (const ClockConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const ClockConfig & operator= (const ClockConfig& src)
	{
		if (this == & src) 
			return *this;

		clockType = src.clockType;
		boardName = src.boardName;
		channelName = src.channelName;
		frequency = src.frequency;
		lowDurationTicks = src.lowDurationTicks;
		highDurationTicks = src.highDurationTicks;
		timebaseScaling = src.timebaseScaling;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout <<  "ClockConfig::clockType........." 
			<< clockType
			<< std::endl;
		std::cout <<  "ClockConfig::boardName........." 
			<< boardName
			<< std::endl;
		std::cout <<  "ClockConfig::channelName........." 
			<< channelName
			<< std::endl;
		std::cout <<  "ClockConfig::frequency........." 
			<< frequency
			<< std::endl;
		std::cout <<  "ClockConfig::lowDurationTicks........." 
			<< lowDurationTicks
			<< std::endl;
		std::cout <<  "ClockConfig::highDurationTicks........." 
			<< highDurationTicks
			<< std::endl;
		std::cout <<  "ClockConfig::timebaseScaling........." 
			<< timebaseScaling
			<< std::endl;
	}

} ClockConfig;

// ============================================================================
// BoardArchitecture:  struct containing the device configuration
// ============================================================================
typedef struct BoardArchitecture
{
	//- members
	//- the TANGO host device (for logging)
	Tango::DeviceImpl * hostDevice;

	//- clock generation
	bool clockGeneration;

	//- board type
	E_BoardType_t boardType;

  //- Board max number of channels
  unsigned int chanNb;

	//- board list
	std::vector<std::string> boardList;

	//- CounterConfigMap
	counterConfigMap_t counterConfiguration;

  //- trigger listener option parameters:
  //- address
  std::string udp_address;

  //- port
  yat::uint32 udp_port;


	//- default constructor -----------------------
	BoardArchitecture ()
		: hostDevice(NULL),
		clockGeneration(false),
		boardType(BT_UNKNOWN),
		chanNb(0),
    udp_address(""),
    udp_port(0)

	{
          boardList.clear();
          counterConfiguration.clear();
	}

	//- destructor -----------------------
	~BoardArchitecture ()
	{
		boardList.clear();
		counterConfiguration.clear();
	}

	//- copy constructor ------------------
	BoardArchitecture (const BoardArchitecture& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const BoardArchitecture & operator= (const BoardArchitecture& src)
	{
		if (this == & src) 
			return *this;

		hostDevice = src.hostDevice;
		clockGeneration = src.clockGeneration;
		boardType = src.boardType;
		chanNb = src.chanNb;
		boardList = src.boardList;
		counterConfiguration = src.counterConfiguration;
    udp_address = src.udp_address;
    udp_port = src.udp_port;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout <<  "BoardArchitecture::clockGeneration........." 
			<< clockGeneration
			<< std::endl;
		std::cout <<  "BoardArchitecture::boardType........." 
			<< boardType
			<< std::endl;
		std::cout <<  "BoardArchitecture::chanNb........." 
			<< chanNb
			<< std::endl;
		std::cout <<  "BoardArchitecture::udp_address........." 
			<< udp_address
			<< std::endl;
		std::cout <<  "BoardArchitecture::udp_port........." 
			<< udp_port
			<< std::endl;
	}

} BoardArchitecture;

//- split_property_line
static std::vector<std::string> split_property_line(std::string line)
{
	//std::cout << "\n\t\t LINE TO PARSE \"" << line << "\"" << std::endl;
	//- string iterators
	std::string::size_type begIdx, endIdx;
	std::string tmp("");
	//- result : the splitted line 
	std::vector<std::string> propertyList;
	//- search beginning of the first word
	begIdx = line.find_first_not_of(DELIMS);

	while ( begIdx != std::string::npos )
	{
		//- search the end of the actual word
		endIdx = line.find_first_of(DELIMS, begIdx);
		if ( endIdx == std::string::npos)
		{
			tmp = line.substr(begIdx);
			//std::cout << "\t\t END LINE TO PARSE \"" << tmp << "\"" << std::endl;
			propertyList.push_back( tmp );
			break;
		}
		//- store the property found
		tmp = line.substr(begIdx, (endIdx-begIdx));
		//std::cout << "\t\t LINE PARSED \"" << tmp << "\"\n" << std::endl;
		propertyList.push_back( tmp );
		//- search beginning of the next word
		begIdx = line.find_first_not_of(DELIMS, endIdx);
	}
	return propertyList;
}
//   configuration example for a counter definition
static std::vector<std::string> define_config_counter_example()
{
	std::vector<std::string> config;
	config.push_back("Name:-- Counter name --");
	config.push_back("Mode:-- POS / EVT / DT / PERIOD --");
	config.push_back("Transfer:-- [ITR] / DMA (for buffered) -- optionnal --");
	config.push_back("EdgeType:-- Falling / Rising --");
  config.push_back("2ndEdgeType:-- DTonly --> Falling / Rising --");
	config.push_back("Direction:-- UP / DOWN / EXTERNAL --");
	config.push_back("Decoding:-- X1 / X2 / X4 / TWO_PULSE --");
	config.push_back("Zindex:-- Enabled / Disabled --");
	config.push_back("ZindexPhase:-- AHighBHigh / AHighBLow / ALowBHigh / ALowBLow --");
	config.push_back("ZindexVal:-- 0 --");
	config.push_back("EncoderMode:-- Linear / Angular --");
	config.push_back("Units:-- LinearOnly -> Meters / Inches / Ticks // AngularOnly -> Degrees / Radians / Ticks // DTonly -> Ticks / Seconds --");
	config.push_back("DistancePerPulse:-- LinearOnly -> 0 --");
	config.push_back("PulsePerRev:-- AngularOnly -> 0 --");
	config.push_back("Proxy:xx/yy/zz/attr [] -- Optional, for position counter --");
  config.push_back("Nexus:true / [false] -- Optional --");
	return config;
}

//- get stored value in Device property
 template <class T>
static T get_value_as_property(Tango::DeviceImpl* dev_p, const std::string& property_name)
  {
    if (!Tango::Util::instance()->_UseDb)
    {
		  THROW_DEVFAILED(
        "DEVICE_ERROR", 
			  "NO DB", 
			  "PulseCounting::get_value_as_property"); 
    } 

    T value;
	  Tango::DbData	dev_prop;
	  dev_prop.push_back(Tango::DbDatum(property_name));

	  //-	Call database and extract values
	  //--------------------------------------------
    try
    {
      dev_p->get_db_device()->get_property(dev_prop);
    }
    catch (Tango::DevFailed &df)
    {
		  RETHROW_DEVFAILED(
        df,
        "DEVICE_ERROR", 
			  (std::string(df.errors[0].desc).c_str()), 
			  "PulseCounting::get_value_as_property"); 
    }

    //-	Try to extract saved property from database
	  if (dev_prop[0].is_empty()==false)
	  {
		  dev_prop[0] >> value;
    }
    else
    {
      // no value stored in data base
		  THROW_DEVFAILED(
        "DEVICE_ERROR", 
			  "No value in database", 
			  "PulseCounting::get_value_as_property"); 
    }

    return value;
  }

} // namespace PulseCounting_ns

#endif // _PULSEGCOUNTING_TYPES_AND_CONSTS_H_