//=============================================================================
// PulseCountingManager.h
//=============================================================================
// abstraction.......PulseCountingManager for PulseCounting
// class.............PulseCountingManager
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _PULSE_COUNTING_MANAGER_H
#define _PULSE_COUNTING_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/time/Timer.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DynamicInterfaceManager.h>
#include <yat4tango/PropertyHelper.h>
#include "NexusManager.h"
#include "PulseCountingTypesAndConsts.h"
#include "CountingBoardInterface.h"
#include "ClockGenerator.h"
#include "GenericCounterInterface.h"

namespace PulseCounting_ns
{
// ============================================================================
// class: PulseCountingManager
// ============================================================================
class PulseCountingManager :  public yat4tango::DeviceTask
{

public:

  //- constructor
  PulseCountingManager (Tango::DeviceImpl * hostDevice);

  //- destructor
  virtual ~PulseCountingManager ();

  //- gets PulseCounting manager state
  Tango::DevState get_state();

  //- gets PulseCounting manager status
  std::string get_status ();

  //- init
  virtual void init(BoardArchitecture p_board_arch);

  //- starts the pulse generation
  void start();

  //- stop the pulse generation
  void stop();

  //- get driver version
  std::string get_driver_version();

	//- get data streams
	std::vector<std::string> get_data_streams();

  //- Sets nexus dataset name for specified channel number
  void change_nx_dataset(yat::uint16 chan_nb, std::string dataset);

  // Get all channel dataset information as a map:<channel id, info string>
  /* NAME:{channel label}
     ENABLED:{channel enabled true / false}
     DATASET_NAME:{name of the dataset}
     DATASET_ENABLED:{true / false}
     DATASET_ATTR:{name of the 'dataset enabled' TANGO attribute}
  */
  std::map<yat::uint16, std::string> get_channel_info();

	//- set acquisition mode
	void set_acquisition_mode(E_AcquisitionMode_t p_acq_mode);

  //- Sets acquisition parameters
  void set_acquisition_parameters (AcquisitionDefinition p_param);

	//- reset buffer index
	void reset_nexus_buffer_index();
	  
	//- set continuous mode
	void set_countinuous_mode(bool p_continuous);

  //- set integration time
	void set_integration_time(double p_time);

	//- Sets new nexus file path
	void set_nexus_file_path (std::string path);

	//- Sets new nexus file generation flag
	void set_nexus_file_generation (bool enable);

	//- Sets new nexus nb acq per file
	void set_nexus_nb_acq_per_file (unsigned int p_nb);

  //- Checks current acquisition configuration
  virtual std::string check_configuration(bool excpt) = 0;

  // clean dynamic interface
  void clean_dyn_attr();

  // configure clock (if needed)
  void configure_clock();

protected:

  //- add dynamic attributes
	void add_attributes(CounterConfig p_cfg);

  void add_common_attributes();

	//- read callback for attr counterPulseWidthEnabled
	void read_counter_pulse_width_enabled(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr counterPulseWidthEnabled
	void write_counter_pulse_width_enabled(yat4tango::DynamicAttributeWriteCallbackData & cbd);

	//- read callback for attr counterPulseWidth
	void read_counter_pulse_width(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr counterPulseWidth
	void write_counter_pulse_width(yat4tango::DynamicAttributeWriteCallbackData & cbd);

	//- read callback for attr enableDatasetcounter
	void read_counter_dataset_enabled(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr enableDatasetcounter
	void write_counter_dataset_enabled(yat4tango::DynamicAttributeWriteCallbackData & cbd);

	//- read callback for attr integrationTime
	void read_integration_time(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr integrationTime
	void write_integration_time(yat4tango::DynamicAttributeWriteCallbackData & cbd);

	//- read callback for attr frequency
	void read_frequency(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr frequency
	void write_frequency(yat4tango::DynamicAttributeWriteCallbackData & cbd);

	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) = 0;

  //- Build Nexus item list
	void build_nexus_item_list();

  //- Configure trigger listener (for external clock)
  //- use address & port specified in parameters
  // default implementation: do nothing.
  virtual void configure_trigger_listener(std::string addr, yat::uint32 port){} 

  //- dynamic attributes manager
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;

  //- Nexus manager
  NexusManager * m_nexus_manager;

  //- clock generator
  ClockGenerator * m_clock_generator;

  //- parameters for nexus
  AcquisitionDefinition m_acq_param;

  //- counting board interface
  CountingBoardInterface * m_counting_board;

  //- nexus items list
  nxItemList_t m_nx_items;

  // map for counters
  //- key = counter ID in device (0 -> N), value = pointer on counter interface
  GenericCounterMap_t m_counterList;

  //- key = pointer on counter interface, value = CounterConfig
  std::map<GenericCounterInterface *, CounterConfig> m_counterListConfig;

  //- board architecture parameters
  BoardArchitecture m_board_arch;

  //- Requested stop flag (if true => user sends stop command or fatal error during acquisition)
  bool m_requestedStop;

  //- 1st start flag (if true => user sends start command) 
  bool m_firstStart;

  // acquisition flag
  bool m_acq_running;

  //- flags mutex protection
  yat::Mutex m_flagLock;

  //- hardware init needed flag (for clock optimization)
  bool m_isHwInitNeeded;

  //- integration time stored value (for clock optimization)
  double m_stored_it;

};

} // namespace PulseCounting_ns

#endif // _PULSE_COUNTING_MANAGER_H
