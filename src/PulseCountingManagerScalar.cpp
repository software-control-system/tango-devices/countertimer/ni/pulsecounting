//=============================================================================
// PulseCountingManagerScalar.cpp
//=============================================================================
// abstraction.......PulseCountingManagerScalar for PulseCounting
// class.............PulseCountingManagerScalar
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingManagerScalar.h"

#define kEND_OF_CLOCK_MSG     (yat::FIRST_USER_MSG + 1003)

namespace PulseCounting_ns
{

	//- Check nexus manager macro:
#define CHECK_NX_MANAGER() \
	if (! m_nexus_manager) \
	{ \
	m_status = "Internal error."; \
	THROW_DEVFAILED("INTERNAL_ERROR", \
	"request aborted - the Nexus manager isn't properly initialized", \
	"PulseCountingManagerScalar::check_nx_manager"); \
} while (0)

	//- check clock generator macro:
#define CHECK_CLK_GEN() \
	do \
	{ \
	if (! m_clock_generator) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the clock generator isn't accessible ", \
	"PulseCountingManagerScalar::check_clk_gen"); \
} while (0)

 //- check counting boerd macro:
#define CHECK_CNT_BOARD() \
	do \
	{ \
	if (! m_counting_board) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the counting board isn't accessible ", \
	"PulseCountingManagerScalar::check_counting_board"); \
} while (0)

//*****************************************************************************
// ECThread
//*****************************************************************************
// ============================================================================
// ECThread::ECThread
// ============================================================================
ECThread::ECThread (const ECConfig & cfg, yat::Thread::IOArg ioa)
	: yat::Thread(ioa),
	Tango::LogAdapter (cfg.hostDevice), 
	m_cfg (cfg),
	m_goOn(true)
{
	//- noop ctor
}

// ============================================================================
// ECThread::~ECThread
// ============================================================================
ECThread::~ECThread (void)
{
	//- noop dtor
}

// ============================================================================
// ECThread::run_undetached
// ============================================================================
yat::Thread::IOArg ECThread::run_undetached (yat::Thread::IOArg ioa)
{
	DEBUG_STREAM << "ECThread::run_undetached() entering... " << std::endl;

	//- get ref. to out our parent task
	PulseCountingManagerScalar * cm_task = reinterpret_cast<PulseCountingManagerScalar *>(ioa);

	DEBUG_STREAM << "Wait end of clock..." << std::endl;
	
  m_cfg.clockGen->waitEndOfClock(); // the driver waits for the clock to end...

	// when function returns, the clock is over 
	DEBUG_STREAM << "Clock has stopped!" << std::endl;

	// send a END of CLOCK message to the PulseCountingManagerScalar task only if "natural clock stop"

  // define message
  yat::Message * msg = new yat::Message(kEND_OF_CLOCK_MSG);
  if (! msg)
  {
	  ERROR_STREAM << "ECThread::run_undetached yat::Message allocation failed!" << std::endl; 
	  return 0;
  }

  //- post it to the task
  cm_task->post(msg);

	return 0;
}

// ============================================================================
// ECThread::exit
// ============================================================================
void ECThread::exit (void)
{
	m_goOn = false;
}

// ============================================================================
// PulseCountingManagerScalar::PulseCountingManagerScalar ()
// ============================================================================ 
PulseCountingManagerScalar::PulseCountingManagerScalar (Tango::DeviceImpl * hostDevice)
: PulseCountingManager(hostDevice)
{
	m_endOfClock_thread = NULL;
	m_acq_param.acqMode = ACQ_MODE_SCAL;
  m_countersVal.clear();
}

// ============================================================================
// PulseCountingManagerScalar::~PulseCountingManagerScalar ()
// ============================================================================ 
PulseCountingManagerScalar::~PulseCountingManagerScalar ()
{
	set_periodic_msg_period (0xFFFF);
	enable_periodic_msg (false);

    releaseObjects();

	if (m_endOfClock_thread)
	{
		delete m_endOfClock_thread;
		m_endOfClock_thread = NULL;
	}
}

// ============================================================================
// PulseCountingManagerScalar::init ()
// ============================================================================ 
void PulseCountingManagerScalar::init(BoardArchitecture p_board_arch) 
{
  // Call init from mother class
	PulseCountingManager::init(p_board_arch);

  // initialize local map for counters value storage
  m_countersVal.clear();
  GenericCounterMap_t::iterator l_it;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
    m_countersVal.insert(std::pair<unsigned int, data_t>(l_it->first, yat::IEEE_NAN));
  }

	try
	{
    // add SCALAR counter
		manage_specific_dyn_attr();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to add specific dynamic attributes", 
			"PulseCountingManagerScalar::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to add specific dynamic attributes" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to add specific dynamic attributes", 
			"PulseCountingManagerScalar::init"); 
	}
}

// ============================================================================
// PulseCountingManagerScalar::process_message
// ============================================================================
void PulseCountingManagerScalar::process_message (yat::Message& msg)
{
	//- handle msg
	switch (msg.type())
	{
		//- THREAD_INIT ----------------------
	case yat::TASK_INIT:
		{
			DEBUG_STREAM << "PulseCountingManagerScalar::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			init_i();
		} 
		break;

		//- THREAD_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "PulseCountingManagerScalar::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;

		//- THREAD_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			//DEBUG_STREAM << "PulseCountingManagerScalar::handle_message::THREAD_PERIODIC" << std::endl;
			periodic_job_i();
		}
		break;

		//- THREAD_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
			//- not used in this example
		}
		break;
		//- kSTART_MSG
	case kSTART_MSG:
		{
			start_i();
		}
		break;
		//- kSTOP_MSG
	case kSTOP_MSG:
		{
      {
        yat::AutoMutex<> guard(m_flagLock);
        m_requestedStop = true;
      }

      if (m_acq_running)
			  stop_i();

      // force state & status update
      periodic_job_i();
		}
		break;
		//- kEND_OF_CLOCK_MSG
	case kEND_OF_CLOCK_MSG:
		{
      // internal stop only if "natural end" (not requested)
      if (!m_requestedStop)
      {
			  stop_i();
      }

			// Delete "waiting end of clock" thread
			DEBUG_STREAM << "PulseCountingManagerScalar::process_message - Asking EC thread to quit" << std::endl;
			if (m_endOfClock_thread)
			{
				yat::Thread::IOArg ioa;
				m_endOfClock_thread->exit();
				m_endOfClock_thread->join(&ioa);
				DEBUG_STREAM << "PulseCountingManagerScalar::process_message - EC thread exited" << std::endl;
				m_endOfClock_thread = NULL; 
			}
			
      // force state & status update
      periodic_job_i();
		}
    break;
		//- UNHANDLED MSG --------------------
	default:
		DEBUG_STREAM << "PulseCountingManagerScalar::handle_message::unhandled msg type received" << std::endl;
		break;
	}
}

// ============================================================================
// PulseCountingManagerScalar::init_i ()
// ============================================================================ 
void PulseCountingManagerScalar::init_i()
{
  if (m_board_arch.clockGeneration)
  {
    // get memorized value for integrationTime if any
	  std::string prop_name = std::string("__integrationTime");
	  double val = 1.0; 
	  try
	  {
		  val = get_value_as_property<double>(m_board_arch.hostDevice, prop_name);
      set_integration_time(val);
	  }
	  catch(...)
	  {
		  // no value in db, use default value
		  set_integration_time(1.0);
	  }
    m_stored_it = val;
  }
  else
  {
    // set integration time to default value
    set_integration_time(1.0);
	}

	// start periodic msg
	set_periodic_msg_period((size_t)m_acq_param.pollingPeriod); //ms
	enable_periodic_msg(true);
}

// ============================================================================
// PulseCountingManagerScalar::periodic_job_i ()
// ============================================================================ 
void PulseCountingManagerScalar::periodic_job_i()
{
	std::vector<Tango::DevState> l_vect_state;
  std::string countersStatus = "";

  yat::AutoMutex<> guard(m_flagLock);
  
  // evaluate state if manager not already in fault
  if (m_state != Tango::FAULT) 
  {
	  if (!m_acq_param.continuousAcquisition)
	  {
		  Tango::DevState l_state = Tango::STANDBY;
		  if (m_acq_running)
		  {
			  // read current counters value & state from board
			  GenericCounterMap_t::iterator l_it;
			  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
			  {
				  l_it->second->update_scalar_value();
				  l_state = l_it->second->get_state();
				  l_vect_state.push_back(l_state);
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
			  }

			  // Check counters state
			  bool acq_error = false;
			  for (unsigned int i=0; i < l_vect_state.size(); ++i)
			  {
				  l_state = l_vect_state.at(i);
          
				  if (l_state == Tango::FAULT)
          {
            m_status = "One counter in FAULT state, acquisition stopped!\n";
					  acq_error = true;
            break;
          }
				  else if (l_state == Tango::ALARM)
          {
            m_status = "One counter OVERRUNS, acquisition stopped!\n";
					  acq_error = true;
            break;
          }
				  else if (l_state == Tango::RUNNING)
          {
					  m_state = Tango::RUNNING;
            m_status = "Acquisition in progress...\n";
          }
				  else if (l_state == Tango::DISABLE)
          {
            m_status = "One counter TIMES OUT, acquisition stopped!\n";
					  acq_error = true;
            break;
          }
          else
				  {
					  // nothing
				  }
			  }

        // add detailed states
        m_status += countersStatus;

        // if one counter in FAULT or ALARM, stop counting process
        if (acq_error)
        {
          m_state = Tango::FAULT;
          m_requestedStop = true;
          stop_i();
        }
		  }
		  else
		  {
			  // read current counters state from board
			  GenericCounterMap_t::iterator l_it;
			  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
			  {
          Tango::DevState l_state = l_it->second->get_state();
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
			  }

		    m_state = Tango::STANDBY;
        m_status = "Device is up and ready.\n";
        m_status += countersStatus;
		  }
	  }
	  else
	  {
      // Continuous mode
	    // read current counters state from board
	    GenericCounterMap_t::iterator l_it;
	    for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
	    {
		    Tango::DevState l_state = l_it->second->get_state();
        countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
	    }

 		  if (m_acq_running)
		  {
		    m_state = Tango::RUNNING;
        m_status = "Acquisition in progress...\n";
        m_status += countersStatus;
		  }
		  else
		  {
		    m_state = Tango::STANDBY;
        m_status = "Device is up and ready.\n";
        m_status += countersStatus;
		  }
	  }
  }
}

// ============================================================================
// PulseCountingManagerScalar::start_i ()
// ============================================================================ 
void PulseCountingManagerScalar::start_i()
{
  // set current acquisition parameters (it, continuous mode, ...)
  CHECK_CNT_BOARD();

  m_counting_board->setIntegrationTime(m_acq_param.integrationTime);
  m_counting_board->setContinuousMode(m_acq_param.continuousAcquisition);

  // then configure new clock (if needed)
  if (m_isHwInitNeeded)
    configure_clock();

  // init nexus manager (if needed)
  // do not re-initialize nexus file in continuous acquisition (except in 1st start)
  if (m_acq_param.nexusFileGeneration && m_firstStart)
  {
    // re-build nexus item list
    build_nexus_item_list();

    try
    {
	    // Initialize nexus manager
	    m_nexus_manager->initNexusAcquisition(
			  m_acq_param.nexusTargetPath,
			  m_acq_param.nexusFileName,
			  0, // infinite mode
			  m_acq_param.nexusNbPerFile,
			  m_nx_items);
    }
    catch( Tango::DevFailed &df )
    {
      releaseObjects();
	    ERROR_STREAM << "Init Nexus manager failed: " << df << std::endl;
	    RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
  	    "Init Nexus manager failed.", 
	      "PulseCountingManagerScalar::start_i"); 	
	  }
	  catch(...)
	  {
      releaseObjects();
      ERROR_STREAM << "Init Nexus manager failed: caught[...]" << std::endl;
	    THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
	      "Init Nexus manager failed.", 
	      "PulseCountingManagerScalar::start_i"); 	
	  }
  }

  // configure counters
  GenericCounterMap_t::iterator l_it;
  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
  {
	  GenericCounterInterface * l_int = l_it->second;
	  try
	  {
      if (l_int)
	    {
        l_int->init(m_counterListConfig[l_int]);
		    l_int->configure(m_counterListConfig[l_int]);
	    }
	  }
	  catch( Tango::DevFailed &df )
	  {
      releaseObjects();
      ERROR_STREAM << "Counter configuration failed: " << df << std::endl;
	    RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
		    "Counter configuration failed!", 
		    "PulseCountingManagerScalar::start_i"); 	
	  }
	  catch(...)
	  {
      releaseObjects();
      ERROR_STREAM << "Counter configure failed: caught [...]" << std::endl;
		  THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
			  "Counter configuration failed!", 
			  "PulseCountingManagerScalar::start_i"); 	
	    }
	  }

	// start counters
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
		try
		{
			l_it->second->start();
		}
		catch( Tango::DevFailed &df )
		{
      releaseObjects();
      ERROR_STREAM << "Counter start failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
				"Counter start failed.", 
				"PulseCountingManagerScalar::start_i"); 	
		}
		catch(...)
		{
      releaseObjects();
      ERROR_STREAM << "Counter start failed: caught [...]" << std::endl;
			THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
				"Counter start failed.", 
				"PulseCountingManagerScalar::start_i"); 	
		}
	}

  // start clock (if master)
	if (m_board_arch.clockGeneration)
	{
		try
		{
      m_clock_generator->start();
		}
		catch( Tango::DevFailed &df )
		{
      releaseObjects();
			ERROR_STREAM << "Start clock failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
				"Start clock failed.", 
				"PulseCountingManagerScalar::start_i"); 	
		}
		catch(...)
		{
      releaseObjects();
      ERROR_STREAM << "Start clock failed: caught [...]" << std::endl;
			THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
				"Start clock failed.", 
				"PulseCountingManagerScalar::start_i"); 	
		}

    // Waits for the end of clock
    wait_end_clock_i();
	}

	m_acq_running = true;
}

// ============================================================================
// PulseCountingManagerScalar::stop_i ()
// ============================================================================ 
void PulseCountingManagerScalar::stop_i()
{
  // stop clock (if master)
  if (m_board_arch.clockGeneration)
  {
    CHECK_CLK_GEN();
	  try
	  {
		  DEBUG_STREAM << "Trying to stop clock" << endl;
		  m_clock_generator->abort();
	  }
	  catch( Tango::DevFailed &df )
	  {
		  ERROR_STREAM << "Abort clock failed: " << df << std::endl;
		  RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
			  "Abort clock failed.", 
			  "PulseCountingManagerScalar::start_i"); 	
	  }
	  catch(...)
	  {
		  ERROR_STREAM << "Abort clock failed." << std::endl;
		  THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
			  "Abort clock failed.", 
			  "PulseCountingManagerScalar::start_i"); 	
	  }
  }

	// stop counters & do last update
	GenericCounterMap_t::iterator l_it;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
		try
		{
			l_it->second->update_scalar_value();
			l_it->second->stop();
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Counter stop failed " << df << std::endl;
			RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
				"Counter stop failed.", 
				"PulseCountingManagerScalar::stop_i"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Counter stop failed." << std::endl;
			THROW_DEVFAILED("SOFTWARE_FAILURE", 
				"Counter stop failed.", 
				"PulseCountingManagerScalar::stop_i"); 	
		}
	}

  // Store Nexus data
	if (m_acq_param.nexusFileGeneration)
	{
		for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
		{
			if (l_it->second->get_config().nexus)
			{
				data_t l_val = l_it->second->get_scalar_value();

				CHECK_NX_MANAGER();
				try
				{
					m_nexus_manager->pushNexusData(l_it->second->get_config().dataset, &l_val);
				}
				catch (Tango::DevFailed & df)
				{
					ERROR_STREAM << "PulseCountingManagerScalar::stop_i-> pushNexusData caught DevFailed: " << df << std::endl;
				}
				catch (...)
				{
					ERROR_STREAM << "PulseCountingManagerScalar::stop_i-> pushNexusData error."<< std::endl;
				}
			}
		}

    // Finalize Nexus file
		if (!m_acq_param.continuousAcquisition || 
        (m_requestedStop && m_acq_param.continuousAcquisition))
		{
			try
			{
				INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
				m_nexus_manager->finalizeNexusGeneration();
			}
			catch (Tango::DevFailed & df)
			{
				ERROR_STREAM << "Stop Nexus failed: " << df << std::endl;
			}
		}
	}

	m_acq_running = false;

	// check continuous mode to restart acquisition if not requested stop
  if (!m_requestedStop && m_acq_param.continuousAcquisition)
	{
		m_firstStart = false;
		start_i();
	}
}

// ============================================================================
// PulseCountingManagerScalar::manage_specific_dyn_attr ()
// ============================================================================ 
void PulseCountingManagerScalar::manage_specific_dyn_attr()
{
	if (! m_dyn_attr_manager)
	{
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "The dynamic attribute manager object isn't accessible ", 
      "PulseCountingManagerScalar::check_manager_dyn_attr_manager"); 
	}

  // Create SCALAR counting attributes
	counterConfigMap_t::iterator l_it;
	for (l_it = m_board_arch.counterConfiguration.begin(); 
    l_it != m_board_arch.counterConfiguration.end(); ++l_it)
	{
		CounterConfig p_cfg = l_it->second;
		yat4tango::DynamicAttributeInfo dai;
    unsigned int l_id = p_cfg.ctNumber; // key = counter ID in device (unique)

		//Counter attribute
		Tango::AttrWriteType rw_mode = Tango::READ;
		int attr_type = Tango::DEV_DOUBLE;
		std::string attrFormat = p_cfg.format;
		std::string attrDescription = "Counter value";
		dai.dev = m_board_arch.hostDevice;
		dai.tai.name = p_cfg.name;
		dai.tai.writable = rw_mode;
		dai.tai.data_format = Tango::SCALAR;
		dai.tai.format = attrFormat;
		dai.tai.data_type = attr_type;
		dai.tai.disp_level = Tango::OPERATOR;
		dai.tai.description = attrDescription;
    dai.tai.unit = p_cfg.pos_unitStr;
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
			&PulseCountingManagerScalar::read_counter);
		dai.set_user_data(l_id);

		try
		{
			//- add all dynamic attributes to the device interface
			m_dyn_attr_manager->add_attribute(dai);
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Dynamic attribute addition failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
				"Dynamic attribute addition failed.", 
				"PulseCountingManagerScalar::manage_specific_dyn_attr"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Dynamic attribute addition failed." << std::endl;
			THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
				"Dynamic attribute addition failed.", 
				"PulseCountingManagerScalar::manage_specific_dyn_attr"); 	
		}
	}
}

// ============================================================================
// PulseCountingManagerScalar::read_counter ()
// ============================================================================ 
void PulseCountingManagerScalar::read_counter(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  // get counter number
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);

  // get counter value
  if (m_counterList[l_id])
  {
    m_countersVal[l_id] = m_counterList[l_id]->get_scalar_value();
  }

	cbd.tga->set_value(&m_countersVal[l_id]);
}

// ============================================================================
// PulseCountingManagerScalar::wait_end_clock_i
// ============================================================================
void PulseCountingManagerScalar::wait_end_clock_i ()
{
  // Task the waiting for end of clock in a different thread, 
  // otherwise, the current thread blocks !

	ECConfig ec_cfg;
	ec_cfg.hostDevice = m_board_arch.hostDevice;
	ec_cfg.clockGen = m_clock_generator;

	m_endOfClock_thread = new ECThread(ec_cfg, static_cast<yat::Thread::IOArg>(this));
	
	if (m_endOfClock_thread == NULL)
	{
		ERROR_STREAM << "Waiting end of clock cannot be tasked!" << std::endl;
		THROW_DEVFAILED(
      "DEVICEN_ERROR",
			"Failed to start waiting task!",
			"PulseCountingManagerScalar::wait_end_clock_i");    
	}
	try
	{
		m_endOfClock_thread->start_undetached();
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start End Of Clock thread." << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to start End Of Clock thread.", 
			"PulseCountingManagerScalar::start_i"); 
	}
}

// ============================================================================
// PulseCountingManagerScalar::releaseObjects
// ============================================================================
void PulseCountingManagerScalar::releaseObjects()
{
  // release clock
  try
  {
    if (m_clock_generator)
    {
      m_clock_generator->deleteObject();
      m_isHwInitNeeded = true;
    }
  }
  catch(...)
  {
  }

  // release counters
  GenericCounterMap_t::iterator e_it;
  for (e_it = m_counterList.begin(); e_it != m_counterList.end(); ++e_it)
  {
    GenericCounterInterface * e_cpt = e_it->second;
    try
    {
	    if (e_cpt)
	    {
		    e_cpt->deleteObject();
	    }
    }
    catch(...)
    {
    }
  }
}

// ============================================================================
// PulseCountingManagerScalar::check_configuration
// ============================================================================
std::string PulseCountingManagerScalar::check_configuration(bool excpt)
{
  yat::OSStream oss;

  if (m_acq_param.continuousAcquisition)
  {
    oss << "Configuration is APPROVED!" << std::endl;
    oss << "Continuous acquisition, so acquisition is infinite. Must be stopped by user." << std::endl;

    if (m_board_arch.clockGeneration)
    {
      oss << "One value will be computed every " << m_acq_param.integrationTime << " seconds." << std::endl;
    }
  }
  else
  {
    oss << "Configuration is APPROVED!" << std::endl;

    if (m_board_arch.clockGeneration)
    {
      oss << "One acquisition will be done." << std::endl;
      oss << "The value will be available after " << m_acq_param.integrationTime << " seconds." << std::endl;
    }
    else
    {
      oss << "Infinite acquisition (external clock), which must be stopped by user." << std::endl;
    }
  }

  return oss.str();
}

} // namespace PulseCounting_ns

