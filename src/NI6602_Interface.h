//=============================================================================
// NI6602_Interface.h
//=============================================================================
// abstraction.......NI6602_Interface
// class.............NI6602_Interface
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _NI6602_INTERFACE_H
#define _NI6602_INTERFACE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CountingBoardInterface.h"
#include "BufferedCounterEvt.h"
#include "BufferedCounterPos.h"
#include "BufferedCounterDt.h"
#include "BufferedCounterPeriod.h"
#include <NI660Xsl/SimpleEventCounting.h>
#include <NI660Xsl/SimplePositionMeasurement.h>
#include <NI660Xsl/SimpleDTMeasurement.h>
#include <NI660Xsl/SimplePeriodMeasurement.h>
#include <NI660Xsl/FinitePulseTrainGeneration.h>
#include <NI660Xsl/ContinuousPulseTrainGeneration.h>

namespace PulseCounting_ns
{

// ============================================================================
// class: NI6602_Interface
// ============================================================================
class NI6602_Interface : public CountingBoardInterface
{

public:

	//- constructor
	NI6602_Interface (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t acqMode, 
    NexusManager * storage);

	//- destructor
	~NI6602_Interface ();

	//- initCounter
	void initCounter(CounterConfig p_cfg);

	//- init clock
	void initClock(ClockConfig p_cfg);

	//- configure counter
	void configureCounter(CounterConfig p_cfg); 

	//- start counter
	void startCounter(CounterConfig p_cfg);

	//- stop counter
	void stopCounter(CounterConfig p_cfg); 

	//- start clock
	void startClock(); 

	//- abort clock
	void abortClock();

	//- wait end of clock
	void waitEndOfClock();

	//- getDriverVersion
	std::string getDriverVersion() ;

	//- get max counter number
	unsigned int getMaxCounterNumber();

	//- set integration time
	void setIntegrationTime(double p_time);

	//- set buffer depth
	void setBufferDepth(long p_size);

	//- set samples number
	void setSamplesNumber(unsigned int p_number);

	//- set continuous mode
	void setContinuousMode(bool p_continuous);

	//- set acquisition timeout
	void setAcquisitionTimeout(double p_time);

  //- set start trigger use
  void setStartTriggerUse(bool p_use);

  //- set nexus storage use
  void setStorageUse(bool p_use);

	//- gets state
	Tango::DevState getCounterState(CounterConfig p_cfg);

	//- get counter scalar value
	data_t getCounterScalarValue(CounterConfig p_cfg); 

	//- get counter Buffer value
	RawData_t & getCounterBufferValue(CounterConfig p_cfg); 

  //- update counter Buffer value (used in "polling" mode)
  virtual void updateCounterBufferValue(CounterConfig p_cfg); 

  //- update last counter Buffer value (used with trigger listener option)
  virtual void updateCounterLastBufferValue(CounterConfig p_cfg);

  //- delete counter object
  void releaseCounter(CounterConfig p_cfg);

  //- release clock object
  void releaseClock();

private:
	// buffer position counter
	std::map<std::string, BufferedCounterPos *> m_buffer_pos_ct_list;

	// buffer event counter
	std::map<std::string, BufferedCounterEvt *> m_buffer_evt_ct_list;

  // buffer dt counter
	std::map<std::string, BufferedCounterDt *> m_buffer_dt_ct_list;

  // buffer period counter
	std::map<std::string, BufferedCounterPeriod *> m_buffer_prd_ct_list;

	// max counter number
	unsigned int m_max_nb_ct;

	// simple event counting
	std::map<std::string, ni660Xsl::SimpleEventCounting *> m_scalar_evt_ct_list;

	// simple position measurement
	std::map<std::string, ni660Xsl::SimplePositionMeasurement *> m_scalar_pos_ct_list;

	// simple dt measurement
	std::map<std::string, ni660Xsl::SimpleDTMeasurement *> m_scalar_dt_ct_list;

	// simple period measurement
	std::map<std::string, ni660Xsl::SimplePeriodMeasurement *> m_scalar_prd_ct_list;

	// finite clock
	ni660Xsl::FinitePulseTrainGeneration * m_scalar_finite_clock;

	// continuous clock
	ni660Xsl::ContinuousPulseTrainGeneration * m_buffer_continuous_clock;
};

} // namespace PulseCounting_ns

#endif // _NI6602_INTERFACE_H
