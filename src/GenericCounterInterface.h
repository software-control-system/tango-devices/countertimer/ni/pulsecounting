//=============================================================================
// GenericCounterInterface.h
//=============================================================================
// abstraction.......GenericCounterInterface for CountingManager
// class.............GenericCounterInterface
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _GENERIC_COUNTER_INTERFACE_H
#define _GENERIC_COUNTER_INTERFACE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/LogHelper.h>
#include <omnithread.h>
#include "CountingBoardInterface.h"
#include "PulseCountingTypesAndConsts.h"


namespace PulseCounting_ns
{

// ============================================================================
// class: GenericCounterInterface
// ============================================================================
class GenericCounterInterface : public yat4tango::TangoLogAdapter
{

public:

  //- constructor
  GenericCounterInterface (Tango::DeviceImpl * hostDevice): yat4tango::TangoLogAdapter(hostDevice){};

  //- destructor
  virtual ~GenericCounterInterface (){};

  //- init
  virtual void init(CounterConfig p_cfg) = 0;

  //- configure
  virtual void configure(CounterConfig p_cfg) = 0;

  //- start
  virtual void start() = 0;

  //- stop
  virtual void stop() = 0;

  //- delete counter object
  virtual void deleteObject() = 0;

  //- is available?
  virtual bool is_available() = 0;

  //- get state
  virtual Tango::DevState get_state() = 0;

  //- update scalar value
  virtual void update_scalar_value() = 0;

  //- update buffer value
  virtual void update_buffer_value() = 0;

  //- force last incomplete buffer update
  virtual void update_last_buffer_value() = 0;

  //- get scalar value
  virtual data_t get_scalar_value() = 0;

  //- get buffer value
  virtual RawData_t get_buffer_value() = 0;

  //- get name
  virtual std::string get_name() = 0;

  //- get config
  virtual CounterConfig get_config() = 0;
    
protected:
	//- counting board interface
	CountingBoardInterface * m_interface;
	//- state
	Tango::DevState m_state;

	//- available
	bool m_is_available;

	// initial value proxy
	Tango::DeviceProxy * m_initial_value_proxy;

	// acquisition mode
	E_AcquisitionMode_t m_acq_mode;

	// counter configuration
	CounterConfig m_cfg;

	// scalar value
	data_t m_value;

  //- data mutex protection
	yat::Mutex m_dataLock;
};

// Generic counter map type
// key = counter ID in device (0 --> N), value = pointer on generic counter
typedef std::map<size_t ,GenericCounterInterface *> GenericCounterMap_t;

} // namespace PulseCounting_ns

#endif // _GENERIC_COUNTER_INTERFACE_H
