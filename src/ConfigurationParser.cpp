//=============================================================================
// ConfigurationParser.cpp
//=============================================================================
// abstraction.......Acquisition configuration parser
// class.............ConfigurationParser
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ConfigurationParser.h"
#include <yat/utils/StringTokenizer.h>
#include <yat/utils/XString.h>
#include <yat/utils/String.h>
#include <yat/Exception.h>

namespace PulseCounting_ns
{

// ======================================================================
// ConfigurationParser::ConfigurationParser
// ======================================================================
ConfigurationParser::ConfigurationParser(Tango::DeviceImpl * host_device)
: Tango::LogAdapter(host_device)
{
}

// ======================================================================
// ConfigurationParser::~ConfigurationParser
// ======================================================================
ConfigurationParser::~ConfigurationParser ()
{
}

// ======================================================================
// ConfigurationParser::parseConfigProperty
// ======================================================================
CounterKeys_t ConfigurationParser::parseConfigProperty (std::vector<std::string> config_property)
{
  DEBUG_STREAM << "ConfigurationParser::parseConfigProperty ==>" << std::endl;

  // This function parses a "Config<i>" property and fills a map with the <KEY, value>
  // list defining the acquisition.

  CounterKeys_t acqKeyList;
  size_t chanIdx = 0;

  // Parse config vector and extract KEY::value fields
  for (std::vector<std::string>::iterator it = config_property.begin() ; it != config_property.end(); ++it)
  {
    std::vector<std::string> tokenList = splitLine(*it, kKEY_VALUE_SEPARATOR);

    if (tokenList.size() != 2)
    {
      // empty list => fatal error
      yat::OSStream oss;
      oss << "Bad configuration syntax: " << *it << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED("CONFIGURATION_ERROR", 
                      oss.str().c_str(), 
                      "ConfigurationParser::parseConfigProperty");
    }
    // insert <key,value> in map
    acqKeyList.insert(CounterKey_pair_t(tokenList[0], tokenList[1]));
    DEBUG_STREAM << "ConfigurationParser::parseConfigProperty - add <" << tokenList[0] 
    << "," << tokenList[1] << "> field" << std::endl;
  }

  return acqKeyList;
}


// ======================================================================
// ConfigurationParser::splitLine
// ======================================================================
std::vector<std::string> ConfigurationParser::splitLine(std::string line, std::string separator,
                                                        yat::uint16 min_token, yat::uint16 max_tokens)
{
	//- result : the split line 
  std::vector<std::string> tokenList;

  yat::StringTokenizer st(line, separator);

	while (st.has_more_tokens()) // while there is a remaining token
	{
    std::string token_str = st.next_token();
		tokenList.push_back(token_str);
	}

  // checks minimum expected number of tokens if not set to a null value
  if ((min_token != 0) &&
      (tokenList.size() < min_token) )
  {
    // expected more tokens
    yat::OSStream oss;
    oss << "Bad value: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::splitLine"); 
  }

  // checks maximum expected number of tokens if not set to a null value
  if ((max_tokens != 0) &&
      (tokenList.size() > max_tokens) )
  {
    // expected less tokens
    yat::OSStream oss;
    oss << "Bad value: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::splitLine"); 
  }

	return tokenList;
}

// ======================================================================
// ConfigurationParser::testKey
// ======================================================================
bool ConfigurationParser::testKey(std::string line, 
                                  std::string separator, 
                                  std::string key)
{
  bool isWrightKey = false;
  std::vector<std::string> tokenList = splitLine(line, separator);

  if (tokenList.size() != 2)
  {
    // empty list => fatal error
    yat::OSStream oss;
    oss << "Bad configuration syntax: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::getKeyValue");
  }

  if (0 == tokenList[0].compare(key))
  {
    // Wright key word => return true
    isWrightKey = true;
  }

  return isWrightKey;
}

// ======================================================================
// ConfigurationParser::getBoolean
// ======================================================================
bool ConfigurationParser::getBoolean(std::string token)
{
  bool result;
  yat::String ytoken(token);
  
  if (ytoken.is_equal_no_case("true"))
  {
    result = true;
  }
  else if (ytoken.is_equal_no_case("false"))
  {
    result = false;
  }
  else
  {
    // bad boolean value ==> fatal error
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    "Bad boolean value", 
                    "ConfigurationParser::parseConfigProperty"); 
  }

  return result;
}

// ======================================================================
// ConfigurationParser::extractName
// ======================================================================
std::string ConfigurationParser::extractName (CounterKeys_t acq_config)
{
	std::string name = "";

	CounterKeys_it_t it = acq_config.find(kKEY_NAME);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Name not found in configuration - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractName");
	}

	name = it->second; 
	DEBUG_STREAM << "Name: " << name << std::endl;

	return name;
}

// ======================================================================
// ConfigurationParser::extractProxy
// ======================================================================
std::string ConfigurationParser::extractProxy (CounterKeys_t acq_config)
{
	std::string proxy = "";

	CounterKeys_it_t it = acq_config.find(kKEY_PROXY);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Proxy not found in configuration - check device property";
		INFO_STREAM << oss.str() << std::endl;
		return "";
		/*THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractProxy");*/
	}

	proxy = it->second; 
	DEBUG_STREAM << "Proxy: " << proxy << std::endl;

	return proxy;
}

// ======================================================================
// ConfigurationParser::extractMode
// ======================================================================
E_CounterMode_t ConfigurationParser::extractMode (CounterKeys_t acq_config)
{
  E_CounterMode_t mode = UNDEFINED_COUNTER_MODE;

  CounterKeys_it_t it = acq_config.find(kKEY_MODE);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "Mode not found in configuration - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::extractTriggerMode");
  }

  std::string tokenValue = it->second;

  if (0 == tokenValue.compare(kKEY_MODE_EVT))
  {
    mode = COUNTER_MODE_EVENT;
  }
  else if (0 == tokenValue.compare(kKEY_MODE_POS))
  {
    mode = COUNTER_MODE_POSITION;
  }
  else if (0 == tokenValue.compare(kKEY_MODE_DT))
  {
    mode = COUNTER_MODE_DELTATIME;
  }
  else if (0 == tokenValue.compare(kKEY_MODE_PERIOD))
  {
    mode = COUNTER_MODE_PERIOD;
  }
  else
  {
    // bad value ==> fatal error
    yat::OSStream oss;
    oss << "Bad mode value: " << tokenValue << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::extractMode");
  }

  return mode;
}

// ======================================================================
// ConfigurationParser::extractFormat
// ======================================================================
std::string ConfigurationParser::extractFormat (CounterKeys_t acq_config)
{
  std::string format = "%f";

  CounterKeys_it_t it = acq_config.find(kKEY_FORMAT);
  
  if (it != acq_config.end())
  {
	  format = it->second;
  }


  return format;
}

// ======================================================================
// ConfigurationParser::extractEdge
// ======================================================================
E_CountingEdge_t ConfigurationParser::extractEdge (CounterKeys_t acq_config, size_t nb)
{
	E_CountingEdge_t edge;
  std::string key = kKEY_EDGE;

  if (nb == 2)
    key = kKEY_EDGE2;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(key);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "Mising edge value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractEdge"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_EDGE_FAL))
	{
	  edge = COUNTING_EDGE_FALLING;
	}
	else if (0 == tokenValue.compare(kKEY_EDGE_RIS))
	{
	  edge = COUNTING_EDGE_RISING;
	}
	else
	{
	  // bad value ==> fatal error
	  yat::OSStream oss;
	  oss << "Bad edge value: " << tokenValue << " - check device property";
	  ERROR_STREAM << oss.str() << std::endl;
	  THROW_DEVFAILED("CONFIGURATION_ERROR", 
					  oss.str().c_str(), 
					  "ConfigurationParser::extractEdge"); 
	}

  DEBUG_STREAM << "Edge: " << edge << std::endl;

  return edge;
}

// ======================================================================
// ConfigurationParser::extractMemoryTransfer
// ======================================================================
E_MemoryTransfer_t ConfigurationParser::extractMemoryTransfer (CounterKeys_t acq_config)
{
	E_MemoryTransfer_t transfer;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_TRANSFER);

	if (it == acq_config.end())
	{
    // no value set, use default value
    transfer = MEMORY_TRSF_ITR;
	}
  else
  {
	  std::string tokenValue = it->second;

	  if (0 == tokenValue.compare(kKEY_TRANSFER_DMA))
	  {
	    transfer = MEMORY_TRSF_DMA;
	  }
	  else if (0 == tokenValue.compare(kKEY_TRANSFER_ITR))
	  {
	    transfer = MEMORY_TRSF_ITR;
	  }
	  else
	  {
	    // bad value ==> fatal error
	    yat::OSStream oss;
	    oss << "Bad transfer value: " << tokenValue << " - check device property";
	    ERROR_STREAM << oss.str() << std::endl;
	    THROW_DEVFAILED("CONFIGURATION_ERROR", 
					    oss.str().c_str(), 
					    "ConfigurationParser::extractMemoryTransfer"); 
	  }
  }

  DEBUG_STREAM << "Transfer: " << transfer << std::endl;

  return transfer;
}

// ======================================================================
// ConfigurationParser::extractDirection
// ======================================================================
E_CountingDirection_t ConfigurationParser::extractDirection (CounterKeys_t acq_config)
{
	E_CountingDirection_t direction;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_DIRECTION);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No direction value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDirection"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_DIRECTION_DOWN))
	{
	  direction = CNT_DIRECTION_DOWN;
	}
	else if (0 == tokenValue.compare(kKEY_DIRECTION_UP))
	{
	  direction = CNT_DIRECTION_UP;
	}
	else if (0 == tokenValue.compare(kKEY_DIRECTION_EXT))
	{
		direction = CNT_DIRECTION_EXT;
	}
	else
	{
	  // bad value ==> fatal error
	  yat::OSStream oss;
	  oss << "Bad direction value: " << tokenValue << " - check device property";
	  ERROR_STREAM << oss.str() << std::endl;
	  THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
					  oss.str().c_str(), 
					  "ConfigurationParser::extractDirection"); 
	}

  DEBUG_STREAM << "Direction: " << direction << std::endl;

  return direction;
}

// ======================================================================
// ConfigurationParser::extractDecodingMode
// ======================================================================
E_DecodingMode_t ConfigurationParser::extractDecodingMode (CounterKeys_t acq_config)
{
	E_DecodingMode_t decodingMode;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_DECODING);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No Decoding value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDecodingMode"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_DECODING_X1))
	{
		decodingMode = DEC_MODE_X1;
	}
	else if (0 == tokenValue.compare(kKEY_DECODING_X2))
	{
		decodingMode = DEC_MODE_X2;
	}
	else if (0 == tokenValue.compare(kKEY_DECODING_X4))
	{
		decodingMode = DEC_MODE_X4;
	}
	else if (0 == tokenValue.compare(kKEY_DECODING_TWO_PULSE))
	{
		decodingMode = DEC_MODE_TWO_PULSE;
	}
	else
	{
		// bad value ==> fatal error
		yat::OSStream oss;
		oss << "Bad decoding mode value: " << tokenValue << " - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDecodingMode"); 
	}

	DEBUG_STREAM << "Decoding mode: " << decodingMode << std::endl;

	return decodingMode;
}

// ======================================================================
// ConfigurationParser::extractZIndexPhase
// ======================================================================
E_ZIndexPhase_t ConfigurationParser::extractZIndexPhase (CounterKeys_t acq_config)
{
	E_ZIndexPhase_t zIndexPhase;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_ZINDEXPHASE);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No ZIndexPhase value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractZIndexPhase"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_ZINDEXPHASE_AHBH))
	{
		zIndexPhase = ZINDEX_PHASE_AH_BH;
	}
	else if (0 == tokenValue.compare(kKEY_ZINDEXPHASE_AHBL))
	{
		zIndexPhase = ZINDEX_PHASE_AH_BL;
	}
	else if (0 == tokenValue.compare(kKEY_ZINDEXPHASE_ALBH))
	{
		zIndexPhase = ZINDEX_PHASE_AL_BH;
	}
	else if (0 == tokenValue.compare(kKEY_ZINDEXPHASE_ALBL))
	{
		zIndexPhase = ZINDEX_PHASE_AL_BL;
	}
	else
	{
		// bad value ==> fatal error
		yat::OSStream oss;
		oss << "Bad Z Index Phase value: " << tokenValue << " - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractZIndexPhase"); 
	}

	DEBUG_STREAM << "Z Index Phase: " << zIndexPhase << std::endl;

	return zIndexPhase;
}

// ======================================================================
// ConfigurationParser::extractEncoderMode
// ======================================================================
E_EncoderMode_t ConfigurationParser::extractEncoderMode (CounterKeys_t acq_config)
{
	E_EncoderMode_t encoderMode;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_ENCODERMODE);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No EcoderMode value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDecodingMode"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_ENCODERMODE_ANG))
	{
		encoderMode = ENC_MODE_ANGULAR;
	}
	else if (0 == tokenValue.compare(kKEY_ENCODERMODE_LIN))
	{
		encoderMode = ENC_MODE_LINEAR;
	}
	else
	{
		// bad value ==> fatal error
		yat::OSStream oss;
		oss << "Bad encoder mode value: " << tokenValue << " - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDecodingMode"); 
	}

	DEBUG_STREAM << "Encoder mode: " << encoderMode << std::endl;

	return encoderMode;
}

// ======================================================================
// ConfigurationParser::extractUnitStr
// ======================================================================
std::string ConfigurationParser::extractUnitStr (CounterKeys_t acq_config)
{
  std::string unitStr;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_UNITS);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No Unit value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractUnit"); 
	}

  unitStr = it->second;

	return unitStr;
}

// ======================================================================
// ConfigurationParser::extractUnit
// ======================================================================
E_EncoderUnit_t ConfigurationParser::extractUnit (CounterKeys_t acq_config)
{
	E_EncoderUnit_t unit;

	// get overrun strategy (optional)
	CounterKeys_it_t it = acq_config.find(kKEY_UNITS);

	if (it == acq_config.end())
	{
		yat::OSStream oss;
		oss << "No Unit value: - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractUnit"); 
	}

	std::string tokenValue = it->second;

	if (0 == tokenValue.compare(kKEY_UNITS_DEGREE))
	{
		unit = ENC_UNIT_DEGREE;
	}
	else if (0 == tokenValue.compare(kKEY_UNITS_RADIAN))
	{
		unit = ENC_UNIT_RADIAN;
	}
	else if (0 == tokenValue.compare(kKEY_UNITS_INCH))
	{
		unit = ENC_UNIT_INCH;
	}
	else if (0 == tokenValue.compare(kKEY_UNITS_METER))
	{
		unit = ENC_UNIT_METER;
	}
	else if (0 == tokenValue.compare(kKEY_UNITS_TICK))
	{
		unit = ENC_UNIT_TICK;
	}
	else if (0 == tokenValue.compare(kKEY_UNITS_SEC))
	{
		unit = ENC_UNIT_SEC;
	}
  else
	{
		// bad value ==> fatal error
		yat::OSStream oss;
		oss << "Bad unit value: " << tokenValue << " - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractUnit"); 
	}

	DEBUG_STREAM << "Unit: " << unit << std::endl;

	return unit;
}

// ======================================================================
// ConfigurationParser::extractZIndexValue
// ======================================================================
yat::uint32 ConfigurationParser::extractZIndexValue (CounterKeys_t acq_config)
{
  yat::uint32 zval = 0;

  // get timeout 
  CounterKeys_it_t it = acq_config.find(kKEY_ZINDEXVAL);
  
  if (it == acq_config.end())
  {
	  zval = 0;
  }
  else
  {
    try
    {
      unsigned int tmo = yat::XString<unsigned int>::to_num(it->second);
      zval = (yat::uint32)tmo;
    }
    catch (const yat::Exception& ye)
    {
      // bad int value ==> fatal error
      yat::OSStream oss;
      oss << "Bad ZIndexValue value: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED("CONFIGURATION_ERROR", 
                      oss.str().c_str(), 
                      "ConfigurationParser::extractZIndexValue"); 
    }
  }

  DEBUG_STREAM << "ZIndexValue: " << zval << std::endl;

  return zval;
}

// ======================================================================
// ConfigurationParser::extractDistPerPulse
// ======================================================================
double ConfigurationParser::extractDistPerPulse (CounterKeys_t acq_config)
{
	double val = 0;

	// get timeout 
	CounterKeys_it_t it = acq_config.find(kKEY_DISTANCEPERPULSE);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "DistPerPulse value not found in configuration - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractDistPerPulse");    
	}
	else
	{
		try
		{
			double tmo = yat::XString<double>::to_num(it->second);
			val = (double)tmo;
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad DistPerPulse value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED(
        "CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractDistPerPulse"); 
		}
	}

	DEBUG_STREAM << "Dist Per Pulse value: " << val << std::endl;

	return val;
}

// ======================================================================
// ConfigurationParser::extractPulsePerRev
// ======================================================================
yat::uint32 ConfigurationParser::extractPulsePerRev (CounterKeys_t acq_config)
{
	yat::uint32 val = 0;

	// get timeout 
	CounterKeys_it_t it = acq_config.find(kKEY_PULSEPERREV);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "PulsePerRev value not found in configuration - check device property";
		ERROR_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractPulsePerRev");    
	}
	else
	{
		try
		{
			unsigned int tmo = yat::XString<unsigned int>::to_num(it->second);
			val = (yat::uint32)tmo;
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad PulsePerRev value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED(
        "CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractPulsePerRev"); 
		}
	}

	DEBUG_STREAM << "Pulse Per Rev value: " << val << std::endl;

	return val;
}

// ======================================================================
// ConfigurationParser::extractZIndexEnabled
// ======================================================================
bool ConfigurationParser::extractZIndexEnabled (CounterKeys_t acq_config)
{
  bool enabled;

  // get mask computed property
  CounterKeys_it_t it = acq_config.find(kKEY_ZINDEX_ENABLED);

  if (it == acq_config.end())
  {
    // if not defined, default value = false
    enabled = false;
  }
  else
  {
    try
    {
      enabled = getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for ZIndexEnabled property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED("CONFIGURATION_ERROR", 
                      oss.str().c_str(), 
                      "ConfigurationParser::extractZIndexEnabled"); 
    }
  }

  return enabled;
}

// ======================================================================
// ConfigurationParser::extractNexus
// ======================================================================
bool ConfigurationParser::extractNexus (CounterKeys_t acq_config)
{
	bool enabled;

	// get mask computed property
	CounterKeys_it_t it = acq_config.find(kKEY_NEXUS);

	if (it == acq_config.end())
	{
		// if not defined, default value = false
		enabled = false;
	}
	else
	{
		try
		{
			enabled = getBoolean(it->second);
		}
		catch(Tango::DevFailed & df)
		{
			// bad boolean value ==> fatal error
			yat::OSStream oss;
			oss << "Bad value for Nexus property: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED(
        "CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractNexus"); 
		}
	}

	return enabled;
}

} // namespace PulseCounting_ns


