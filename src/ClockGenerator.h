//=============================================================================
// ClockGenerator.h
//=============================================================================
// abstraction.......ClockGenerator for PulseCounting
// class.............ClockGenerator
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _CLOCK_GENERATOR_H
#define _CLOCK_GENERATOR_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingTypesAndConsts.h"
#include "CountingBoardInterface.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: ClockGenerator
// ============================================================================
class ClockGenerator  : public yat4tango::TangoLogAdapter
{

public:

  //- constructor
  ClockGenerator (Tango::DeviceImpl * hostDevice, CountingBoardInterface * p_board);

  //- destructor
  ~ClockGenerator ();

  //- init
  void init(ClockConfig p_cfg);

  //- starts the clock
  void start();

  //- abort the clock
  void abort();

  //- wait end of clock
  void waitEndOfClock();

  //- delete clock object
  void deleteObject();
  
protected:
	//- counting board interface
	CountingBoardInterface * m_counting_board;

	//- clock config
	ClockConfig m_cfg;
};

} // namespace PulseCounting_ns

#endif // _CLOCK_GENERATOR_H
