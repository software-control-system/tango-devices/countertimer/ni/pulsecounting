//=============================================================================
// NI6602_Interface.cpp
//=============================================================================
// abstraction.......NI6602_Interface
// class.............NI6602_Interface
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "NI6602_Interface.h"


namespace PulseCounting_ns
{
// ============================================================================
// NI6602_Interface::NI6602_Interface ()
// ============================================================================ 
NI6602_Interface::NI6602_Interface (Tango::DeviceImpl * hostDevice, 
                                    E_AcquisitionMode_t p_acqMode,
                                    NexusManager * storage)
: CountingBoardInterface(hostDevice, p_acqMode, storage)
{
	m_max_nb_ct = k_NI66602_MAX_NUMBER_OF_COUNTERS;
	m_scalar_finite_clock = NULL;
	m_buffer_continuous_clock = NULL;
}

// ============================================================================
// NI6602_Interface::~NI6602_Interface ()
// ============================================================================ 
NI6602_Interface::~NI6602_Interface ()
{

	if (m_scalar_finite_clock)
	{
		delete m_scalar_finite_clock;
		m_scalar_finite_clock = NULL;
	}

	if (m_buffer_continuous_clock)
	{
		delete m_buffer_continuous_clock;
		m_buffer_continuous_clock = NULL;
	}
}

// ============================================================================
// NI6602_Interface::initCounter ()
// ============================================================================ 
void NI6602_Interface::initCounter(CounterConfig p_cfg)
{
	switch (m_acq_mode)
	{
	  case ACQ_MODE_SCAL:
		  switch (p_cfg.mode)
		  {
			  case COUNTER_MODE_POSITION:
				  if (m_scalar_pos_ct_list[p_cfg.name])
				  {
					  delete m_scalar_pos_ct_list[p_cfg.name];
					  m_scalar_pos_ct_list[p_cfg.name] = NULL;
				  }
				  break;
			  case COUNTER_MODE_EVENT:
				  if (m_scalar_evt_ct_list[p_cfg.name])
				  {
					  delete m_scalar_evt_ct_list[p_cfg.name];
					  m_scalar_evt_ct_list[p_cfg.name] = NULL;
				  }
				  break;
			  case COUNTER_MODE_DELTATIME:
				  if (m_scalar_dt_ct_list[p_cfg.name])
				  {
					  delete m_scalar_dt_ct_list[p_cfg.name];
					  m_scalar_dt_ct_list[p_cfg.name] = NULL;
				  }
				  break;
			  case COUNTER_MODE_PERIOD:
				  if (m_scalar_prd_ct_list[p_cfg.name])
				  {
					  delete m_scalar_prd_ct_list[p_cfg.name];
					  m_scalar_prd_ct_list[p_cfg.name] = NULL;
				  }
				  break;
        default:
				  break;
		  }
		  break;

	case ACQ_MODE_BUFF:
		switch (p_cfg.mode)
		{
		  case COUNTER_MODE_POSITION:
			  if (m_buffer_pos_ct_list[p_cfg.name])
			  {
				  delete m_buffer_pos_ct_list[p_cfg.name];
				  m_buffer_pos_ct_list[p_cfg.name] = NULL;
			  }
			  break;
		  case COUNTER_MODE_EVENT:
			  if (m_buffer_evt_ct_list[p_cfg.name])
			  {
				  delete m_buffer_evt_ct_list[p_cfg.name];
				  m_buffer_evt_ct_list[p_cfg.name] = NULL;
			  }
			  break;
		  case COUNTER_MODE_DELTATIME:
			  if (m_buffer_dt_ct_list[p_cfg.name])
			  {
				  delete m_buffer_dt_ct_list[p_cfg.name];
				  m_buffer_dt_ct_list[p_cfg.name] = NULL;
			  }
			  break;
		  case COUNTER_MODE_PERIOD:
			  if (m_buffer_prd_ct_list[p_cfg.name])
			  {
				  delete m_buffer_prd_ct_list[p_cfg.name];
				  m_buffer_prd_ct_list[p_cfg.name] = NULL;
			  }
			  break;
		  default:
			  break;
		}
		break;

	default:
		break;
	}
}

// ============================================================================
// NI6602_Interface::initClock ()
// ============================================================================ 
void NI6602_Interface::initClock(ClockConfig p_cfg)
{
	m_clk_cfg = p_cfg;

  // 3 possible timebases in AUTO mode: 100kHz, 20MHz, 80Mhz
  const double time_limit_100k = double(0x7FFFFFFF) / 100000;
  const double time_limit_20M  = double(0x7FFFFFFF) / 2E7;
  const double time_limit_80M  = double(0x7FFFFFFF) / 8E7;

  // SCALAR mode
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
		ni660Xsl::OutClockTicksChan chan;

    // if clock object already exists, release and delete
		if (m_scalar_finite_clock)
		{
      m_scalar_finite_clock->release();
			delete m_scalar_finite_clock;
			m_scalar_finite_clock = NULL;
		}

		DEBUG_STREAM << "NI6602_Interface::initClock Finite clock" << std::endl;

		// Configure finite clock
		m_scalar_finite_clock = new ni660Xsl::FinitePulseTrainGeneration();

		chan.chan_name = std::string("/") + m_clk_cfg.boardName + std::string("/ctr0");
		chan.idle_state = ni::low;
		chan.initial_delay = 0;

    // Automatic configuration according to integration time value
		if (m_cfg.integrationTime <= time_limit_80M)
		{
			DEBUG_STREAM << "Using 80 MHz timebase";
			chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/80MHzTimebase");
			chan.low_ticks = 3;
			chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 8E7); //secs
		}
		else if (m_cfg.integrationTime <= time_limit_20M)
		{
			DEBUG_STREAM << "Using 20 MHz timebase";
			chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/20MHzTimebase");
			chan.low_ticks = 3;
			chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 2E7); //secs
		}
		else if (m_cfg.integrationTime <= time_limit_100k)
		{
			DEBUG_STREAM << "Using 100 kHz timebase";
			chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/100kHzTimebase");
			chan.low_ticks = 3;
			chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 1E5); //secs
		}
		else
		{
			ERROR_STREAM << "Finite clock configuration error!" << std::endl;
			THROW_DEVFAILED(
        "OUT_OF_RANGE",
				"Failed to configure finite clock! Integration time out of range!",
				"NI6602_Interface::initClock");
		}

		// Initiliazing and configuring clock
		try
		{
			m_scalar_finite_clock->add_clock_ticks_channel(chan);
			m_scalar_finite_clock->set_nb_pulses(1);
			m_scalar_finite_clock->init();
			m_scalar_finite_clock->configure();
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
  }
  // BUFFERED mode
	else if (m_acq_mode == ACQ_MODE_BUFF)
	{
		ni660Xsl::OutClockTicksChan chan;

    // if clock object already exists, release and delete
		if (m_buffer_continuous_clock)
		{
      m_buffer_continuous_clock->release();
			delete m_buffer_continuous_clock;
			m_buffer_continuous_clock = NULL;
		}

		DEBUG_STREAM << "NI6602_Interface::configure continuous clock" << std::endl;
		DEBUG_STREAM << "NI6602_Interface::initClock timebase scaling: " << m_clk_cfg.timebaseScaling << std::endl;

		// Configure continous clock
		m_buffer_continuous_clock = new ni660Xsl::ContinuousPulseTrainGeneration();
		chan.chan_name = std::string("/") + m_clk_cfg.boardName + std::string("/ctr0");
		chan.idle_state = ni::low;
		chan.initial_delay = 0;

    // Configure clock according to timebase scaling property
		if (m_clk_cfg.timebaseScaling.compare("AUTO") == 0)
		{
			DEBUG_STREAM << "AUTO timebasScaling" << endl;
			if (m_cfg.integrationTime <= time_limit_80M)
			{
				DEBUG_STREAM << "Using 80 MHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/80MHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 8E7); //secs
			}
			else if (m_cfg.integrationTime <= time_limit_20M)
			{
				DEBUG_STREAM << "Using 20 MHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/20MHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 2E7); //secs
			}
			else if (m_cfg.integrationTime <= time_limit_100k)
			{
				DEBUG_STREAM << "Using 100 kHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/100kHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 1E5); //secs
			}
			else
			{
				ERROR_STREAM << "Continuous clock configuration error!" << std::endl;
				THROW_DEVFAILED(
          "OUT_OF_RANGE",
					"Failed to configure continuous clock! Integration time out of range!",
					"NI6602_Interface::initClock");
			}
		}
		else
		{
			DEBUG_STREAM << "USER TimebaseScaling" << endl;
			if (m_clk_cfg.timebaseScaling.compare("20M") == 0)
			{
				DEBUG_STREAM << "Using 20 MHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/20MHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 2E7); //secs
			}
			else if (m_clk_cfg.timebaseScaling.compare("80M") == 0)
			{
				DEBUG_STREAM << "Using 80 MHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/80MHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 8E7); //secs
			}
			else if (m_clk_cfg.timebaseScaling.compare("100k") == 0)
			{
				DEBUG_STREAM << "Using 100 kHz timebase";
				chan.clk_source = std::string("/") + m_clk_cfg.boardName + std::string("/100kHzTimebase");
				chan.low_ticks = 3;
				chan.high_ticks = static_cast<long>(m_cfg.integrationTime * 1E5); //secs
			}
			else
			{
				THROW_DEVFAILED(
          "CONFIGURATION_ERROR",
					"Failed to configure continuous clock! TimebasScaling value is not correct!",
					"NI6602_Interface::initClock");
			}
		}

		// Initiliazing and configuring clock
		try
		{
			// Initiliazing and configuring clock
			m_buffer_continuous_clock->add_clock_ticks_channel(chan);
			m_buffer_continuous_clock->init();
			m_buffer_continuous_clock->configure();
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
	}
	else
	{
		// bad value
		ERROR_STREAM << "Clock cannot be started! Bad acquisition mode!" << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR",
			"Failed to start clock, bad acquisition mode value!",
			"NI6602_Interface::initClock");
	}
}

// ============================================================================
// NI6602_Interface::configureCounter ()
// ============================================================================ 
void NI6602_Interface::configureCounter(CounterConfig p_cfg)
{
	ni660Xsl::EventCountChan l_evtConfig;
	ni660Xsl::PositionChan l_posConfig;
  ni660Xsl::DeltaTimeChan l_dtConfig;
  ni660Xsl::PeriodChan l_prdConfig;
	ni660Xsl::AngularEncoderChan l_angConfig;
	ni660Xsl::LinearEncoderChan l_linConfig;
	ni::AngleUnitsType l_angleUnits;
	ni::DistanceUnitsType l_distUnits;
  ni::TimeUnitsType l_dtUnits;
	double l_distPerPulse = 0.0;
	long l_pulsePerRev = 0;
	double l_init_pos = 0.0;

	//- get the initial position proxy (if defined)
	if (p_cfg.initProxy.compare("") != 0)
	{
		Tango::AttributeProxy * l_attr_proxy;
		try
		{
			l_attr_proxy = new Tango::AttributeProxy(p_cfg.initProxy);
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Failed to create AttributeProxy: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"AttributeProxy creation failed",
				"PulseCountingManagerScalar::configureCounter"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to create AttributeProxy." << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"AttributeProxy creation failed", 
				"PulseCountingManagerScalar::configureCounter"); 	
		}
		try
		{
			Tango::DeviceAttribute l_da = l_attr_proxy->read();
			l_da >> l_init_pos;
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Failed to read AttributeProxy value: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"AttributeProxy read failed",
				"PulseCountingManagerScalar::configureCounter"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to read AttributeProxy value." << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"AttributeProxy read failed", 
				"PulseCountingManagerScalar::configureCounter"); 	
		}

    if (l_attr_proxy)
    {
      delete l_attr_proxy;
      l_attr_proxy = NULL;
    }

    // store initial position in counter config
    p_cfg.absInitPos = l_init_pos;
	}

  //- get configuration according to counter type
  switch (p_cfg.mode)
  {
    //********************************************************************/
    // POSITION COUNTER
    //********************************************************************/
    case COUNTER_MODE_POSITION:
      l_posConfig.chan_name = std::string("/") + p_cfg.boardName + std::string("/") + std::string("ctr")
			  + yat::XString<size_t>::to_string(p_cfg.number);
			
		  l_posConfig.z_idx_enable = p_cfg.pos_ZindexEnabled;
		  l_posConfig.z_idx_val = p_cfg.pos_ZindexValue;

      // decoding mode
		  switch (p_cfg.pos_decodingMode)
		  {
		    case DEC_MODE_X1: 
			    l_posConfig.decoding_type = ni::x1;
			    break;
		    case DEC_MODE_X2: 
			    l_posConfig.decoding_type = ni::x2;
			    break;
		    case DEC_MODE_X4: 
			    l_posConfig.decoding_type = ni::x4;
			    break;
		    case DEC_MODE_TWO_PULSE: 
			    l_posConfig.decoding_type = ni::two_pulse;
			    break;
		    default:
			    break;
		  }

      // Z index
		  switch (p_cfg.pos_ZindexPhase)
		  {
			  case ZINDEX_PHASE_AH_BH: 
				  l_posConfig.z_idx_phase = ni::AHigh_BHigh;
				  break;
			  case ZINDEX_PHASE_AH_BL: 
				  l_posConfig.z_idx_phase = ni::AHigh_BLow;
				  break;
			  case ZINDEX_PHASE_AL_BH: 
				  l_posConfig.z_idx_phase = ni::ALow_BHigh;
				  break;
			  case ZINDEX_PHASE_AL_BL: 
				  l_posConfig.z_idx_phase = ni::ALow_BLow;
				  break;
			  default:
				  break;
		  }

      // Encoder type
      switch (p_cfg.pos_encoderMode)
		  {
			  case ENC_MODE_ANGULAR:
				  switch (p_cfg.pos_encoderUnit)
				  {
				    case ENC_UNIT_DEGREE:
					    l_angleUnits = ni::degrees;
					    break;
				    case ENC_UNIT_RADIAN:
					    l_angleUnits = ni::radians;
					    break;
				    case ENC_UNIT_TICK:
					    l_angleUnits = ni::ang_ticks;
					    break;
				    default:
					    break;
				  }
				  l_pulsePerRev = p_cfg.pos_pulsePerRevolution;

				  l_angConfig.chan_name = l_posConfig.chan_name;
				  l_angConfig.decoding_type = l_posConfig.decoding_type;
				  l_angConfig.z_idx_enable = l_posConfig.z_idx_enable;
				  l_angConfig.z_idx_val = l_posConfig.z_idx_val;
				  l_angConfig.z_idx_phase = l_posConfig.z_idx_phase;
				  l_angConfig.units = l_angleUnits;
				  l_angConfig.pulse_per_revolution = l_pulsePerRev;
				  l_angConfig.initial_angle = l_init_pos;
          break;

				case ENC_MODE_LINEAR:
				  switch (p_cfg.pos_encoderUnit)
				  {
					  case ENC_UNIT_METER:
						  l_distUnits = ni::meters;
						  break;
					  case ENC_UNIT_INCH:
						  l_distUnits = ni::inches;
						  break;
					  case ENC_UNIT_TICK:
						  l_distUnits = ni::dist_ticks;
						  break;
					  default:
						  break;
				  }
          l_distPerPulse = p_cfg.pos_distPerPulse;

				  l_linConfig.chan_name = l_posConfig.chan_name;
				  l_linConfig.decoding_type = l_posConfig.decoding_type;
				  l_linConfig.z_idx_enable = l_posConfig.z_idx_enable;
				  l_linConfig.z_idx_val = l_posConfig.z_idx_val;
				  l_linConfig.z_idx_phase = l_posConfig.z_idx_phase;
				  l_linConfig.units = l_distUnits;
				  l_linConfig.distance_per_pulse = l_distPerPulse;
				  l_linConfig.initial_position = l_init_pos;
          break;

        default:
          break;
      }
      break;


    //********************************************************************/
    // EVENT COUNTER
    //********************************************************************/
	  case COUNTER_MODE_EVENT:
		  l_evtConfig.chan_name = std::string("/") + p_cfg.boardName + std::string("/") + std::string("ctr")
			  + yat::XString<size_t>::to_string(p_cfg.number);

		  ni::EdgeType edge_type;
		  switch (p_cfg.edge)
		  {
		    case COUNTING_EDGE_RISING:
			    edge_type = ni::rising_edge;
			    break;
		    case COUNTING_EDGE_FALLING:
			    edge_type = ni::falling_edge;
			    break;
		    default:
			    edge_type = ni::rising_edge; //default value if not defined
			    break;
		  }
		  l_evtConfig.edge = edge_type;

		  l_evtConfig.initial_count = 0;

		  ni::DirectionType direction;
		  switch (p_cfg.direction)
		  {
		    case CNT_DIRECTION_UP:
			    direction = ni::count_up;
			    break;
		    case CNT_DIRECTION_DOWN:
			    direction = ni::count_down;
			    break;
		    case CNT_DIRECTION_EXT:
			    direction = ni::external_control;
			    break;
		    default:
			    direction = ni::count_up; //default value if not defined
			    break;
		  }
		  l_evtConfig.count_direction = direction;
		  break;

    //********************************************************************/
    // DELTA TIME COUNTER
    //********************************************************************/
	  case COUNTER_MODE_DELTATIME:
		  l_dtConfig.chan_name = std::string("/") + p_cfg.boardName + std::string("/") + std::string("ctr")
			  + yat::XString<size_t>::to_string(p_cfg.number);

		  ni::EdgeType edge1;
		  switch (p_cfg.edge)
		  {
		    case COUNTING_EDGE_RISING:
			    edge1 = ni::rising_edge;
			    break;
		    case COUNTING_EDGE_FALLING:
			    edge1 = ni::falling_edge;
			    break;
		    default:
			    edge1 = ni::rising_edge; //default value if not defined
			    break;
		  }
		  l_dtConfig.first_edge = edge1;

      ni::EdgeType edge2;
      switch (p_cfg.scdEdge)
		  {
		    case COUNTING_EDGE_RISING:
			    edge2 = ni::rising_edge;
			    break;
		    case COUNTING_EDGE_FALLING:
			    edge2 = ni::falling_edge;
			    break;
		    default:
			    edge2 = ni::falling_edge; //default value if not defined
			    break;
		  }
		  l_dtConfig.second_edge = edge2;
		  
      switch (p_cfg.dt_unit)
		  {
			  case ENC_UNIT_TICK:
				  l_dtUnits = ni::time_ticks;
				  break;
			  case ENC_UNIT_SEC:
				  l_dtUnits = ni::seconds;
				  break;
        default:
				  l_dtUnits = ni::seconds;
				  break;
		  }
      l_dtConfig.units = l_dtUnits;
      break;

    //********************************************************************/
    // PERIOD COUNTER
    //********************************************************************/
	  case COUNTER_MODE_PERIOD:
		  l_prdConfig.chan_name = std::string("/") + p_cfg.boardName + std::string("/") + std::string("ctr")
			  + yat::XString<size_t>::to_string(p_cfg.number);

		  ni::EdgeType edge;
		  switch (p_cfg.edge)
		  {
		    case COUNTING_EDGE_RISING:
			    edge = ni::rising_edge;
			    break;
		    case COUNTING_EDGE_FALLING:
			    edge = ni::falling_edge;
			    break;
		    default:
			    edge = ni::rising_edge; //default value if not defined
			    break;
		  }
		  l_prdConfig.edge = edge;
		  
      switch (p_cfg.dt_unit)
		  {
			  case ENC_UNIT_TICK:
				  l_dtUnits = ni::time_ticks;
				  break;
			  case ENC_UNIT_SEC:
				  l_dtUnits = ni::seconds;
				  break;
        default:
				  l_dtUnits = ni::seconds;
				  break;
		  }
      l_prdConfig.units = l_dtUnits;
      break;

	  default:
		  break;
	}


  //****************************************************************/
  // Configue counting objects according to acquisition mode
  //****************************************************************/

  if (m_acq_mode == ACQ_MODE_SCAL) // SCALAR acquisition
  {
    if (p_cfg.mode == COUNTER_MODE_POSITION)
    {
      // Create POSITION counting object and configure it
	    try
	    {
        m_scalar_pos_ct_list[p_cfg.name] = new ni660Xsl::SimplePositionMeasurement();

        if (m_scalar_pos_ct_list[p_cfg.name])
        {
          if (p_cfg.pos_encoderMode == ENC_MODE_ANGULAR)
          {
		        m_scalar_pos_ct_list[p_cfg.name]->add_angular_encoder(l_angConfig);
	        }
          else // linear
          {
				    m_scalar_pos_ct_list[p_cfg.name]->add_linear_encoder(l_linConfig);
          }

		      if (p_cfg.minPulseWidthEnabled)
		      {
				    m_scalar_pos_ct_list[p_cfg.name]->set_min_pulse_width(p_cfg.minPulseWidth);
		      }

          m_scalar_pos_ct_list[p_cfg.name]->init();
			    m_scalar_pos_ct_list[p_cfg.name]->configure();
        }
      }
      catch(ni660Xsl::DAQException & nie)
	    {
		    throw_devfailed(nie);
	    }
    }
    else if (p_cfg.mode == COUNTER_MODE_EVENT)
    {
      // Create EVENT counting object and configure it
	    try
	    {
		    m_scalar_evt_ct_list[p_cfg.name] = new ni660Xsl::SimpleEventCounting();

        if (m_scalar_evt_ct_list[p_cfg.name])
        {
	        std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
	        {
		        // Master board: generates clock on ctr0 = trigger
		        pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
	        }
	        else
	        {
		        // Not master board: receives external clock on PFI38
		        pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
	        }

	        if (p_cfg.minPulseWidthEnabled)
	        {
            m_scalar_evt_ct_list[p_cfg.name]->set_min_pulse_width(p_cfg.minPulseWidth);
	        }

		      m_scalar_evt_ct_list[p_cfg.name]->set_pause_trigger(pause_trigger, ni::low);
		      m_scalar_evt_ct_list[p_cfg.name]->add_input_channel(l_evtConfig);
		      m_scalar_evt_ct_list[p_cfg.name]->init();
		      m_scalar_evt_ct_list[p_cfg.name]->configure();  
        }
	    }
	    catch(ni660Xsl::DAQException & nie)
	    {
		    throw_devfailed(nie);
	    }
    }
    else if (p_cfg.mode == COUNTER_MODE_DELTATIME)
    {
      // Create DELTA TIME counting object and configure it
	    try
	    {
		    m_scalar_dt_ct_list[p_cfg.name] = new ni660Xsl::SimpleDTMeasurement();

        if (m_scalar_dt_ct_list[p_cfg.name])
        {
	        std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
	        {
		        // Master board: generates clock on ctr0 = trigger
		        pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
	        }
	        else
	        {
		        // Not master board: receives external clock on PFI38
		        pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
	        }

		      m_scalar_dt_ct_list[p_cfg.name]->set_pause_trigger(pause_trigger, ni::low);
		      m_scalar_dt_ct_list[p_cfg.name]->add_input_channel(l_dtConfig);
		      m_scalar_dt_ct_list[p_cfg.name]->init();
		      m_scalar_dt_ct_list[p_cfg.name]->configure();  
        }
	    }
	    catch(ni660Xsl::DAQException & nie)
	    {
		    throw_devfailed(nie);
	    }
    }
    else if (p_cfg.mode == COUNTER_MODE_PERIOD) 
    {
      // Create PERIOD counting object and configure it
	    try
	    {
		    m_scalar_prd_ct_list[p_cfg.name] = new ni660Xsl::SimplePeriodMeasurement();

        if (m_scalar_prd_ct_list[p_cfg.name])
        {
	        std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
	        {
		        // Master board: generates clock on ctr0 = trigger
		        pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
	        }
	        else
	        {
		        // Not master board: receives external clock on PFI38
		        pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
	        }

		      m_scalar_prd_ct_list[p_cfg.name]->set_pause_trigger(pause_trigger, ni::low);
		      m_scalar_prd_ct_list[p_cfg.name]->add_input_channel(l_prdConfig);
		      m_scalar_prd_ct_list[p_cfg.name]->init();
		      m_scalar_prd_ct_list[p_cfg.name]->configure();  
        }
	    }
	    catch(ni660Xsl::DAQException & nie)
	    {
		    throw_devfailed(nie);
	    }
    }
    else
    {
      // bad counter mode, nothing to do...
    }
  }
  else if (m_acq_mode == ACQ_MODE_BUFF) // BUFFERED acquisition
  {
		BCEconfig l_conf;
		l_conf.acq = m_cfg;
		l_conf.cnt = p_cfg;
		l_conf.hostDevice = m_device_host;

		//- compute internal buffer depth in number of points
		unsigned long buffer_depth_nbpts = 0;
		if (m_cfg.continuousAcquisition)
		{
			buffer_depth_nbpts = m_cfg.samplesNumber;
		}
		else
		{
			buffer_depth_nbpts = static_cast<unsigned long>(m_cfg.bufferDepth);
		}
		INFO_STREAM << "Buffered mode - computed buffer depth in nb of points = " << buffer_depth_nbpts << endl;

    if (p_cfg.mode == COUNTER_MODE_POSITION)
    {
      // Create POSITION counting object and configure it
      try
      {
        m_buffer_pos_ct_list[p_cfg.name] = new BufferedCounterPos(l_conf, m_storage);

        if (m_buffer_pos_ct_list[p_cfg.name])
        {
          if (p_cfg.pos_encoderMode == ENC_MODE_ANGULAR)
          {
				    m_buffer_pos_ct_list[p_cfg.name]->add_angular_encoder(l_angConfig);		       
	        }
          else // linear
          {
				    m_buffer_pos_ct_list[p_cfg.name]->add_linear_encoder(l_linConfig);
          }

				  if (p_cfg.minPulseWidthEnabled)
				  {
						m_buffer_pos_ct_list[p_cfg.name]->set_min_pulse_width(p_cfg.minPulseWidth);
				  }

				  // Define acquisition:
				  m_buffer_pos_ct_list[p_cfg.name]->set_timeout(m_cfg.timeout);
				  m_buffer_pos_ct_list[p_cfg.name]->set_overrun_strategy(ni::notify);
				  m_buffer_pos_ct_list[p_cfg.name]->set_timing_mode(ni::continuous);
				  m_buffer_pos_ct_list[p_cfg.name]->set_buffer_depth(buffer_depth_nbpts);

				  if (p_cfg.memTranfer == MEMORY_TRSF_DMA)
				  {
					  m_buffer_pos_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::dma);
				  }
				  else
				  {
					  m_buffer_pos_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::interrupts);
				  }

					std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
					{
						// Master board: generates clock on ctr0 = trigger
						pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
					}
					else
					{
						// Not master board: receives external clock on PFI38
						pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
					}

				  double max_rate = 1.1 / m_cfg.integrationTime;
				  m_buffer_pos_ct_list[p_cfg.name]->set_sample_clock(pause_trigger, ni::rising_edge, max_rate);

				  if (m_cfg.startTriggerUse)
				  {
					  m_buffer_pos_ct_list[p_cfg.name]->set_start_trigger(pause_trigger, ni::rising_edge);
				  }

#if defined (USE_CALLBACK) 
          DEBUG_STREAM << "Position counter - Callback mode set" << std::endl;
          m_buffer_pos_ct_list[p_cfg.name]->set_callback_mode(true);
          m_buffer_pos_ct_list[p_cfg.name]->set_total_nb_pts(m_cfg.samplesNumber);
#endif
				  m_buffer_pos_ct_list[p_cfg.name]->init();
				  m_buffer_pos_ct_list[p_cfg.name]->configure();
        }
      }
			catch(ni660Xsl::DAQException & nie)
			{
				throw_devfailed(nie);
			}
    }
    else if (p_cfg.mode == COUNTER_MODE_EVENT)
    {
      // Create EVENT counting object and configure it
      try
      {
        m_buffer_evt_ct_list[p_cfg.name] = new BufferedCounterEvt(l_conf, m_storage);

        if (m_buffer_evt_ct_list[p_cfg.name])
        {
					// Define acquisition
					m_buffer_evt_ct_list[p_cfg.name]->set_timeout(m_cfg.timeout);
					m_buffer_evt_ct_list[p_cfg.name]->set_overrun_strategy(ni::notify);
					m_buffer_evt_ct_list[p_cfg.name]->set_timing_mode(ni::continuous);
					m_buffer_evt_ct_list[p_cfg.name]->set_buffer_depth(buffer_depth_nbpts);

					if (p_cfg.memTranfer == MEMORY_TRSF_DMA)
					{
						m_buffer_evt_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::dma);
					}
					else
					{
						m_buffer_evt_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::interrupts);
					}

					if (p_cfg.minPulseWidthEnabled)
					{
						m_buffer_evt_ct_list[p_cfg.name]->set_min_pulse_width(p_cfg.minPulseWidth);
					}

					std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
					{
						// Master board: generates clock on ctr0 = trigger
						pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
					}
					else
					{
						// Not master board: receives external clock on PFI38
						pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
					}

					double max_rate = 1.1 / m_cfg.integrationTime;
					m_buffer_evt_ct_list[p_cfg.name]->set_sample_clock(pause_trigger, ni::rising_edge, max_rate);

					if (m_cfg.startTriggerUse)
					{
						m_buffer_evt_ct_list[p_cfg.name]->set_start_trigger(pause_trigger, ni::rising_edge);
					}

#if defined (USE_CALLBACK) 
				  DEBUG_STREAM << "Event counter - Callback mode set" << std::endl;
          m_buffer_evt_ct_list[p_cfg.name]->set_callback_mode(true);
          m_buffer_evt_ct_list[p_cfg.name]->set_total_nb_pts(m_cfg.samplesNumber);
#endif					

					m_buffer_evt_ct_list[p_cfg.name]->add_input_channel(l_evtConfig);
					m_buffer_evt_ct_list[p_cfg.name]->init();
					m_buffer_evt_ct_list[p_cfg.name]->configure();
        }
      }
			catch(ni660Xsl::DAQException & nie)
			{
				throw_devfailed(nie);
			}
    }
    else if (p_cfg.mode == COUNTER_MODE_DELTATIME)
    {
      // Create DELTA TIME counting object and configure it
      try
      {
        m_buffer_dt_ct_list[p_cfg.name] = new BufferedCounterDt(l_conf, m_storage);

        if (m_buffer_dt_ct_list[p_cfg.name])
        {
					// Define acquisition
					m_buffer_dt_ct_list[p_cfg.name]->set_timeout(m_cfg.timeout);
					m_buffer_dt_ct_list[p_cfg.name]->set_overrun_strategy(ni::notify);
					m_buffer_dt_ct_list[p_cfg.name]->set_timing_mode(ni::continuous);
					m_buffer_dt_ct_list[p_cfg.name]->set_buffer_depth(buffer_depth_nbpts);

					if (p_cfg.memTranfer == MEMORY_TRSF_DMA)
					{
						m_buffer_dt_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::dma);
					}
					else
					{
						m_buffer_dt_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::interrupts);
					}

					std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
					{
						// Master board: generates clock on ctr0 = trigger
						pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
					}
					else
					{
						// Not master board: receives external clock on PFI38
						pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
					}

					double max_rate = 1.1 / m_cfg.integrationTime;
					m_buffer_dt_ct_list[p_cfg.name]->set_sample_clock(pause_trigger, ni::rising_edge, max_rate);

					if (m_cfg.startTriggerUse)
					{
						m_buffer_dt_ct_list[p_cfg.name]->set_start_trigger(pause_trigger, ni::rising_edge);
					}

#if defined (USE_CALLBACK) 
				  DEBUG_STREAM << "DeltaTime counter - Callback mode set" << std::endl;
          m_buffer_dt_ct_list[p_cfg.name]->set_callback_mode(true);
          m_buffer_dt_ct_list[p_cfg.name]->set_total_nb_pts(m_cfg.samplesNumber);
#endif					

					m_buffer_dt_ct_list[p_cfg.name]->add_input_channel(l_dtConfig);
					m_buffer_dt_ct_list[p_cfg.name]->init();
					m_buffer_dt_ct_list[p_cfg.name]->configure();
        }
      }
			catch(ni660Xsl::DAQException & nie)
			{
				throw_devfailed(nie);
			}
    }
    else if (p_cfg.mode == COUNTER_MODE_PERIOD)
    {
      // Create PERIOD counting object and configure it
      try
      {
        m_buffer_prd_ct_list[p_cfg.name] = new BufferedCounterPeriod(l_conf, m_storage);

        if (m_buffer_prd_ct_list[p_cfg.name])
        {
					// Define acquisition
					m_buffer_prd_ct_list[p_cfg.name]->set_timeout(m_cfg.timeout);
					m_buffer_prd_ct_list[p_cfg.name]->set_overrun_strategy(ni::notify);
					m_buffer_prd_ct_list[p_cfg.name]->set_timing_mode(ni::continuous);
					m_buffer_prd_ct_list[p_cfg.name]->set_buffer_depth(buffer_depth_nbpts);

					if (p_cfg.memTranfer == MEMORY_TRSF_DMA)
					{
						m_buffer_prd_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::dma);
					}
					else
					{
						m_buffer_prd_ct_list[p_cfg.name]->set_data_tranfer_mechanism(ni::interrupts);
					}

					std::string pause_trigger;
          // Check board
	        if ((m_clk_cfg.channelName.compare(CLOCK_INTERNAL) == 0) &&
              (m_clk_cfg.boardName.compare(p_cfg.boardName) == 0))
					{
						// Master board: generates clock on ctr0 = trigger
						pause_trigger = "/"  + p_cfg.boardName + "/ctr0InternalOutput";
					}
					else
					{
						// Not master board: receives external clock on PFI38
						pause_trigger = "/"  + p_cfg.boardName + "/PFI38";
					}

					double max_rate = 1.1 / m_cfg.integrationTime;
					m_buffer_prd_ct_list[p_cfg.name]->set_sample_clock(pause_trigger, ni::rising_edge, max_rate);

					if (m_cfg.startTriggerUse)
					{
						m_buffer_prd_ct_list[p_cfg.name]->set_start_trigger(pause_trigger, ni::rising_edge);
					}

#if defined (USE_CALLBACK) 
				  DEBUG_STREAM << "Period counter - Callback mode set" << std::endl;
          m_buffer_prd_ct_list[p_cfg.name]->set_callback_mode(true);
          m_buffer_prd_ct_list[p_cfg.name]->set_total_nb_pts(m_cfg.samplesNumber);
#endif					

					m_buffer_prd_ct_list[p_cfg.name]->add_input_channel(l_prdConfig);
					m_buffer_prd_ct_list[p_cfg.name]->init();
					m_buffer_prd_ct_list[p_cfg.name]->configure();
        }
      }
			catch(ni660Xsl::DAQException & nie)
			{
				throw_devfailed(nie);
			}
    }
    else
    {
      // bad counter mode, nothing to do...
    }
  }
	else
	{
    // bad acquisition mode, nothing to do...
  }
}

// ============================================================================
// NI6602_Interface::startCounter ()
// ============================================================================ 
void NI6602_Interface::startCounter(CounterConfig p_cfg)
{
	try
	{
		switch (m_acq_mode)
		{
		  case ACQ_MODE_SCAL:
			  switch (p_cfg.mode)
			  {
			    case COUNTER_MODE_POSITION:
				    m_scalar_pos_ct_list[p_cfg.name]->start();
				    break;
			    case COUNTER_MODE_EVENT:
				    m_scalar_evt_ct_list[p_cfg.name]->start();
				    break;
			    case COUNTER_MODE_DELTATIME:
				    m_scalar_dt_ct_list[p_cfg.name]->start();
				    break;
			    case COUNTER_MODE_PERIOD:
				    m_scalar_prd_ct_list[p_cfg.name]->start();
				    break;
          default:
				    break;
			  }
			  break;

		  case ACQ_MODE_BUFF:
			  switch (p_cfg.mode)
			  {
			    case COUNTER_MODE_POSITION:
				    m_buffer_pos_ct_list[p_cfg.name]->startCnt();
				    break;
			    case COUNTER_MODE_EVENT:
				    m_buffer_evt_ct_list[p_cfg.name]->startCnt();
				    break;
			    case COUNTER_MODE_DELTATIME:
				    m_buffer_dt_ct_list[p_cfg.name]->startCnt();
				    break;
			    case COUNTER_MODE_PERIOD:
				    m_buffer_prd_ct_list[p_cfg.name]->startCnt();
				    break;
          default:
				    break;
			  }
			  break;

      default:
			  break;
		}
	}
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}
}

// ============================================================================
// NI6602_Interface::stopCounter ()
// ============================================================================ 
void NI6602_Interface::stopCounter(CounterConfig p_cfg)
{
	try
	{
		switch (m_acq_mode)
		{
			case ACQ_MODE_SCAL:
			  switch (p_cfg.mode)
			  {
			    case COUNTER_MODE_POSITION:
            //- from NI660XSl
				    m_scalar_pos_ct_list[p_cfg.name]->stop();
				    m_scalar_pos_ct_list[p_cfg.name]->release();
				    break;
			    case COUNTER_MODE_EVENT:
            //- from NI660XSl
				    m_scalar_evt_ct_list[p_cfg.name]->stop();
				    m_scalar_evt_ct_list[p_cfg.name]->release();
				    break;
			    case COUNTER_MODE_DELTATIME:
            //- from NI660XSl
				    m_scalar_dt_ct_list[p_cfg.name]->stop();
				    m_scalar_dt_ct_list[p_cfg.name]->release();
				    break;
			    case COUNTER_MODE_PERIOD:
            //- from NI660XSl
				    m_scalar_prd_ct_list[p_cfg.name]->stop();
				    m_scalar_prd_ct_list[p_cfg.name]->release();
				    break;
          default:
				    break;
			  }
			  break;

			case ACQ_MODE_BUFF:
				switch (p_cfg.mode)
				{
				  case COUNTER_MODE_POSITION:
					  m_buffer_pos_ct_list[p_cfg.name]->stopCnt();
					  break;
				  case COUNTER_MODE_EVENT:
					  m_buffer_evt_ct_list[p_cfg.name]->stopCnt();
					  break;
				  case COUNTER_MODE_DELTATIME:
					  m_buffer_dt_ct_list[p_cfg.name]->stopCnt();
					  break;
				  case COUNTER_MODE_PERIOD:
					  m_buffer_prd_ct_list[p_cfg.name]->stopCnt();
					  break;
          default:
					  break;
				}
			  break;

      default:
        break;
		}
	}
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}
}

// ============================================================================
// NI6602_Interface::startClock ()
// ============================================================================ 
void NI6602_Interface::startClock()
{
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
		try
		{
			m_scalar_finite_clock->start();
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
		catch(...)
		{
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to start finite clock.",
				"NI6602_Interface::startClock");
		}
	} 
	else if (m_acq_mode == ACQ_MODE_BUFF)
	{
		try
		{
			m_buffer_continuous_clock->start();
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
		catch(...)
		{
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to start continuous clock.",
				"NI6602_Interface::startClock");
		}
	}
	else
	{
		// bad value
    ERROR_STREAM << "Clock cannot be started! Bad acquisition mode: " << m_acq_mode << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR",
			"Failed to start clock - bad acquisition mode!",
			"NI6602_Interface::startClock");
	}
}

// ============================================================================
// NI6602_Interface::abortClock ()
// ============================================================================ 
void NI6602_Interface::abortClock()
{
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
		if (m_scalar_finite_clock)
		{
			try
			{
				m_scalar_finite_clock->abort();
			}
			catch(...)
			{
      			// Bug in DAQmx 9.x: Aborting counter generates exception 
      			// An effective workaround is to catch and clear the error 
			}
		}
	} 
	else if (m_acq_mode == ACQ_MODE_BUFF)
	{
		if (m_buffer_continuous_clock)
		{
			try
			{
				m_buffer_continuous_clock->abort();
			}
			catch(...)
			{
				// Bug in DAQmx 9.x: Aborting counter generates exception 
      			// An effective workaround is to catch and clear the error 
			}
		}
	}
	else
	{
		// bad value
    ERROR_STREAM << "Clock cannot be aborted! Bad acquisition mode: " << m_acq_mode << std::endl;
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR",
      "Failed to abort clock: bad acquisition mode!",
			"NI6602_Interface::abortClock");
	}
}

// ============================================================================
// NI6602_Interface::waitEndOfClock ()
// ============================================================================ 
void NI6602_Interface::waitEndOfClock()
{
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
		try
		{
			m_scalar_finite_clock->wait_finished(-1);
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
		catch(...)
		{
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to wait for finite clock!",
				"NI6602_Interface::waitEndOfClock");
		}
	}
	else if (m_acq_mode == ACQ_MODE_BUFF)
	{
		try
		{
			m_buffer_continuous_clock->wait_finished(-1);
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
		catch(...)
		{
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to wait for continuous clock!",
				"NI6602_Interface::waitEndOfClock");
		}
	}
}

// ============================================================================
// NI6602_Interface::getDriverVersion ()
// ============================================================================ 
std::string NI6602_Interface::getDriverVersion()
{
	std::string l_version;
	try
	{
		l_version = ni660Xsl::SystemProperties::get_driver_version();
	}
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}
	catch(...)
	{
		THROW_DEVFAILED(
			"DEVICE_ERROR",
			"Failed to wait for continuous clock!",
			"NI6602_Interface::getDriverVersion");
	}
	return l_version;
}

// ============================================================================
// NI6602_Interface::getMaxCounterNumber ()
// ============================================================================ 
unsigned int NI6602_Interface::getMaxCounterNumber()
{
	return k_NI66602_MAX_NUMBER_OF_COUNTERS;
}

// ============================================================================
// NI6602_Interface::setIntegrationTime ()
// ============================================================================ 
void NI6602_Interface::setIntegrationTime(double p_time)
{
	m_cfg.integrationTime = p_time;
}

// ============================================================================
// NI6602_Interface::setBufferDepth ()
// ============================================================================ 
void NI6602_Interface::setBufferDepth(long p_size)
{
	m_cfg.bufferDepth = p_size;
}

// ============================================================================
// NI6602_Interface::setSamplesNumber ()
// ============================================================================ 
void NI6602_Interface::setSamplesNumber(unsigned int p_number)
{
	m_cfg.samplesNumber = p_number;
}

// ============================================================================
// NI6602_Interface::setContinuousMode ()
// ============================================================================ 
void NI6602_Interface::setContinuousMode(bool p_continuous)
{
	m_cfg.continuousAcquisition = p_continuous;
}

// ============================================================================
// NI6602_Interface::setAcquisitionTimeout ()
// ============================================================================ 
void NI6602_Interface::setAcquisitionTimeout(double p_time)
{
	m_cfg.timeout = p_time;
}

// ============================================================================
// NI6602_Interface::setStartTriggerUse ()
// ============================================================================ 
void NI6602_Interface::setStartTriggerUse(bool p_use)
{
	m_cfg.startTriggerUse = p_use;
}

// ============================================================================
// NI6602_Interface::setStorageUse ()
// ============================================================================ 
void NI6602_Interface::setStorageUse(bool p_use)
{
  m_cfg.nexusFileGeneration = p_use;
}

// ============================================================================
// NI6602_Interface::getCounterState ()
// ============================================================================ 
Tango::DevState NI6602_Interface::getCounterState(CounterConfig p_cfg)
{
	Tango::DevState l_val = Tango::UNKNOWN;
	int l_state = -1;

  bool l_overrun = false;
  bool l_timedout = false;
  bool l_storageErr = false;


	switch (m_acq_mode)
	{
		case ACQ_MODE_SCAL:
		  switch (p_cfg.mode)
		  {
		    case COUNTER_MODE_POSITION:
			    try
			    {
            if (m_scalar_pos_ct_list[p_cfg.name])
            {
              // get underlying counter state
				      l_state = m_scalar_pos_ct_list[p_cfg.name]->state();
            }
            else
            {
              l_state = ni660Xsl::InputOperation::UNKNOWN;
            }
			    }
			    catch (...)
			    {
				    l_state = -1;
			    }
			    break;
		    case COUNTER_MODE_EVENT:
			    try
			    {
            if (m_scalar_evt_ct_list[p_cfg.name])
            {
              // get underlying counter state
				      l_state = m_scalar_evt_ct_list[p_cfg.name]->state();
            }
            else
            {
              l_state = ni660Xsl::InputOperation::UNKNOWN;
            }
			    }
			    catch (...)
			    {
				    l_state = -1;
			    }
			    break;
		    case COUNTER_MODE_DELTATIME:
			    try
			    {
            if (m_scalar_dt_ct_list[p_cfg.name])
            {
              // get underlying counter state
				      l_state = m_scalar_dt_ct_list[p_cfg.name]->state();
            }
            else
            {
              l_state = ni660Xsl::InputOperation::UNKNOWN;
            }
			    }
			    catch (...)
			    {
				    l_state = -1;
			    }
			    break;
		    case COUNTER_MODE_PERIOD:
			    try
			    {
            if (m_scalar_prd_ct_list[p_cfg.name])
            {
              // get underlying counter state
				      l_state = m_scalar_prd_ct_list[p_cfg.name]->state();
            }
            else
            {
              l_state = ni660Xsl::InputOperation::UNKNOWN;
            }
			    }
			    catch (...)
			    {
				    l_state = -1;
			    }
			    break;
        default:
		      l_state = -1;
			    break;
		  }
		  break;

	case ACQ_MODE_BUFF:
		switch (p_cfg.mode)
		{
		  case COUNTER_MODE_POSITION:
			  try
			  {
          if (m_buffer_pos_ct_list[p_cfg.name])
          {
            // get underlying counter state
				    l_state = m_buffer_pos_ct_list[p_cfg.name]->state();

            // get buffering errors (overrun, timeout or storage error)
            l_overrun = m_buffer_pos_ct_list[p_cfg.name]->isOverrun();
            l_timedout = m_buffer_pos_ct_list[p_cfg.name]->isTimedout();
            l_storageErr = m_buffer_pos_ct_list[p_cfg.name]->isStorageKO();
          }
          else
          {
            l_state = ni660Xsl::InputOperation::UNKNOWN;
          }

			  }
			  catch (...)
			  {
				  l_state = -1;
			  }
			  break;
		  case COUNTER_MODE_EVENT:
			  try
			  {
          if (m_buffer_evt_ct_list[p_cfg.name])
          {
            // get underlying counter state
				    l_state = m_buffer_evt_ct_list[p_cfg.name]->state();

            // get buffering errors (overrun, timeout or storage error)
            l_overrun = m_buffer_evt_ct_list[p_cfg.name]->isOverrun();
            l_timedout = m_buffer_evt_ct_list[p_cfg.name]->isTimedout();
            l_storageErr = m_buffer_evt_ct_list[p_cfg.name]->isStorageKO();
          }
          else
          {
            l_state = ni660Xsl::InputOperation::UNKNOWN;
          }
			  }
			  catch (...)
			  {
				  l_state = -1;
			  }
			  break;
		  case COUNTER_MODE_DELTATIME:
			  try
			  {
          if (m_buffer_dt_ct_list[p_cfg.name])
          {
            // get underlying counter state
				    l_state = m_buffer_dt_ct_list[p_cfg.name]->state();

            // get buffering errors (overrun, timeout or storage error)
            l_overrun = m_buffer_dt_ct_list[p_cfg.name]->isOverrun();
            l_timedout = m_buffer_dt_ct_list[p_cfg.name]->isTimedout();
            l_storageErr = m_buffer_dt_ct_list[p_cfg.name]->isStorageKO();
          }
          else
          {
            l_state = ni660Xsl::InputOperation::UNKNOWN;
          }
			  }
			  catch (...)
			  {
				  l_state = -1;
			  }
			  break;
		  case COUNTER_MODE_PERIOD:
			  try
			  {
          if (m_buffer_prd_ct_list[p_cfg.name])
          {
            // get underlying counter state
				    l_state = m_buffer_prd_ct_list[p_cfg.name]->state();

            // get buffering errors (overrun, timeout or storage error)
            l_overrun = m_buffer_prd_ct_list[p_cfg.name]->isOverrun();
            l_timedout = m_buffer_prd_ct_list[p_cfg.name]->isTimedout();
            l_storageErr = m_buffer_prd_ct_list[p_cfg.name]->isStorageKO();
          }
          else
          {
            l_state = ni660Xsl::InputOperation::UNKNOWN;
          }
			  }
			  catch (...)
			  {
				  l_state = -1;
			  }
			  break;
      default:
			  l_state = -1;
			  break;
		}
		break;

	default:
		break;
	}

	switch (l_state)
	{
	  case ni660Xsl::InputOperation::INIT:
		  l_val = Tango::INIT;
		  break;
	  case ni660Xsl::InputOperation::RUNNING:
		  l_val = Tango::RUNNING;
		  break;
	  case ni660Xsl::InputOperation::STANDBY:
		  l_val = Tango::STANDBY;
		  break;
	  case ni660Xsl::InputOperation::UNKNOWN:
      // TODO state problem: in the NI660XSl lib, the UNKNOWN state is :
      // � either associated to a device error (with exception thrown),
      // � either a normal state, like a STANDBY state (after a release or abort command, for example.
      // For now, we say that it's a normal state => STANDBY
		  l_val = Tango::STANDBY;
		  break;
    default:
      l_val = Tango::FAULT;
      break;
	}

  // set specific state for buffering pbs
  if (l_overrun)
    l_val = Tango::ALARM;

  if (l_timedout)
    l_val = Tango::DISABLE;

  if (l_storageErr)
    l_val = Tango::FAULT;

	return l_val;
}

// ============================================================================
// NI6602_Interface::getCounterScalarValue ()
// ============================================================================ 
data_t NI6602_Interface::getCounterScalarValue(CounterConfig p_cfg)
{
	data_t l_val = 0;

	switch (p_cfg.mode)
	{
	  case COUNTER_MODE_POSITION:
		  try
		  {
			  l_val = m_scalar_pos_ct_list[p_cfg.name]->get_current_scaled_value();
		  }
		  catch (...)
		  {
			  l_val = -1;
		  }
		  break;
	  case COUNTER_MODE_EVENT:
		  try
		  {
			  l_val = m_scalar_evt_ct_list[p_cfg.name]->get_current_raw_value();
		  }
		  catch (...)
		  {
			  l_val = -1;
		  }
		  break;
	  case COUNTER_MODE_DELTATIME:
		  try
		  {
			  l_val = m_scalar_dt_ct_list[p_cfg.name]->get_current_scaled_value();
		  }
		  catch (...)
		  {
			  l_val = -1;
		  }
		  break;
	  case COUNTER_MODE_PERIOD:
		  try
		  {
			  l_val = m_scalar_prd_ct_list[p_cfg.name]->get_current_scaled_value();
		  }
		  catch (...)
		  {
			  l_val = -1;
		  }
		  break;
    default:
		  break;
	}
	return l_val;
}

// ============================================================================
// NI6602_Interface::getCounterBufferValue ()
// ============================================================================ 
RawData_t & NI6602_Interface::getCounterBufferValue(CounterConfig p_cfg)
{
	switch (p_cfg.mode)
	{
	  case COUNTER_MODE_POSITION:
		  if (m_buffer_pos_ct_list[p_cfg.name] != NULL)
		  {
			  return m_buffer_pos_ct_list[p_cfg.name]->get_value();
		  }
		  break;
	  case COUNTER_MODE_EVENT:
		  if (m_buffer_evt_ct_list[p_cfg.name] != NULL)
		  {
		    return m_buffer_evt_ct_list[p_cfg.name]->get_value();
		  }
		  break;
	  case COUNTER_MODE_DELTATIME:
		  if (m_buffer_dt_ct_list[p_cfg.name] != NULL)
		  {
		    return m_buffer_dt_ct_list[p_cfg.name]->get_value();
		  }
		  break;
	  case COUNTER_MODE_PERIOD:
		  if (m_buffer_prd_ct_list[p_cfg.name] != NULL)
		  {
		    return m_buffer_prd_ct_list[p_cfg.name]->get_value();
		  }
		  break;
    default:
      THROW_DEVFAILED(
         "CONFIGURATION_ERROR", 
			   "Bad counter mode!", 
			   "NI6602_Interface::getCounterBufferValue"); 
		  break;	}

  // if not defined pters (before a start command)
  THROW_DEVFAILED(
   "DEVICE_ERROR", 
   "Not initialized counter!", 
   "NI6602_Interface::getCounterBufferValue");
}

// ============================================================================
// NI6602_Interface::updateCounterBufferValue ()
// ============================================================================ 
void NI6602_Interface::updateCounterBufferValue(CounterConfig p_cfg)
{
	switch (p_cfg.mode)
	{
	  case COUNTER_MODE_POSITION:
		  if (m_buffer_pos_ct_list[p_cfg.name] != NULL)
		  {
			  m_buffer_pos_ct_list[p_cfg.name]->updateScaledBuffer();
		  }
		  break;
	  case COUNTER_MODE_EVENT:
		  if (m_buffer_evt_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_evt_ct_list[p_cfg.name]->updateRawBuffer();
		  }
		  break;
	  case COUNTER_MODE_DELTATIME:
		  if (m_buffer_dt_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_dt_ct_list[p_cfg.name]->updateScaledBuffer();
		  }
		  break;
	  case COUNTER_MODE_PERIOD:
		  if (m_buffer_prd_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_prd_ct_list[p_cfg.name]->updateScaledBuffer();
		  }
		  break;
    default:
		  break;
	}
}

// ============================================================================
// NI6602_Interface::releaseClock ()
// ============================================================================ 
void NI6602_Interface::releaseClock()
{
	// SCALAR mode
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
		if (m_scalar_finite_clock)
		{
			// stop & release clock object
			try
			{
				m_scalar_finite_clock->abort();
				m_scalar_finite_clock->release();
			}
			catch(...)
			{
			}

			// delete clock object
			delete m_scalar_finite_clock;
			m_scalar_finite_clock = NULL;
		}
	}
	else
	{
		// BUFFERED mode
		if (m_buffer_continuous_clock)
		{
			// stop & release clock object
			try
			{
				m_buffer_continuous_clock->abort();
				m_buffer_continuous_clock->release();
			}
			catch(...)
			{
			}

			// delete clock object
			delete m_buffer_continuous_clock;
			m_buffer_continuous_clock = NULL;
		}
	}
}

// ============================================================================
// NI6602_Interface::releaseCounter ()
// ============================================================================ 
void NI6602_Interface::releaseCounter(CounterConfig p_cfg)
{
	// SCALAR mode
	if (m_acq_mode == ACQ_MODE_SCAL)
	{
	  switch (p_cfg.mode)
	  {
	    case COUNTER_MODE_POSITION:
		    if (m_scalar_pos_ct_list[p_cfg.name])
		    {
			    m_scalar_pos_ct_list[p_cfg.name]->stop();
			    m_scalar_pos_ct_list[p_cfg.name]->release();

			    delete m_scalar_pos_ct_list[p_cfg.name];
			    m_scalar_pos_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_EVENT:
		    if (m_scalar_evt_ct_list[p_cfg.name])
		    {
			    m_scalar_evt_ct_list[p_cfg.name]->stop();
			    m_scalar_evt_ct_list[p_cfg.name]->release();

			    delete m_scalar_evt_ct_list[p_cfg.name];
			    m_scalar_evt_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_DELTATIME:
		    if (m_scalar_dt_ct_list[p_cfg.name])
		    {
			    m_scalar_dt_ct_list[p_cfg.name]->stop();
			    m_scalar_dt_ct_list[p_cfg.name]->release();

			    delete m_scalar_dt_ct_list[p_cfg.name];
			    m_scalar_dt_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_PERIOD:
		    if (m_scalar_prd_ct_list[p_cfg.name])
		    {
			    m_scalar_prd_ct_list[p_cfg.name]->stop();
			    m_scalar_prd_ct_list[p_cfg.name]->release();

			    delete m_scalar_prd_ct_list[p_cfg.name];
			    m_scalar_prd_ct_list[p_cfg.name] = NULL;
		    }
		    break;
      default:
		    break;
	  }
  }
  else
  {
    // BUFFERED mode
	  switch (p_cfg.mode)
	  {
	    case COUNTER_MODE_POSITION:
		    if (m_buffer_pos_ct_list[p_cfg.name])
		    {
			    m_buffer_pos_ct_list[p_cfg.name]->stopCnt();

			    delete m_buffer_pos_ct_list[p_cfg.name];
			    m_buffer_pos_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_EVENT:
		    if (m_buffer_evt_ct_list[p_cfg.name])
		    {
			    m_buffer_evt_ct_list[p_cfg.name]->stopCnt();

			    delete m_buffer_evt_ct_list[p_cfg.name];
			    m_buffer_evt_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_DELTATIME:
		    if (m_buffer_dt_ct_list[p_cfg.name])
		    {
			    m_buffer_dt_ct_list[p_cfg.name]->stopCnt();

			    delete m_buffer_dt_ct_list[p_cfg.name];
			    m_buffer_dt_ct_list[p_cfg.name] = NULL;
		    }
		    break;
	    case COUNTER_MODE_PERIOD:
		    if (m_buffer_prd_ct_list[p_cfg.name])
		    {
			    m_buffer_prd_ct_list[p_cfg.name]->stopCnt();

			    delete m_buffer_prd_ct_list[p_cfg.name];
			    m_buffer_prd_ct_list[p_cfg.name] = NULL;
		    }
		    break;
      default:
		    break;
	  }
  }
}

// ============================================================================
// NI6602_Interface::updateCounterLastBufferValue ()
// ============================================================================ 
void NI6602_Interface::updateCounterLastBufferValue(CounterConfig p_cfg)
{
	switch (p_cfg.mode)
	{
	  case COUNTER_MODE_POSITION:
		  if (m_buffer_pos_ct_list[p_cfg.name] != NULL)
		  {
			  m_buffer_pos_ct_list[p_cfg.name]->updateLastScaledBuffer();
		  }
		  break;
	  case COUNTER_MODE_EVENT:
		  if (m_buffer_evt_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_evt_ct_list[p_cfg.name]->updateLastRawBuffer();
		  }
		  break;
	  case COUNTER_MODE_DELTATIME:
		  if (m_buffer_dt_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_dt_ct_list[p_cfg.name]->updateLastScaledBuffer();
		  }
		  break;
	  case COUNTER_MODE_PERIOD:
		  if (m_buffer_prd_ct_list[p_cfg.name] != NULL)
		  {
        m_buffer_prd_ct_list[p_cfg.name]->updateLastScaledBuffer();
		  }
		  break;
    default:
		  break;
	}
}

} // namespace PulseCounting_ns