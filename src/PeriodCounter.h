//=============================================================================
// PeriodCounter.h
//=============================================================================
// abstraction.......PeriodCounter for PulseCounting
// class.............PeriodCounter
// original author...S.Minolli - Nexeya
//=============================================================================

#ifndef _PERIOD_COUNTER_H
#define _PERIOD_COUNTER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingTypesAndConsts.h"
#include "GenericCounterInterface.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: PeriodCounter
// ============================================================================
class PeriodCounter  : public GenericCounterInterface
{

public:

  //- constructor
  PeriodCounter (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board);

  //- destructor
  virtual ~PeriodCounter ();
  
  //- is available?
  bool is_available();

  //- init
  void init(CounterConfig p_cfg);

  //- configure
  void configure(CounterConfig p_cfg);

  //- start
  void start();

  //- stop
  void stop();

  //- release counter
  void deleteObject();

  //- get State
  Tango::DevState get_state();
	  
  //- update scalar value
  void update_scalar_value();
	  
  //- update buffer value (used in "polling" mode)
  void update_buffer_value();
	  
  //- force last incomplete buffer update (used with trigger listener option)
  void update_last_buffer_value();

  //- get scalar value
  data_t get_scalar_value();
	  
  //- get buffer value
  RawData_t get_buffer_value();

  //- get name
  std::string get_name();

  //- get config
  CounterConfig get_config();
  
protected:
};

} // namespace PulseCounting_ns

#endif // _PERIOD_COUNTER_H
