//=============================================================================
// BufferedCounterDt.cpp
//=============================================================================
// abstraction.......BufferedCounterDt for PulseCounting
// class.............BufferedCounterDt
// original author...S.Minolli - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "BufferedCounterDt.h"

namespace PulseCounting_ns
{

// ============================================================================
// BufferedCounterDt::BufferedCounterDt ()
// ============================================================================ 
BufferedCounterDt::BufferedCounterDt (BCEconfig p_conf, NexusManager * storage)
: yat4tango::TangoLogAdapter(p_conf.hostDevice)
{
	m_bufferNbToReceive = 0;
	m_currentBufferNb = 0;
	m_firstPtReceivedBuff = 0;
	m_acquisitionDone = true;
	m_dataBuffer_thread = NULL;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;
  m_data.clear();
	m_cfg = p_conf;
  m_storage_mgr = storage;
}

// ============================================================================
// BufferedCounterDt::~BufferedCounterDt ()
// ============================================================================ 
BufferedCounterDt::~BufferedCounterDt ()
{
	
}

// ============================================================================
// BufferedCounterDt::updateScaledBuffer
// ============================================================================
void BufferedCounterDt::updateScaledBuffer()
{
	// Delete "buffer recover" thread if exists and if data recovery is over
	if (m_dataBuffer_thread)
	{
		if (m_dataBuffer_thread->updateDone())
		{
			yat::Thread::IOArg ioa;
			m_dataBuffer_thread->exit();
			m_dataBuffer_thread->join(&ioa);
			//std::cout << "BufferedCounterDt::updateScaledBuffer - BR thread exited for counter " 
			//  << m_cfg.cnt.number << std::endl;
			m_dataBuffer_thread = NULL; 
		}
	}

	// Task the buffer reception if acquisition is on & if previous buffer recovery is over
	if ((!m_acquisitionDone) && 
		  (m_dataBuffer_thread == NULL))
	{
		m_dataBuffer_thread = new BRDThread(m_cfg.hostDevice, static_cast<yat::Thread::IOArg>(this));
		DEBUG_STREAM << "BufferedCounterDt Thread created ..." << endl;
		if (m_dataBuffer_thread == NULL)
		{
			ERROR_STREAM << "Buffer recovery cannot be tasked for counter " 
				<< m_cfg.cnt.number << std::endl;
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to start buffer recover task!",
				"BufferedCounterDt::updateScaledBuffer");    
		}
		//std::cout << "BufferedCounterDt::updateScaledBuffer - starting Buffer recovery thread for counter " 
		//  << m_cfg.cnt.number << std::endl;
		m_dataBuffer_thread->start_undetached();   
	}
}

// ============================================================================
// BufferedCounterDt::getScaledBuffer
// ============================================================================
void BufferedCounterDt::getScaledBuffer()
{
	// call "get scaled buffer" to compose the whole buffer.
	// In "polling" mode, the client has to poll to compose the acquisition buffer.
	if (!m_acquisitionDone)
	{
		DEBUG_STREAM << "BufferedCounterDt::getScaledBuffer entering..." << endl;
		//- from NI660Xsl - calls ni660Xsl::handle_xxx methods
		try
		{
			ni660Xsl::BufferedDTMeasurement::get_scaled_buffer(); // the driver waits 'buffer depth' seconds...
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
	}
}

// ============================================================================
// BufferedCounterDt::handle_timeout ()
// ============================================================================ 
void BufferedCounterDt::handle_timeout()
{
	ERROR_STREAM << "BufferedCounterDt::handle_timeout() entering..." << endl;
	m_cntTimedout = true;
}

// ============================================================================
// BufferedCounterDt::handle_data_lost ()
// ============================================================================ 
void BufferedCounterDt::handle_data_lost()
{
	ERROR_STREAM << "BufferedCounterDt::handle_data_lost() entering..." << endl;
	m_cntOverrun = true;
}

// ============================================================================
// BufferedCounterDt::handle_raw_buffer ()
// ============================================================================ 
void BufferedCounterDt::handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read)
{
  //- DT values are not raw !!!
  delete buffer;
}

// ============================================================================
// BufferedCounterDt::handle_scaled_buffer ()
// ============================================================================ 
void BufferedCounterDt::handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read)
{
  DEBUG_STREAM << "BufferedCounterDt::handle_scaled_buffer() entering..." << std::endl;
	DEBUG_STREAM << "BufferedCounterDt::handle_scaled_buffer() - samples to read = " << _samples_read << std::endl;

  // check samples number is > 0
  if (_samples_read <= 0)
  {
    ERROR_STREAM << "BufferedCounterDt::handle_scaled_buffer-> samples to read negative or null value!" << std::endl;
    stopCnt();
	return;
  }	
	yat::AutoMutex<> guard(m_buffLock);

	ni660Xsl::InScaledBuffer& buf = *buffer;
	RawData_t & data = m_data;
  bool data_to_be_stored = (m_cfg.acq.nexusFileGeneration && m_cfg.cnt.nexus);

	//- Continuous acquisition: only one buffer is received !!
	if (m_cfg.acq.continuousAcquisition)
	{
		DEBUG_STREAM << "continuous acquisition: nb buffer to receive = 1!" << std::endl;  

		for (unsigned long idx = 0; idx < (unsigned long)_samples_read; idx++)
		{
			data[idx] = (data_t)(buf[idx]);
		}

    // store received buffer in Nexus, if nx storage enabled
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &data[0], (unsigned long)_samples_read);
      }
      catch (Tango::DevFailed & df)
      {
	      ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
	      ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }

    // stop acquisition
		stopCnt();
	}
  else if (m_bufferNbToReceive == 0) //- infinite mode
  {
		DEBUG_STREAM << "one buffer received." << std::endl;

		unsigned long nb_to_copy;
		nb_to_copy = (unsigned long)_samples_read;
		DEBUG_STREAM << "NB to copy : " << nb_to_copy << endl;

		for (unsigned long idx = 0; idx < nb_to_copy; idx++ )
		{
			data[idx] = (data_t)(buf[idx]);
		}

    // store received buffer in Nexus, if nx storage enabled for this counter
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &data[0], nb_to_copy);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
        ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }
  }
	else //- not continuous nor infinite
	{
		//- All buffers have not been yet received
		if (m_currentBufferNb < m_bufferNbToReceive)
		{
			DEBUG_STREAM << "buffer currently received = " << m_currentBufferNb + 1 << " on " << m_bufferNbToReceive << std::endl;
			unsigned long nb_to_copy;

			if (((unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff) < (unsigned long)_samples_read)
			{
				nb_to_copy = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
			}
			else
			{
				nb_to_copy = (unsigned long)_samples_read;
			}
			DEBUG_STREAM << "NB to copy : " << nb_to_copy << endl;

			// add number to copy to current data
			data.capacity(data.length() + nb_to_copy, true);
			data.force_length(data.length() + nb_to_copy);

      // tempo buffer for storage
      RawData_t bufNx;
      if (data_to_be_stored)
      {
        bufNx.capacity(nb_to_copy);
        bufNx.force_length(nb_to_copy);
      }

			for (unsigned long idx = 1; idx < nb_to_copy; idx++ )
			{
				data[idx + m_firstPtReceivedBuff] = (data_t)(buf[idx]);
        if (data_to_be_stored)
          bufNx[idx] = data[idx + m_firstPtReceivedBuff];
			}

			//- at start, m_firstPtReceivedBuff equals 0.
			//- m_firstPtReceivedBuff is the position of the 1st point of the buffer
			data[m_firstPtReceivedBuff] = (data_t)(buf[0]);
      if (data_to_be_stored)
        bufNx[0] = data[m_firstPtReceivedBuff];

			m_currentBufferNb++;
			m_firstPtReceivedBuff += nb_to_copy;

      // store received buffer in Nexus, if nx storage enabled for this counter
      if (data_to_be_stored && m_storage_mgr)
      {
        try
        {
          m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &bufNx[0], nb_to_copy);
        }
        catch (Tango::DevFailed & df)
        {
	        ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
        catch (...)
        {
	        ERROR_STREAM << "BufferedCounterDt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
      }
		}

		// if all requested buffers are received 
		if (m_currentBufferNb == m_bufferNbToReceive)
		{	
			// stop acquisition
			stopCnt();
		}
	}

  delete buffer;
}

// ============================================================================
// BufferedCounterDt::startCnt
// ============================================================================
void BufferedCounterDt::startCnt()
{
	DEBUG_STREAM << "BufferedCounterDt::startCnt() entering..." << endl;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;

	{
		yat::AutoMutex<> guard(m_buffLock);

    // reset current counters & buffers
		m_currentBufferNb = 0;
		m_firstPtReceivedBuff = 0;
		m_acquisitionDone = false;

    // check if infinite mode
    if (m_cfg.acq.samplesNumber == 0)
    {
      // set data length & capacity to buffer depth
	    m_data.capacity(m_cfg.acq.bufferDepth);
	    m_data.force_length(m_cfg.acq.bufferDepth);
    }
    else
    {
      // check if continuous mode
      if (m_cfg.acq.continuousAcquisition)
      {
        // set data length & capacity to samples nber
	      m_data.capacity(m_cfg.acq.samplesNumber);
	      m_data.force_length(m_cfg.acq.samplesNumber);
      }
      else
      {
        // set data capacity & length to 0
	      m_data.capacity(0);
	      m_data.force_length(0);
      }
    }
	  m_data.fill(yat::IEEE_NAN);

		unsigned long buffer_depth_nbpts = 0;

		if (m_cfg.acq.continuousAcquisition)
		{
			buffer_depth_nbpts = m_cfg.acq.samplesNumber;
		}
		else
		{
			buffer_depth_nbpts = static_cast<unsigned long>(m_cfg.acq.bufferDepth);
		}
		m_bufferNbToReceive = (m_cfg.acq.samplesNumber + buffer_depth_nbpts - 1) / buffer_depth_nbpts;
		DEBUG_STREAM << "Buffer to receive : " << m_bufferNbToReceive << endl;
	}

	//- from NI660Xsl
  try
  {
	  ni660Xsl::BufferedDTMeasurement::start();
  }
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}

#if !defined (USE_CALLBACK) 
	// start the data buffer waiting task for the 1st time
	updateScaledBuffer();
#endif
}

// ============================================================================
// BufferedCounterDt::stopCnt
// ============================================================================
void BufferedCounterDt::stopCnt()
{
  DEBUG_STREAM << "BufferedCounterDt::stopCnt() entering..." << endl;
	m_acquisitionDone = true;

	try
	{
		//- from NI660Xsl
		// we use abort to stop counting immediately
		// sets an UNKNOWN state in NI660Xsl lib !
		ni660Xsl::BufferedDTMeasurement::abort_and_release();
	}
	catch(ni660Xsl::DAQException & nie)
	{
		if (nie.errors[0].code > 0)
		{
			WARN_STREAM << "BufferedCounterDt::stopCnt() - Trying to abort during buffer acquisition, retry once..." << std::endl;
			ni660Xsl::BufferedDTMeasurement::abort_and_release();
		}
		else
		{
			// Bug in DAQmx 9.x: Aborting counter generates exception 
			// An effective workaround is to catch and clear the error 
		}
	}
	catch(...)
	{
		// One more time:
		// Bug in DAQmx 9.x: Aborting counter generates exception 
		// An effective workaround is to catch and clear the error 
	}
}

// ============================================================================
// BufferedCounterDt::get_value ()
// ============================================================================ 
RawData_t & BufferedCounterDt::get_value()
{
  yat::AutoMutex<> guard(m_buffLock);
  return m_data;
}

// ============================================================================
// BufferedCounterDt::updateLastScaledBuffer
// ============================================================================
void BufferedCounterDt::updateLastScaledBuffer()
{
  // call "get last scaled buffer" to compose the whole buffer.
  // With trigger listener option, the device has to force the reading of the last
  // incomplete buffer to get the final data.
  DEBUG_STREAM << "BufferedCounterDt::updateLastScaledBuffer ..." << endl;

  // wait a little to be sure that the driver has received the last point
  yat::Thread::sleep(kLAST_BUFFER_DELAY_MS);

  //- from NI660Xsl - calls ni660Xsl::handle_xxx methods
  try
  {
    unsigned long expected_samples_nb = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
	ni660Xsl::BufferedDTMeasurement::get_last_scaled_buffer(expected_samples_nb);
  }
  catch(ni660Xsl::DAQException & nie)
  {
    throw_devfailed(nie);
  }
}


//*****************************************************************************
// BRDThread
//*****************************************************************************
// ============================================================================
// BRDThread::BRDThread
// ============================================================================
BRDThread::BRDThread (Tango::DeviceImpl * hostDevice, yat::Thread::IOArg ioa)
: yat::Thread(ioa),
Tango::LogAdapter (hostDevice),
m_goOn(true),
m_isUpdateDone(false)
{
	//- noop ctor
}

// ============================================================================
// BRDThread::~BRDThread
// ============================================================================
BRDThread::~BRDThread (void)
{
	//- noop dtor
}

// ============================================================================
// BRDThread::run_undetached
// ============================================================================
yat::Thread::IOArg BRDThread::run_undetached (yat::Thread::IOArg ioa)
{
	DEBUG_STREAM << "BRDThread::run_undetached() entering... " << std::endl;

	m_isUpdateDone = false;
	//- get ref. to out our parent task
	BufferedCounterDt * cm_task = reinterpret_cast<BufferedCounterDt *>(ioa);

	//std::cout << "**Wait for buffer value to be set from board..." << std::endl;
	cm_task->getScaledBuffer(); 

	// when function returns, the buffer is handled (or timeout if no data)
	//std::cout << "**Buffer update done." << std::endl;
	m_isUpdateDone = true;

	return 0;
}

// ============================================================================
// BRDThread::exit
// ============================================================================
void BRDThread::exit (void)
{
	m_goOn = false;
}

} // namespace PulseCounting_ns

