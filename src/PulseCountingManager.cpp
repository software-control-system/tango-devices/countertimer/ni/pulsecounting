//=============================================================================
// PulseCountingManager.cpp
//=============================================================================
// abstraction.......PulseCountingManager for PulseCounting
// class.............PulseCountingManager
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingManager.h"
#include "CountingBoardFactory.h"
#include "CounterFactory.h"

namespace PulseCounting_ns
{
	//- Check nexus manager macro:
#define CHECK_NX_MANAGER() \
	if (! m_nexus_manager) \
	{ \
	m_status = "Internal error."; \
	THROW_DEVFAILED("INTERNAL_ERROR", \
	"request aborted - the Nexus manager isn't properly initialized", \
	"PulseCountingManager::check_nx_manager"); \
} while (0)

	//- check macro:
#define CHECK_MANAGER_DYN_ATTR_MANAGER() \
	do \
	{ \
	if (! m_dyn_attr_manager)\
	THROW_DEVFAILED("DEVICE_ERROR", \
	"The dynamic attribute manager object isn't accessible ", \
	"PulseCountingManager::check_manager_dyn_attr_manager"); \
} while (0)

 //- check counting boerd macro:
#define CHECK_CNT_BOARD() \
	do \
	{ \
	if (! m_counting_board) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the counting board isn't accessible ", \
	"PulseCountingManager::check_counting_board"); \
} while (0)

// ============================================================================
// PulseCountingManager::PulseCountingManager ()
// ============================================================================ 
PulseCountingManager::PulseCountingManager (Tango::DeviceImpl * hostDevice)
: yat4tango::DeviceTask(hostDevice)
{
	//- trace/profile this method
	yat4tango::TraceHelper t("PulseCountingManager::PulseCountingManager", this);

	set_timeout_msg_period (0xFFFF);
	enable_timeout_msg (false);

	set_periodic_msg_period (0xFFFF);
	enable_periodic_msg (false);

	m_dyn_attr_manager = NULL;
	m_nexus_manager = NULL;
	m_clock_generator = NULL;
	m_counting_board = NULL;
	m_state = Tango::INIT;
	m_status = "Unknown";

  //- init flags
  m_requestedStop = false;
  m_acq_running = false;
  m_firstStart = true;
  m_isHwInitNeeded = true;

	try
	{
		m_dyn_attr_manager = new yat4tango::DynamicAttributeManager(hostDevice);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to create Dynamic Attribute Manager", 
			"PulseCountingManager::PulseCountingManager"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to create Dynamic Attribute Manager" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to create Dynamic Attribute Manager", 
			"PulseCountingManager::PulseCountingManager"); 
	}
}

// ============================================================================
// PulseCountingManager::~PulseCountingManager ()
// ============================================================================ 
PulseCountingManager::~PulseCountingManager ()
{
	enable_periodic_msg(false);

  // delete clock generator if exists
  if (m_clock_generator)
  {
    delete m_clock_generator;
    m_clock_generator = NULL;
  }

  // delete counting board
  if (m_counting_board)
  {
    delete m_counting_board;
    m_counting_board = NULL;
  }

  // delete counters
  GenericCounterMap_t::iterator l_it;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
    if (l_it->second)
    {
      delete (l_it->second);
      l_it->second = NULL;
    }
  }

  if (m_nexus_manager)
  {
    delete m_nexus_manager;
    m_nexus_manager = NULL;
  }

  if (m_dyn_attr_manager)
  {
    delete m_dyn_attr_manager;
    m_dyn_attr_manager = NULL;
  }
}

// ============================================================================
// PulseCountingManager::get_state ()
// ============================================================================ 
Tango::DevState PulseCountingManager::get_state()
{
  yat::AutoMutex<> guard(m_flagLock);
	return m_state;
}

// ============================================================================
// PulseCountingManager::get_status ()
// ============================================================================ 
std::string PulseCountingManager::get_status()
{
  yat::AutoMutex<> guard(m_flagLock);
	return m_status;
}

// ============================================================================
// PulseCountingManager::init ()
// ============================================================================ 
void PulseCountingManager::init(BoardArchitecture p_board_arch)
{
	INFO_STREAM << "PulseCountingManager::init" << endl;
	m_state = Tango::STANDBY;
	m_status = "Device is up and ready.";
	m_board_arch = p_board_arch;

  // Create nexus manager for data storage
	m_nexus_manager = new NexusManager(m_board_arch.hostDevice);

	if (!m_nexus_manager)
	{
		m_status = "Internal error. Problem occurred during Nexus manager allocation.";

		ERROR_STREAM << "Problem occurred during Nexus manager allocation" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Problem during Nexus manager allocation", 
			"PulseCountingManager::init");
	}

	//construct all the objects depending on the configuration
	try
	{
		m_counting_board = CountingBoardFactory::instanciate(m_board_arch.hostDevice, m_board_arch.boardType, 
      m_acq_param.acqMode, m_nexus_manager); 
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to create counting board", 
			"PulseCountingManager::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to create counting board" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to create counting board", 
			"PulseCountingManager::init"); 
	}

	if (!m_counting_board)
	{
		ERROR_STREAM << "Failed to access counting board" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to access counting board", 
			"PulseCountingManager::init"); 
	}

  // Create acquisition attributes if internal clock generation
  if (m_board_arch.clockGeneration)
  {
    try
    {
      add_common_attributes();
    }
	  catch (Tango::DevFailed &e)
	  {
		  ERROR_STREAM << e << std::endl;
		  RETHROW_DEVFAILED(e, 
			  "DEVICE_ERROR", 
			  "Failed to create acquisition attributes!", 
			  "PulseCountingManager::init"); 
	  }
	  catch (...)
	  {
		  ERROR_STREAM << "Failed to init counter" << std::endl;
		  THROW_DEVFAILED(
			  "DEVICE_ERROR", 
			  "Failed to create acquisition attributes!", 
			  "PulseCountingManager::init"); 
	  }
  }

  // Create & initialize counters
	try
	{
		counterConfigMap_t::iterator l_it;
		for (l_it = m_board_arch.counterConfiguration.begin(); l_it != m_board_arch.counterConfiguration.end(); ++l_it)
		{
			CounterConfig l_cfg = l_it->second;			
			std::string prop_name = std::string("__") + l_cfg.name + PULSE_WIDTH;
			double val = 0.0;
			try
			{
				val = get_value_as_property<double>(m_board_arch.hostDevice, prop_name);
				l_cfg.minPulseWidth = val;
			}
			catch(...)
			{
				// no value in db, use default value
				l_cfg.minPulseWidth = 0.0;
			}

			prop_name = std::string("__") + l_cfg.name + PULSE_WIDTH_ENABLED;
			bool tof = false;
			try
			{
				tof = get_value_as_property<bool>(m_board_arch.hostDevice, prop_name);
				l_cfg.minPulseWidthEnabled = tof;
			}
			catch(...)
			{
				// no value in db, use default value
				l_cfg.minPulseWidthEnabled = false;
			}

			prop_name = std::string("__") + DATASET_ENABLED + l_cfg.name;
			tof = false;
			try
			{
				tof = get_value_as_property<bool>(m_board_arch.hostDevice, prop_name);
				l_cfg.nexus = tof;
			}
			catch(...)
			{
        // OBSOLETE - to be deleted in NEXT version ->
        // l_cfg.nexus already updated in get_device_property
        DEBUG_STREAM << "PulseCountingManager::init() - no memorized value found for " << prop_name << std::endl;
        DEBUG_STREAM << "Initialize from property Nexus:" << l_cfg.nexus << std::endl;
        // memorize this value
        yat4tango::PropertyHelper::set_property<bool>(m_board_arch.hostDevice, prop_name, l_cfg.nexus);
        // <- end of OBSOLETE code

        // code to be enabled in NEXT version:
        /*
        // no value in db, use default value
				l_cfg.nexus = false;
        */
			}

      m_counterList[l_cfg.ctNumber] = CounterFactory::instanciate(m_board_arch.hostDevice, l_cfg , m_acq_param.acqMode, m_counting_board);
			m_counterListConfig[m_counterList[l_cfg.ctNumber]] = l_cfg;

			try
			{
				m_counterList[l_cfg.ctNumber]->init(l_cfg);
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, 
					"DEVICE_ERROR", 
					"Failed to init counter", 
					"PulseCountingManager::init"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to init counter" << std::endl;
				THROW_DEVFAILED(
					"DEVICE_ERROR", 
					"Failed to init counter", 
					"PulseCountingManager::init"); 
			}
		}
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
			"DEVICE_ERROR", 
			"Failed to instantiate counter", 
			"PulseCountingManager::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to instantiate counter" << std::endl;
		THROW_DEVFAILED(
			"DEVICE_ERROR", 
			"Failed to instantiate counter", 
			"PulseCountingManager::init"); 
	}

  // check counters validity
	counterConfigMap_t::iterator l_it;
  for (l_it = m_board_arch.counterConfiguration.begin(); l_it != m_board_arch.counterConfiguration.end(); ++l_it)
	{
		CounterConfig l_cfg = l_it->second;
		bool l_is_available = m_counterList[l_cfg.ctNumber]->is_available();
    if (!l_is_available)
		{
      ERROR_STREAM << "Counter not available: " << l_cfg.number << std::endl;
			THROW_DEVFAILED(
				"DEVICE_ERROR", 
				"The counter is not available!", 
				"PulseCountingManager::init"); 
		}

    // Create the associated dynamic attributes (except counter value)
		try
		{
			add_attributes(l_cfg);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
				"DEVICE_ERROR", 
				"Failed to add attributes", 
				"PulseCountingManager::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to add attributes" << std::endl;
			THROW_DEVFAILED(
				"DEVICE_ERROR", 
				"Failed to add attributes", 
				"PulseCountingManager::init"); 
		}
	}

	// Create internal clock generator if necessary
	if (m_board_arch.clockGeneration)
	{
		try
		{
			// clock creation
			m_clock_generator = new ClockGenerator(m_board_arch.hostDevice, m_counting_board);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
				"DEVICE_ERROR", 
				"Failed to create clock generator", 
				"PulseCountingManager::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to create clock generator" << std::endl;
			THROW_DEVFAILED(
				"DEVICE_ERROR", 
				"Failed to create clock generator", 
				"PulseCountingManager::init"); 
		}

    // check clock
		if (!m_clock_generator)
		{
			ERROR_STREAM << "Failed to create clock generator" << std::endl;
			THROW_DEVFAILED(
				"DEVICE_ERROR", 
				"Failed to create clock generator", 
				"PulseCountingManager::init"); 
		}
	}
  else
  {
    // create trigger listener in external clock case, if
    // associated properties are defined
    if (m_board_arch.udp_address.size() && (m_board_arch.udp_port != 0))
    {
      configure_trigger_listener(m_board_arch.udp_address, m_board_arch.udp_port);
    }
    else
    {
      DEBUG_STREAM << "UDP listener option not enabled." << std::endl;
    }
  }

  INFO_STREAM << "PulseCountingManager::init OK" << endl;
}

// ============================================================================
// PulseCountingManager::start ()
// ============================================================================ 
void PulseCountingManager::start()
{
  // initialize flags
  {
    yat::AutoMutex<> guard(m_flagLock);
    m_firstStart = true;
    m_requestedStop = false;
  }

	yat::Message * msg = yat::Message::allocate(kSTART_MSG, DEFAULT_MSG_PRIORITY, true);

    // force internal state to RUNNING because configuring board may take a long time
	wait_msg_handled(msg, 5000);
	{
	  yat::AutoMutex<> guard(m_flagLock);
      m_state = Tango::RUNNING;
	}
}

// ============================================================================
// PulseCountingManager::stop ()
// ============================================================================ 
void PulseCountingManager::stop()
{
	yat::Message * msg = yat::Message::allocate(kSTOP_MSG, DEFAULT_MSG_PRIORITY, true);
	wait_msg_handled(msg, 2500);
}

// ============================================================================
// PulseCountingManager::get_driver_version ()
// ============================================================================ 
std::string PulseCountingManager::get_driver_version()
{
	std::string l_driver_version;
	
  CHECK_CNT_BOARD();

	try
	{
		l_driver_version = m_counting_board->getDriverVersion();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
			"DEVICE_ERROR", 
			"Failed to get driver version", 
			"PulseCountingManager::get_driver_version"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get driver version" << std::endl;
		THROW_DEVFAILED(
			"DEVICE_ERROR", 
			"Failed to get driver version", 
			"PulseCountingManager::get_driver_version"); 
	}

	return l_driver_version;
}

// ============================================================================
// PulseCountingManager::reset_nexus_buffer_index ()
// ============================================================================ 
void PulseCountingManager::reset_nexus_buffer_index()
{
	CHECK_NX_MANAGER();

	m_nexus_manager->resetNexusBufferIndex();
}

// ============================================================================
// PulseCountingManager::get_data_streams ()
// ============================================================================ 
std::vector<std::string> PulseCountingManager::get_data_streams()
{
	std::vector<std::string> l_vect;

  // if no nexus recording, return empty list
  if (m_acq_param.nexusFileGeneration)
  {
    // Re-built nexus item list
    build_nexus_item_list();

    // data stream = <nexus basename>:<coma separated nexus data items>@<flyscan spool>
    yat::OSStream oss; 
    oss << m_acq_param.nexusFileName
        << ":"; 

    for (size_t idx = 0; idx < m_nx_items.size(); idx++)
    {
      if (idx)
        oss << ",";
      oss << m_nx_items.at(idx).name;
    }

    oss << "@"
        << m_acq_param.flyscanSpool;
    l_vect.push_back(oss.str());
  }
	return l_vect;
}

// ======================================================================
// PulseCountingManager::set_acquisition_parameters
// ======================================================================
void PulseCountingManager::set_acquisition_parameters(AcquisitionDefinition p_param)
{
	//- set local acquisition parameters
	m_acq_param = p_param;
}

// ======================================================================
// PulseCountingManager::set_integration_time
// ======================================================================
void PulseCountingManager::set_integration_time(double p_time)
{
  CHECK_CNT_BOARD();

	m_counting_board->setIntegrationTime(p_time);
	m_acq_param.integrationTime = p_time;
}

// ======================================================================
// PulseCountingManager::set_countinuous_mode
// ======================================================================
void PulseCountingManager::set_countinuous_mode(bool p_continuous)
{
  CHECK_CNT_BOARD();

	m_counting_board->setContinuousMode(p_continuous);
	m_acq_param.continuousAcquisition = p_continuous;
}

// ======================================================================
// PulseCountingManager::set_acquisition_mode
// ======================================================================
void PulseCountingManager::set_acquisition_mode(E_AcquisitionMode_t p_acq_mode)
{
	if (p_acq_mode == ACQ_MODE_BUFF)
	{
		m_acq_param.acqMode = ACQ_MODE_BUFF;
	}
	else if (p_acq_mode == ACQ_MODE_SCAL)
	{
		m_acq_param.acqMode = ACQ_MODE_SCAL;
	}
	else
	{
		ERROR_STREAM << "Acquisition mode is not correct" << std::endl;
		THROW_DEVFAILED(
			"DEVICE_ERROR", 
			"Failed to set acquisition mode: use SCALAR or BUFFERED", 
			"PulseCountingManager::set_acquisition_mode"); 
	}
}

// ======================================================================
// PulseCountingManager::set_nexus_file_path
// ======================================================================
void PulseCountingManager::set_nexus_file_path(std::string path)
{
	if (!path.empty())
	{
		m_acq_param.nexusTargetPath = path;
	}
	else
	{
		ERROR_STREAM << "Bad path name: " << path << std::endl;
		THROW_DEVFAILED(
			"COMMAND_FAILED",
			"Failed to set new Nexus file path!",
			"PulseCountingManager::set_nexus_file_path");
	}
}

// ======================================================================
// PulseCountingManager::set_nexus_file_generation
// ======================================================================
void PulseCountingManager::set_nexus_file_generation(bool enable)
{
	m_acq_param.nexusFileGeneration = enable;
}

// ======================================================================
// PulseCountingManager::set_nexus_nb_acq_per_file
// ======================================================================
void PulseCountingManager::set_nexus_nb_acq_per_file(unsigned int p_nb)
{
	m_acq_param.nexusNbPerFile = p_nb;
}

// ============================================================================
// PulseCountingManager::read_counter_pulse_width ()
// ============================================================================ 
void PulseCountingManager::read_counter_pulse_width(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);
	cbd.tga->set_value(&(m_counterListConfig[m_counterList[l_id]].minPulseWidth));
}

// ============================================================================
// PulseCountingManager::write_counter_pulse_width ()
// ============================================================================ 
void PulseCountingManager::write_counter_pulse_width(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManager::write_counter_pulse_width"); 
  }

	Tango::DevDouble l_val;
	unsigned int l_id = 0;
	cbd.tga->get_write_value(l_val);
	cbd.dya->get_user_data<unsigned int>(l_id);

  // check if negative value
  if (l_val < 0)
  {
	  THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
		  "Pulse width should be a positive value!", 
		  "PulseCountingManager::write_counter_pulse_width"); 
  }

	m_counterListConfig[m_counterList[l_id]].minPulseWidth = l_val;

  // memorize new value
  std::string prop_name = std::string("__") + cbd.tga->get_name();
  yat4tango::PropertyHelper::set_property<double>(m_board_arch.hostDevice, prop_name, l_val);
}

// ============================================================================
// PulseCountingManager::read_counter_pulse_width_enabled ()
// ============================================================================ 
void PulseCountingManager::read_counter_pulse_width_enabled(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);
  cbd.tga->set_value(&(m_counterListConfig[m_counterList[l_id]].minPulseWidthEnabled));
}

// ============================================================================
// PulseCountingManager::write_counter_pulse_width_enabled ()
// ============================================================================ 
void PulseCountingManager::write_counter_pulse_width_enabled(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManager::write_counter_pulse_width_enabled"); 
  }

	Tango::DevBoolean l_val;
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);
	cbd.tga->get_write_value(l_val);
	m_counterListConfig[m_counterList[l_id]].minPulseWidthEnabled = l_val;

  // memorize new value
  std::string prop_name = std::string("__") + cbd.tga->get_name();
  yat4tango::PropertyHelper::set_property<bool>(m_board_arch.hostDevice, prop_name, l_val);
}

// ============================================================================
// PulseCountingManager::read_counter_dataset_enabled ()
// ============================================================================ 
void PulseCountingManager::read_counter_dataset_enabled(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);
  cbd.tga->set_value(&(m_counterListConfig[m_counterList[l_id]].nexus));
}

// ============================================================================
// PulseCountingManager::write_counter_dataset_enabled ()
// ============================================================================ 
void PulseCountingManager::write_counter_dataset_enabled(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManager::write_counter_dataset_enabled"); 
  }

	Tango::DevBoolean l_val;
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);
	cbd.tga->get_write_value(l_val);

	m_counterListConfig[m_counterList[l_id]].nexus = l_val;

  // memorize new value
  std::string prop_name = std::string("__") + cbd.tga->get_name();
  yat4tango::PropertyHelper::set_property<bool>(m_board_arch.hostDevice, prop_name, l_val);
}

// ============================================================================
// PulseCountingManager::add_attributes ()
// ============================================================================ 
void PulseCountingManager::add_attributes(CounterConfig p_cfg)
{
	CHECK_MANAGER_DYN_ATTR_MANAGER();

	std::vector<yat4tango::DynamicAttributeInfo> dai_list;
  unsigned int l_id = p_cfg.ctNumber; // key = counter ID in device (unique)

  // Do not create pulse width attributes for DT & PERIOD counters: unavailable function
  if ((p_cfg.mode != COUNTER_MODE_DELTATIME) && 
      (p_cfg.mode != COUNTER_MODE_PERIOD))
  {
	  // pulseWidthEnabled Attribute
    yat4tango::DynamicAttributeInfo dai0;

	  dai0.dev = m_board_arch.hostDevice;
	  dai0.tai.name = p_cfg.name + PULSE_WIDTH_ENABLED;
	  dai0.tai.writable = Tango::READ_WRITE;
	  dai0.tai.data_format = Tango::SCALAR;
	  dai0.tai.format = "%d";
	  dai0.tai.data_type = Tango::DEV_BOOLEAN;
	  dai0.tai.disp_level = Tango::EXPERT;
	  dai0.tai.description = "Pulse width enabled";
	  dai0.memorized = false;

	  dai0.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		  &PulseCountingManager::read_counter_pulse_width_enabled);

	  dai0.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
		  &PulseCountingManager::write_counter_pulse_width_enabled);

	  dai0.set_user_data(l_id);
    dai_list.push_back(dai0);

    // pulseWidth Attribute
    yat4tango::DynamicAttributeInfo dai1;

	  dai1.dev = m_board_arch.hostDevice;
	  dai1.tai.name = p_cfg.name + PULSE_WIDTH;
	  dai1.tai.writable = Tango::READ_WRITE;
	  dai1.tai.data_format = Tango::SCALAR;
	  dai1.tai.format = "%f";
	  dai1.tai.data_type = Tango::DEV_DOUBLE;
	  dai1.tai.disp_level = Tango::EXPERT;
	  dai1.tai.description = "Pulse width";
	  dai1.tai.unit = "s";
	  dai1.memorized = false;

	  dai1.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		  &PulseCountingManager::read_counter_pulse_width);

	  dai1.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
		  &PulseCountingManager::write_counter_pulse_width);
	  
    dai1.set_user_data(l_id);
    dai_list.push_back(dai1);
  }

	// enableDataset Attribute
  yat4tango::DynamicAttributeInfo dai2;

	dai2.dev = m_board_arch.hostDevice;
	dai2.tai.name = DATASET_ENABLED + p_cfg.name;
	dai2.tai.writable = Tango::READ_WRITE;
	dai2.tai.data_format = Tango::SCALAR;
	dai2.tai.format = "%d";
	dai2.tai.data_type = Tango::DEV_BOOLEAN;
	dai2.tai.disp_level = Tango::EXPERT;
	dai2.tai.description = "Dataset enabled";
	dai2.memorized = false;

	dai2.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseCountingManager::read_counter_dataset_enabled);

	dai2.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
		&PulseCountingManager::write_counter_dataset_enabled);

	dai2.set_user_data(l_id);
  dai_list.push_back(dai2);

	try
	{
		//- add all dynamic attributes to the device interface
		m_dyn_attr_manager->add_attributes(dai_list);
	}
	catch (Tango::DevFailed &df)
	{
		ERROR_STREAM << "Dynamic attribute addition failed: " << df << std::endl;
		RETHROW_DEVFAILED(df,
      "SOFTWARE_FAILURE", 
			"Dynamic attribute addition failed.", 
			"PulseManager::add_attributes"); 	
	}
	catch(...)
	{
		ERROR_STREAM << "Dynamic attribute addition failed." << std::endl;
		THROW_DEVFAILED(
      "SOFTWARE_FAILURE", 
			"Dynamic attribute addition failed.", 
			"PulseManager::add_attribute"); 	
	}
}

// ============================================================================
// PulseCountingManager::build_nexus_item_list ()
// ============================================================================
void PulseCountingManager::build_nexus_item_list()
{
  if (m_nx_items.size() > 0)
  {
	  m_nx_items.clear();
  }

  // 1D item in BUFFERED mode with meas dim = 1
  if ((m_acq_param.acqMode == ACQ_MODE_BUFF) && (m_acq_param.nexusMeasDim == 1))
  {
    // 1D
    GenericCounterMap_t::iterator l_it;
    for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
    {
      if (m_counterListConfig[l_it->second].nexus)
      {
	      nxItem item;
        item.name = m_counterListConfig[l_it->second].dataset;
	      item.storageType = NX_DATA_TYPE_1D;

        // in continuous mode, 1 buffer received, with size = samplesNumber
        // otherwise, N buffers received, with size = bufferDepth
        if (m_acq_param.continuousAcquisition)
	        item.dim1 = m_acq_param.samplesNumber;
        else
          item.dim1 = m_acq_param.bufferDepth;

	      item.dim2 = 0;
	      m_nx_items.push_back(item);
      }
    }
  }
  else   // 0D item in SCALAR mode or in BUFFERED mode with meas dim = 0
  {
    // 0D
    GenericCounterMap_t::iterator l_it;
    for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
    {
      if (m_counterListConfig[l_it->second].nexus)
      {
        nxItem item;
        item.name = m_counterListConfig[l_it->second].dataset;
        item.storageType = NX_DATA_TYPE_0D;
        item.dim1 = 0;
        item.dim2 = 0;
        m_nx_items.push_back(item);
      }
    }
  }
}

// ============================================================================
// PulseCountingManager::add_common_attributes ()
// ============================================================================
void PulseCountingManager::add_common_attributes()
{
  // Create integration time attributes
  std::vector<yat4tango::DynamicAttributeInfo> dai(2);

  // integrationTime attribute
	Tango::AttrWriteType rw_mode = Tango::READ_WRITE;
	int attr_type = Tango::DEV_DOUBLE;
	std::string attrFormat = "%6.2f";
	std::string attrDescription = "Acquisition integration period in s\n= 1/frequency";
	dai[0].dev = m_board_arch.hostDevice;
	dai[0].tai.name = "integrationTime";
	dai[0].tai.writable = rw_mode;
	dai[0].tai.data_format = Tango::SCALAR;
	dai[0].tai.format = attrFormat;
	dai[0].tai.data_type = attr_type;
	dai[0].tai.disp_level = Tango::OPERATOR;
	dai[0].tai.description = attrDescription;
	dai[0].tai.unit = "s";
	dai[0].tai.min_value = "0";
	dai[0].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseCountingManager::read_integration_time);
	dai[0].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseCountingManager::write_integration_time);

  // integrationTime attribute
	rw_mode = Tango::READ_WRITE;
	attr_type = Tango::DEV_DOUBLE;
	attrFormat = "%6.2f";
	attrDescription = "Acquisition frequency in Hz\n= 1/integrationTime";
	dai[1].dev = m_board_arch.hostDevice;
	dai[1].tai.name = "frequency";
	dai[1].tai.writable = rw_mode;
	dai[1].tai.data_format = Tango::SCALAR;
	dai[1].tai.format = attrFormat;
	dai[1].tai.data_type = attr_type;
	dai[1].tai.disp_level = Tango::OPERATOR;
	dai[1].tai.description = attrDescription;
  dai[1].tai.unit = "Hz";
  dai[1].tai.min_value = "0";
	dai[1].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseCountingManager::read_frequency);
  dai[1].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
			&PulseCountingManager::write_frequency);

	try
	{
		//- add all dynamic attributes to the device interface
		m_dyn_attr_manager->add_attributes(dai);
	}
	catch(Tango::DevFailed &df)
	{
		ERROR_STREAM << "Acquisition dynamic attributes creation failed: " << df << std::endl;
		RETHROW_DEVFAILED(df,
      "SOFTWARE_FAILURE", 
			"Acquisition dynamic attributes creation failed.", 
			"PulseCountingManager::add_common_attributes"); 	
	}
	catch(...)
	{
		ERROR_STREAM << "Acquisition dynamic attributes creation failed." << std::endl;
		THROW_DEVFAILED(
      "SOFTWARE_FAILURE", 
			"Acquisition dynamic attributes creation failed.", 
			"PulseCountingManager::add_common_attributes"); 	
	}
}

// ============================================================================
// PulseCountingManager::read_frequency ()
// ============================================================================
void PulseCountingManager::read_frequency(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	//DEBUG_STREAM << "PulseCountingManager::read_frequency() entering... "<< endl;
  m_acq_param.frequency = 1 / m_acq_param.integrationTime;
  
  cbd.tga->set_value(&m_acq_param.frequency);
}

// ============================================================================
// PulseCountingManager::write_frequency ()
// ============================================================================
void PulseCountingManager::write_frequency(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManager::write_frequency"); 
  }

	DEBUG_STREAM << "PulseCountingManager::write_frequency() entering... "<< endl;
	double l_freq;
	cbd.tga->get_write_value(l_freq);

  // check if null or negative value
  if (l_freq <= 0.0)
  {
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Frequency should be a strictly positive value!", 
			"PulseCountingManager::write_frequency"); 
  }

  try
	{
		set_integration_time(1/l_freq);
    m_isHwInitNeeded = true;
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,
      "DEVICE_ERROR", 
			"write frequency failed", 
			"PulseCountingManager::write_frequency"); 
	}
	catch (...)
	{
		ERROR_STREAM << "failed to write frequency" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"write frequency failed", 
			"PulseCountingManager::write_frequency"); 
	}

  // memorize new value
  yat4tango::PropertyHelper::set_property<double>(m_board_arch.hostDevice, "__integrationTime", m_acq_param.integrationTime);
}

// ============================================================================
// PulseCountingManager::read_integration_time ()
// ============================================================================
void PulseCountingManager::read_integration_time(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	//DEBUG_STREAM << "PulseCountingManager::read_integrationTime() entering... "<< endl;
	cbd.tga->set_value(&m_acq_param.integrationTime);
}

// ============================================================================
// PulseCountingManager::write_integration_time ()
// ============================================================================
void PulseCountingManager::write_integration_time(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManager::write_integration_time"); 
  }

	DEBUG_STREAM << "PulseCountingManager::write_integration_time() entering... "<< endl;
	double l_time;
	cbd.tga->get_write_value(l_time);
	
  // check if null or negative value
  if (l_time <= 0.0)
  {
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Integration time should be a strictly positive value!",
			"PulseCountingManager::write_integration_time"); 
  }

	try
	{
    if (m_stored_it != l_time)
    {
		  set_integration_time(l_time);
      m_isHwInitNeeded = true;
      m_stored_it = l_time;

      // memorize new value
      yat4tango::PropertyHelper::set_property<double>(m_board_arch.hostDevice, "__integrationTime", m_acq_param.integrationTime);
    }
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,
      "DEVICE_ERROR", 
			"write integration time failed", 
			"PulseCountingManager::write_integration_time"); 
	}
	catch (...)
	{
		ERROR_STREAM << "failed to write integration time" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"write integration time failed", 
			"PulseCountingManager::write_integration_time"); 
	}
}

// ============================================================================
// PulseCountingManager::clean_dyn_attr ()
// ============================================================================
void PulseCountingManager::clean_dyn_attr()
{
  // remove dynamic attributes 
  if (m_dyn_attr_manager)
  {
	  try
	  {
		  m_dyn_attr_manager->remove_attributes();
	  }
	  catch (...)
	  {
		  //- ignore any error
	  }
  }

  // delete dynamic attributes manager
  if (m_dyn_attr_manager)
  {
	  delete m_dyn_attr_manager;
	  m_dyn_attr_manager = NULL;
  }
}

// ============================================================================
// PulseCountingManager::configure_clock ()
// ============================================================================
void PulseCountingManager::configure_clock()
{
  if (m_board_arch.clockGeneration)
  {
    // Configure clock
    ClockConfig l_cfg;	
    l_cfg.channelName = CLOCK_INTERNAL;
    l_cfg.boardName = m_board_arch.boardList[0]; // take 1st board for clock
    l_cfg.timebaseScaling = m_acq_param.timebaseScaling;

    try
    {
      m_clock_generator->init(l_cfg);
      m_isHwInitNeeded = false;
    }
    catch( Tango::DevFailed &df )
    {
      ERROR_STREAM << "Init clock failed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
	      "Init clock failed.", 
	      "PulseCountingManager::configure_clock"); 	
    }
    catch(...)
    {
      ERROR_STREAM << "Init clock failed." << std::endl;
      THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
	      "Init clock failed.", 
	      "PulseCountingManager::configure_clock"); 	
    }
  }
}

// ======================================================================
// PulseCountingManager::change_nx_dataset
// ======================================================================
void PulseCountingManager::change_nx_dataset(yat::uint16 chan_nb, std::string dataset)
{
  // check counter number
  if (m_counterList.count(chan_nb) == 0)
  {
    // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Undefined counter number: " << chan_nb;
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED("DATA_OUT_OF_RANGE", 
                      oss.str().c_str(), 
                      "PulseCountingManager::change_nx_dataset"); 
  }
	
  // update internal map with new dataset
  m_counterListConfig[m_counterList[chan_nb]].dataset = dataset;
}

// ======================================================================
// PulseCountingManager::get_channel_info
// ======================================================================
std::map<yat::uint16, std::string> PulseCountingManager::get_channel_info()
{
  std::map<yat::uint16, std::string> infos;

  // parse counter list
  GenericCounterMap_t::iterator l_it;
  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
  {
    // the string contains:
    /*      
    NAME:<counter name>  
    ENABLED:true // always true in PulseCounting case (if defined, enabled)
    DATASET_NAME:<name of the dataset>
    DATASET_ENABLED:<true / false>
    DATASET_ATTR:<name of the dataset enabled TANGO attribute>
    */
    yat::uint16 chan_nb = m_counterListConfig[l_it->second].ctNumber;
    std::string chan_label = m_counterListConfig[l_it->second].name;
    std::string dataset_name = m_counterListConfig[l_it->second].dataset;
    bool dataset_enabled = m_counterListConfig[l_it->second].nexus;
    yat::OSStream oss;
    std::string separator = ";";

    oss << "NAME:" << chan_label << separator;
    oss << "ENABLED:true" << separator;
    oss << "DATASET_NAME:" << dataset_name << separator;

    std::string tof = dataset_enabled ? "true" : "false"; 
    oss << "DATASET_ENABLED:" << tof << separator;

    oss << "DATASET_ATTR:" << DATASET_ENABLED << chan_label;

    infos.insert( pair<yat::uint16, std::string>(chan_nb, oss.str()) );
  }

  return infos;
}

} // namespace PulseCounting_ns

