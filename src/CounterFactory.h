//=============================================================================
// CounterFactory.h
//=============================================================================
// abstraction.......CounterFactory
// class.............CounterFactory
// original author...S.GARA - Nexeya
//=============================================================================

#ifndef _COUNTER_FACTORY_H_
#define _COUNTER_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "GenericCounterInterface.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: CounterFactory
// ============================================================================
class CounterFactory
{
public: 
  //- instanciate a specialized GenericCounterInterface
  static GenericCounterInterface * instanciate (Tango::DeviceImpl * hostDevice, CounterConfig p_ctr_cfg, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board);
  	
private:
  CounterFactory ();
  ~CounterFactory ();
};

} // namespace PulseCounting_ns

#endif // _COUNTER_FACTORY_H_
