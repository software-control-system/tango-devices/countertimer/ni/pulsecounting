//=============================================================================
// PeriodCounter.cpp
//=============================================================================
// abstraction.......PeriodCounter for PulseCounting
// class.............PeriodCounter
// original author...S.Minolli - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PeriodCounter.h"

namespace PulseCounting_ns
{

	//- check manager macro:
#define CHECK_INTERFACE() \
	do \
	{ \
	if (! m_interface) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the interface isn't accessible ", \
	"PeriodCounter::check_interface"); \
} while (0)


// ============================================================================
// PeriodCounter::PeriodCounter ()
// ============================================================================ 
PeriodCounter::PeriodCounter (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board)
: GenericCounterInterface(hostDevice)
{
	m_is_available = true;
	m_state = Tango::STANDBY;
	m_acq_mode = p_acq_mode;
	m_interface = p_board;
	m_value = 0;
}

// ============================================================================
// PeriodCounter::~PeriodCounter ()
// ============================================================================ 
PeriodCounter::~PeriodCounter ()
{

}

// ============================================================================
// PeriodCounter::is_available ()
// ============================================================================ 
bool PeriodCounter::is_available()
{
	return m_is_available;
}

// ============================================================================
// PeriodCounter::init ()
// ============================================================================ 
void PeriodCounter::init(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->initCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to initialize period counter!", 
			"PeriodCounter::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to initialize period counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to initialize period counter!", 
			"PeriodCounter::init"); 
	}
}

// ============================================================================
// PeriodCounter::configure ()
// ============================================================================ 
void PeriodCounter::configure(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->configureCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to configure period counter!", 
			"PeriodCounter::configure"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to configure period counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to configure period counter!", 
			"PeriodCounter::configure"); 
	}
}

// ============================================================================
// PeriodCounter::start ()
// ============================================================================ 
void PeriodCounter::start()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->startCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start period counter", 
			"PeriodCounter::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start period counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to start period counter", 
			"PeriodCounter::start"); 
	}
	m_state = Tango::RUNNING;
}

// ============================================================================
// PeriodCounter::stop ()
// ============================================================================ 
void PeriodCounter::stop()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->stopCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to stop period counter", 
			"PeriodCounter::stop"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to stop period counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to stop period counter", 
			"PeriodCounter::stop"); 
	}
	m_state = Tango::STANDBY;
}

// ============================================================================
// PeriodCounter::get_state ()
// ============================================================================ 
Tango::DevState PeriodCounter::get_state()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterState(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get period counter state!", 
			"PeriodCounter::get_state"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get period counter state!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get period counter state!", 
			"PeriodCounter::get_state"); 
	}
}

// ============================================================================
// PeriodCounter::update_scalar_value ()
// ============================================================================ 
void PeriodCounter::update_scalar_value()
{
  CHECK_INTERFACE();

	try
	{
    yat::AutoMutex<> guard(m_dataLock);
		m_value = m_interface->getCounterScalarValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update scalar period counter value!", 
			"PeriodCounter::update_scalar_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update scalar period counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update scalar period counter value!", 
			"PeriodCounter::update_scalar_value"); 
	}
}

// ============================================================================
// PeriodCounter::update_buffer_value ()
// ============================================================================ 
void PeriodCounter::update_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
    m_interface->updateCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update buffer period counter value!", 
			"PeriodCounter::update_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update buffer period counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update buffer period counter value!", 
			"PeriodCounter::update_buffer_value"); 
	}
}

// ============================================================================
// PeriodCounter::get_scalar_value ()
// ============================================================================ 
data_t PeriodCounter::get_scalar_value()
{
  yat::AutoMutex<> guard(m_dataLock);
	return m_value;
}

// ============================================================================
// PeriodCounter::get_buffer_value ()
// ============================================================================ 
RawData_t PeriodCounter::get_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get period counter buffer value", 
			"PeriodCounter::get_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get period counter buffer value" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get period counter buffer value", 
			"PeriodCounter::get_buffer_value"); 
	}
}

// ============================================================================
// PeriodCounter::get_name ()
// ============================================================================ 
std::string PeriodCounter::get_name()
{
	return m_cfg.name;
}

// ============================================================================
// PeriodCounter::get_config ()
// ============================================================================ 
CounterConfig PeriodCounter::get_config()
{
	return m_cfg;
}

// ============================================================================
// PeriodCounter::deleteObject ()
// ============================================================================ 
void PeriodCounter::deleteObject()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->releaseCounter(m_cfg);
  }
  catch(...)
  {
  }
}

// ============================================================================
// PeriodCounter::update_last_buffer_value ()
// ============================================================================ 
void PeriodCounter::update_last_buffer_value()
{
  CHECK_INTERFACE();

  try
  {
    m_interface->updateCounterLastBufferValue(m_cfg);
  }
  catch (Tango::DevFailed &e)
  {
	ERROR_STREAM << e << std::endl;
	RETHROW_DEVFAILED(e,
        "DEVICE_ERROR", 
		"Failed to update last buffer period counter value!", 
		"PeriodCounter::update_last_buffer_value"); 
  }
  catch (...)
  {
	ERROR_STREAM << "Failed to update last buffer period counter value!" << std::endl;
	THROW_DEVFAILED(
        "DEVICE_ERROR", 
 		"Failed to update last buffer period counter value!", 
		"PeriodCounter::update_last_buffer_value"); 
  }
}

} // namespace PulseCounting_ns

