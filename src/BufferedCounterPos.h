//=============================================================================
// BufferedCounterPos.h
//=============================================================================
// abstraction.......BufferedCounterPos for PulseCounting
// class.............BufferedCounterPos
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _BUFFERED_COUNTER_POS_H
#define _BUFFERED_COUNTER_POS_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingTypesAndConsts.h"
#include <NI660Xsl/BufferedPositionMeasurement.h>
#include <yat/threading/Thread.h>
#include <yat4tango/ExceptionHelper.h>
#include "NexusManager.h"

namespace PulseCounting_ns
{

	// ============================================================================
	// class: BRPThread
	// ============================================================================
	class BRPThread : public yat::Thread, public Tango::LogAdapter
	{
		friend class BufferedCounterPos;

	protected:
		//- ctor ---------------------------------
		BRPThread (Tango::DeviceImpl * hostDevice, yat::Thread::IOArg ioa);

		//- dtor ---------------------------------
		virtual ~BRPThread (void);

		//- thread's entry point
		virtual yat::Thread::IOArg run_undetached (yat::Thread::IOArg ioa);

		//- asks this BREThread to quit
		virtual void exit (void);

		//- Thread state
		bool isThreadAlive()
		{
			return m_goOn;
		}

		//- Buffer update state
		bool updateDone()
		{
			return m_isUpdateDone;
		}

	private:
		//- thread's ctrl flag
		bool m_goOn;

		//- data update done
		bool m_isUpdateDone;
	};


// ============================================================================
// class: BufferedCounterPos
// ============================================================================
class BufferedCounterPos : public yat4tango::TangoLogAdapter, public ni660Xsl::BufferedPositionMeasurement
{

public:

  //- constructor
  BufferedCounterPos (BCEconfig p_conf, NexusManager * storage);

  //- destructor
  virtual ~BufferedCounterPos ();

  //*********** NI660Xsl inheritance  ****************
  // These handle_xxx functions are called directly by NI660Xsl lib in "callback mode" and
  // called after wait task ends in "polling" mode.

  //- handle timeout
  void handle_timeout();

  //- handle data lost
  void handle_data_lost();

  //- handle raw buffer
  void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read);

  //- handle scaled buffer
  void handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read);

  //********** local members  ****************
  // start counter
  void startCnt();

  // stop counter
  void stopCnt();

  //- get value
  RawData_t & get_value();

  // Acquisition done info
  bool isAcquisitionDone()
  {
    yat::AutoMutex<> guard(m_buffLock);
	  return m_acquisitionDone;
  }

  // updates scaled buffer <=> starts waiting task if not already running
  // (used in "polling" mode)
  void updateScaledBuffer();

  // updates last scaled buffer <=> force buffer reading
  // (used with trigger listener option)
  void updateLastScaledBuffer();

  // gets scaled buffer <=> calls the driver waiting task (used in "polling" mode)
  void getScaledBuffer();

  //- release counter
  void deleteObject();

  // is counter in overrun ?
  bool isOverrun()
  {
    return m_cntOverrun;
  }

  // has counter timed out ?
  bool isTimedout()
  {
    return m_cntTimedout;
  }

  // has counter stopped on storage error?
  bool isStorageKO()
  {
    return m_storageError;
  }


protected:
	// counter config
	BCEconfig m_cfg;

	// total number of buffers to receive for each acquisition
	unsigned long m_bufferNbToReceive;

	// current number of buffers actually received
	unsigned long m_currentBufferNb;

	// First point of the currently received buffer
	unsigned long m_firstPtReceivedBuff;

	// raw data buffer
	RawData_t m_data;

	// last value of the n-1 buffer
	data_t m_lastReceivedValue;

	// Acquisition done flag
	bool m_acquisitionDone;

	//- data mutex protection
	yat::Mutex m_buffLock;

	// Data buffer waiting task
	BRPThread * m_dataBuffer_thread;

  // overrun flag
  bool m_cntOverrun;

  // timeout flag
  bool m_cntTimedout;

  // Storage error flag
  bool m_storageError;

  // storage manager
  NexusManager * m_storage_mgr;
};

} // namespace PulseCounting_ns

#endif // _BUFFERED_COUNTER_POS_H
