//=============================================================================
// BufferedCounterEvt.cpp
//=============================================================================
// abstraction.......BufferedCounterEvt for PulseCounting
// class.............BufferedCounterEvt
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "BufferedCounterEvt.h"

namespace PulseCounting_ns
{

// ============================================================================
// BufferedCounterEvt::BufferedCounterEvt ()
// ============================================================================ 
BufferedCounterEvt::BufferedCounterEvt (BCEconfig p_conf, NexusManager * storage)
: yat4tango::TangoLogAdapter(p_conf.hostDevice)
{
	m_bufferNbToReceive = 0;
	m_currentBufferNb = 0;
	m_firstPtReceivedBuff = 0;
	m_lastReceivedValue = 0.0;
	m_acquisitionDone = true;
	m_dataBuffer_thread = NULL;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;
  m_data.clear();
	m_cfg = p_conf;
  m_storage_mgr = storage;
}

// ============================================================================
// BufferedCounterEvt::~BufferedCounterEvt ()
// ============================================================================ 
BufferedCounterEvt::~BufferedCounterEvt ()
{
	
}

// ============================================================================
// BufferedCounterEvt::updateRawBuffer
// ============================================================================
void BufferedCounterEvt::updateRawBuffer()
{
	// Delete "buffer recover" thread if exists and if data recovery is over
	if (m_dataBuffer_thread)
	{
		if (m_dataBuffer_thread->updateDone())
		{
			yat::Thread::IOArg ioa;
			m_dataBuffer_thread->exit();
			m_dataBuffer_thread->join(&ioa);
			//std::cout << "BufferedCounterEvt::updateRawBuffer - BR thread exited for counter " 
			//  << m_cfg.cnt.number << std::endl;
			m_dataBuffer_thread = NULL; 
		}
	}

	// Task the buffer reception if acquisition is on & if previous buffer recovery is over
	if ((!m_acquisitionDone) && 
		  (m_dataBuffer_thread == NULL))
	{
		m_dataBuffer_thread = new BREThread(m_cfg.hostDevice, static_cast<yat::Thread::IOArg>(this));
		DEBUG_STREAM << "BufferedCounterEvt Thread created ..." << endl;
		if (m_dataBuffer_thread == NULL)
		{
			ERROR_STREAM << "Buffer recovery cannot be tasked for counter " 
				<< m_cfg.cnt.number << std::endl;
			THROW_DEVFAILED(
        "DEVICE_ERROR",
				"Failed to start buffer recover task!",
				"BufferedCounterEvt::updateRawBuffer");    
		}
		//std::cout << "BufferedCounterEvt::updateRawBuffer - starting Buffer recovery thread for counter " 
		//  << m_cfg.cnt.number << std::endl;
		m_dataBuffer_thread->start_undetached();   
	}
}

// ============================================================================
// BufferedCounterEvt::getRawBuffer
// ============================================================================
void BufferedCounterEvt::getRawBuffer()
{
	// call "get raw buffer" to compose the whole buffer.
	// In "polling" mode, the client has to poll to compose the acquisition buffer.
	if (!m_acquisitionDone)
	{
		DEBUG_STREAM << "BufferedCounterEvt::GetRawBuffer entering..." << endl;
		//- from NI660Xsl - calls ni660Xsl::handle_xxx methods
		try
		{
			ni660Xsl::BufferedEventCounting::get_raw_buffer(); // the driver waits 'buffer depth' seconds...
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
	}
}

// ============================================================================
// BufferedCounterEvt::handle_timeout ()
// ============================================================================ 
void BufferedCounterEvt::handle_timeout()
{
	ERROR_STREAM << "BufferedCounterEvt::handle_timeout() entering..." << endl;
	m_cntTimedout = true;
}

// ============================================================================
// BufferedCounterEvt::handle_data_lost ()
// ============================================================================ 
void BufferedCounterEvt::handle_data_lost()
{
	ERROR_STREAM << "BufferedCounterEvt::handle_data_lost() entering..." << endl;
	m_cntOverrun = true;
}

// ============================================================================
// BufferedCounterEvt::handle_raw_buffer ()
// ============================================================================ 
void BufferedCounterEvt::handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read)
{
	DEBUG_STREAM << "BufferedCounterEvt::handle_raw_buffer() entering for counter: " << m_cfg.cnt.name << " ... " 
                 << " - samples to read = " << _samples_read << std::endl;

  // check samples number is > 0
  if (_samples_read <= 0)
  {
    ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> samples to read negative or null value!" << std::endl;
    stopCnt();
	return;
  }	
  
	yat::AutoMutex<> guard(m_buffLock);

	ni660Xsl::InRawBuffer& buf = *buffer;
	RawData_t & data = m_data;
  bool data_to_be_stored = (m_cfg.acq.nexusFileGeneration && m_cfg.cnt.nexus);

	//- Continuous acquisition: only one buffer is received !!
	if (m_cfg.acq.continuousAcquisition)
	{
		DEBUG_STREAM << "continuous acquisition: nb buffer to receive = 1!" << std::endl;  
		data[0] = (data_t)buf[0];

		//- Compute the difference between 2 points
		//- because the counts are cumulated
		for (unsigned long idx = 1; idx < (unsigned long)_samples_read; idx++)
		{
			data[idx + m_firstPtReceivedBuff] = (data_t)(buf[idx] - buf[idx - 1]);
		}

    // store received buffer in Nexus, if nx storage enabled
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &data[0], (unsigned long)_samples_read);
      }
      catch (Tango::DevFailed & df)
      {
	      ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
	      ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }

    // stop acquisition
		stopCnt();
	}
  else if (m_bufferNbToReceive == 0) //- infinite mode
  {
    DEBUG_STREAM << "one buffer received." << std::endl;

    unsigned long nb_to_copy;
    nb_to_copy = (unsigned long)_samples_read;
    DEBUG_STREAM << "NB to copy : " << nb_to_copy << endl;

    // tempo buffer for storage
    RawData_t bufNx;
    if (data_to_be_stored)
    {
      bufNx.capacity(nb_to_copy);
      bufNx.force_length(nb_to_copy);
    }

    //- Compute the difference between 2 points
    //- because the counts are cumulated
    for (unsigned long idx = 1; idx < nb_to_copy; idx++ )
    {
      data[idx] = (data_t)(buf[idx] - buf[idx - 1]);
      if (data_to_be_stored)
        bufNx[idx] = data[idx];
    }

    //- at start, m_lastReceivedValue equals 0.
    //- m_lastReceivedValue is the last value of the n-1 buffer
    data[0] = (data_t)(buf[0] - m_lastReceivedValue);
    if (data_to_be_stored)
      bufNx[0] = data[0];

    //- memorize the last value of buf (will be used for the next buf)
    m_lastReceivedValue = (data_t)buf[nb_to_copy - 1];

    // store received buffer in Nexus, if nx storage enabled for this counter
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &bufNx[0], nb_to_copy);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
        ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }
  }
	else //- not continuous nor infinite
	{
		//- All buffers have not been yet received
		if (m_currentBufferNb < m_bufferNbToReceive)
		{
			DEBUG_STREAM << "buffer currently received for counter " << m_cfg.cnt.name << " = " << m_currentBufferNb + 1 
			             << " on " << m_bufferNbToReceive << std::endl;
			unsigned long nb_to_copy;

			if (((unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff) < (unsigned long)_samples_read)
			{
				nb_to_copy = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
			}
			else
			{
				nb_to_copy = (unsigned long)_samples_read;
			}
			DEBUG_STREAM << "NB to copy for counter " << m_cfg.cnt.name << ": " << nb_to_copy << endl;

      // add number to copy to current data
	  data.capacity(data.length() + nb_to_copy, true);
	  data.force_length(data.length() + nb_to_copy);

      // tempo buffer for storage
      RawData_t bufNx;
      if (data_to_be_stored)
      {
        bufNx.capacity(nb_to_copy);
        bufNx.force_length(nb_to_copy);
      }

			//- Compute the difference between 2 points
			//- because the counts are cumulated
			for (unsigned long idx = 1; idx < nb_to_copy; idx++ )
			{
				data[idx + m_firstPtReceivedBuff] = (data_t)(buf[idx] - buf[idx - 1]);
        if (data_to_be_stored)
          bufNx[idx] = data[idx + m_firstPtReceivedBuff];
			}

			//- at start, m_firstPtReceivedBuff and m_lastReceivedValue equal 0.
			//- m_firstPtReceivedBuff is the position of the 1st point of the buffer
			//- m_lastReceivedValue is the last value of the n-1 buffer
			data[m_firstPtReceivedBuff] = (data_t)(buf[0] - m_lastReceivedValue);
      if (data_to_be_stored)
        bufNx[0] = data[m_firstPtReceivedBuff];

			//- memorize the last value of buf (will be used for the next buf)
			m_lastReceivedValue = (data_t)buf[nb_to_copy - 1];

			m_currentBufferNb++;
			m_firstPtReceivedBuff += nb_to_copy;

      // store received buffer in Nexus, if nx storage enabled for this counter
      if (data_to_be_stored && m_storage_mgr)
      {
        try
        {
          m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &bufNx[0], nb_to_copy);
		  DEBUG_STREAM << "Push data ok for counter: " << m_cfg.cnt.name << endl;
        }
        catch (Tango::DevFailed & df)
        {
	        ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
        catch (...)
        {
	        ERROR_STREAM << "BufferedCounterEvt::handle_raw_buffer-> pushNexusData caugth unknown exception!" << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
      }
		}

		// if all requested buffers are received 
		if (m_currentBufferNb == m_bufferNbToReceive)
		{	
			// stop acquisition
			stopCnt();
		}
	}

  delete buffer;
}

// ============================================================================
// BufferedCounterEvt::handle_scaled_buffer ()
// ============================================================================ 
void BufferedCounterEvt::handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read)
{
	//- Event values are not scaled !!!
	delete buffer;
}

// ============================================================================
// BufferedCounterEvt::startCnt
// ============================================================================
void BufferedCounterEvt::startCnt()
{
	DEBUG_STREAM << "BufferedCounterEvt::startCnt() entering..." << endl;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;

	{
		yat::AutoMutex<> guard(m_buffLock);

    // reset current counters & buffers
		m_currentBufferNb = 0;
		m_firstPtReceivedBuff = 0;
		m_lastReceivedValue = 0;
		m_acquisitionDone = false;

    // check if infinite mode
    if (m_cfg.acq.samplesNumber == 0)
    {
      // set data length & capacity to buffer depth
	    m_data.capacity(m_cfg.acq.bufferDepth); 
	    m_data.force_length(m_cfg.acq.bufferDepth);
    }
    else
    {
      // check if continuous mode
      if (m_cfg.acq.continuousAcquisition)
      {
        // set data length & capacity to samples nber
	      m_data.capacity(m_cfg.acq.samplesNumber);
	      m_data.force_length(m_cfg.acq.samplesNumber);
      }
      else
      {
        // set data capacity & length to 0
	      m_data.capacity(0);
	      m_data.force_length(0);
      }
    }
    m_data.fill(yat::IEEE_NAN);

		unsigned long buffer_depth_nbpts = 0;

		if (m_cfg.acq.continuousAcquisition)
		{
			buffer_depth_nbpts = m_cfg.acq.samplesNumber;
		}
		else
		{
			buffer_depth_nbpts = static_cast<unsigned long>(m_cfg.acq.bufferDepth);
		}
		m_bufferNbToReceive = (m_cfg.acq.samplesNumber + buffer_depth_nbpts - 1) / buffer_depth_nbpts;
		DEBUG_STREAM << "Buffer to receive : " << m_bufferNbToReceive << endl;
	}

	//- from NI660Xsl
  try
  {
	  ni660Xsl::BufferedEventCounting::start();
  }
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}

#if !defined (USE_CALLBACK) 
	// start the data buffer waiting task for the 1st time
	updateRawBuffer();
#endif
}

// ============================================================================
// BufferedCounterEvt::stopCnt
// ============================================================================
void BufferedCounterEvt::stopCnt()
{
  DEBUG_STREAM << "BufferedCounterEvt::stopCnt() entering..." << endl;
	m_acquisitionDone = true;

  try
  {
    //- from NI660Xsl
    // we use abort to stop counting immediately
    // sets an UNKNOWN state in NI660Xsl lib !
    ni660Xsl::BufferedEventCounting::abort_and_release();
  }
  catch(ni660Xsl::DAQException & nie) 
  {
    if (nie.errors[0].code > 0)
    {
      WARN_STREAM << "BufferedCounterEvt::stopCnt() - Trying to abort during buffer acquisition, retry once..." << std::endl;
	    ni660Xsl::BufferedEventCounting::abort_and_release();
    }
    else
    {
      // Bug in DAQmx 9.x: Aborting counter generates exception 
		  // An effective workaround is to catch and clear the error 
    }
  }
  catch(...)
	{
      // One more time:
      // Bug in DAQmx 9.x: Aborting counter generates exception 
      // An effective workaround is to catch and clear the error 
	}
}

// ============================================================================
// BufferedCounterEvt::get_value ()
// ============================================================================ 
RawData_t & BufferedCounterEvt::get_value()
{
  yat::AutoMutex<> guard(m_buffLock);
  return m_data;
}

// ============================================================================
// BufferedCounterEvt::updateLastRawBuffer
// ============================================================================
void BufferedCounterEvt::updateLastRawBuffer()
{
  // call "get last raw buffer" to compose the whole buffer.
  // With trigger listener option, the device has to force the reading of the last
  // incomplete buffer to get the final data.
  DEBUG_STREAM << "BufferedCounterEvt::updateLastRawBuffer ..." << endl;

  // wait a little to be sure that the driver has received the last point
  yat::Thread::sleep(kLAST_BUFFER_DELAY_MS);

  //- from NI660Xsl - calls ni660Xsl::handle_xxx methods
  try
  {
    // if delay is not sufficient, send number of expected samples
    // to the get last buffer function which will do the comparison
    unsigned long expected_samples_nb = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
	ni660Xsl::BufferedEventCounting::get_last_raw_buffer(expected_samples_nb);
  }
  catch(ni660Xsl::DAQException & nie)
  {
    throw_devfailed(nie);
  }
}


//*****************************************************************************
// BREThread
//*****************************************************************************
// ============================================================================
// BREThread::BREThread
// ============================================================================
BREThread::BREThread (Tango::DeviceImpl * hostDevice, yat::Thread::IOArg ioa)
: yat::Thread(ioa),
Tango::LogAdapter (hostDevice),
m_goOn(true),
m_isUpdateDone(false)
{
	//- noop ctor
}

// ============================================================================
// BREThread::~BREThread
// ============================================================================
BREThread::~BREThread (void)
{
	//- noop dtor
}

// ============================================================================
// BREThread::run_undetached
// ============================================================================
yat::Thread::IOArg BREThread::run_undetached (yat::Thread::IOArg ioa)
{
	DEBUG_STREAM << "BREThread::run_undetached() entering... " << std::endl;

	m_isUpdateDone = false;
	//- get ref. to out our parent task
	BufferedCounterEvt * cm_task = reinterpret_cast<BufferedCounterEvt *>(ioa);

	//std::cout << "**Wait for buffer value to be set from board..." << std::endl;
	cm_task->getRawBuffer(); 

	// when function returns, the buffer is handled (or timeout if no data)
	//std::cout << "**Buffer update done." << std::endl;
	m_isUpdateDone = true;

	return 0;
}

// ============================================================================
// BREThread::exit
// ============================================================================
void BREThread::exit (void)
{
	m_goOn = false;
}

} // namespace PulseCounting_ns

