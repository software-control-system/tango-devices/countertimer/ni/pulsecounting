//=============================================================================
// CountingBoardFactory.h
//=============================================================================
// abstraction.......CountingBoardFactory
// class.............CountingBoardFactory
// original author...S.GARA - Nexeya
//=============================================================================

#ifndef _COUNTING_BOARD_FACTORY_H_
#define _COUNTING_BOARD_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CountingBoardInterface.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: CountingBoardFactory
// ============================================================================
class CountingBoardFactory
{
public: 
  //- instanciate a specialized CountingBoard
  static CountingBoardInterface * instanciate (Tango::DeviceImpl * hostDevice, 
    E_BoardType_t p_board, E_AcquisitionMode_t p_acqMode, NexusManager * p_storage);
      
private:
  CountingBoardFactory ();
  ~CountingBoardFactory ();
};

} // namespace PulseCounting_ns

#endif // _COUNTING_BOARD_FACTORY_H_
