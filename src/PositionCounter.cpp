//=============================================================================
// PositionCounter.cpp
//=============================================================================
// abstraction.......PositionCounter for PulseCounting
// class.............PositionCounter
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PositionCounter.h"

namespace PulseCounting_ns
{

	//- check manager macro:
#define CHECK_INTERFACE() \
	do \
	{ \
	if (! m_interface) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the interface isn't accessible ", \
	"PositionCounter::check_interface"); \
} while (0)


// ============================================================================
// PositionCounter::PositionCounter ()
// ============================================================================ 
PositionCounter::PositionCounter (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board)
:  GenericCounterInterface(hostDevice)
{
	m_is_available = true;
	m_state = Tango::STANDBY;
	m_acq_mode = p_acq_mode;
	m_interface = p_board;
	m_value = 0;
}

// ============================================================================
// PositionCounter::~PositionCounter ()
// ============================================================================ 
PositionCounter::~PositionCounter ()
{

}

// ============================================================================
// PositionCounter::is_available ()
// ============================================================================ 
bool PositionCounter::is_available()
{
  // Check if counter0 is enabled with isMaster = true
  // TODO?
  // NI driver throws an exception in this case...

	return m_is_available;
}

// ============================================================================
// PositionCounter::init ()
// ============================================================================ 
void PositionCounter::init(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->initCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to init counter", 
			"PositionCounter::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to init counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to init counter", 
			"PositionCounter::init"); 
	}
}

// ============================================================================
// PositionCounter::configure ()
// ============================================================================ 
void PositionCounter::configure(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->configureCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to configure counter", 
			"PositionCounter::configure"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to configure counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to configure counter", 
			"PositionCounter::configure"); 
	}
}

// ============================================================================
// PositionCounter::start ()
// ============================================================================ 
void PositionCounter::start()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->startCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start counter", 
			"PositionCounter::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start counter" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to start counter", 
			"PositionCounter::start"); 
	}
	m_state = Tango::RUNNING;
}

// ============================================================================
// PositionCounter::stop ()
// ============================================================================ 
void PositionCounter::stop()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->stopCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to stop counter", 
			"PositionCounter::stop"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to stop counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to stop counter", 
			"PositionCounter::stop"); 
	}
	m_state = Tango::STANDBY;
}

// ============================================================================
// PositionCounter::get_state ()
// ============================================================================ 
Tango::DevState PositionCounter::get_state()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterState(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get evt counter state!", 
			"PositionCounter::get_state"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get evt counter state!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get evt counter state!", 
			"PositionCounter::get_state"); 
	}
}

// ============================================================================
// PositionCounter::update_scalar_value ()
// ============================================================================ 
void PositionCounter::update_scalar_value()
{
  CHECK_INTERFACE();

	try
	{   
    yat::AutoMutex<> guard(m_dataLock);
		m_value = m_interface->getCounterScalarValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to update scalar counter value", 
			"PositionCounter::update_scalar_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update scalar counter value" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update scalar counter value", 
			"PositionCounter::update_scalar_value"); 
	}	
}

// ============================================================================
// PositionCounter::update_buffer_value ()
// ============================================================================ 
void PositionCounter::update_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
    m_interface->updateCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update buffer evt counter value!", 
			"PositionCounter::update_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update buffer evt counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update buffer evt counter value!", 
			"PositionCounter::update_buffer_value"); 
	}
}

// ============================================================================
// PositionCounter::get_scalar_value ()
// ============================================================================ 
data_t PositionCounter::get_scalar_value()
{
  yat::AutoMutex<> guard(m_dataLock);
	return m_value;
}

// ============================================================================
// PositionCounter::get_buffer_value ()
// ============================================================================ 
RawData_t PositionCounter::get_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get counter buffer value", 
			"PositionCounter::get_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get counter buffer value" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get counter buffer value", 
			"PositionCounter::get_buffer_value"); 
	}
}

// ============================================================================
// PositionCounter::get_name ()
// ============================================================================ 
std::string PositionCounter::get_name()
{
	return m_cfg.name;
}
// ============================================================================
// PositionCounter::get_config ()
// ============================================================================ 
CounterConfig PositionCounter::get_config()
{
	return m_cfg;
}

// ============================================================================
// PositionCounter::deleteObject ()
// ============================================================================ 
void PositionCounter::deleteObject()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->releaseCounter(m_cfg);
  }
  catch(...)
  {
  }
}

// ============================================================================
// PositionCounter::update_last_buffer_value ()
// ============================================================================ 
void PositionCounter::update_last_buffer_value()
{
  CHECK_INTERFACE();

  try
  {
    m_interface->updateCounterLastBufferValue(m_cfg);
  }
  catch (Tango::DevFailed &e)
  {
	ERROR_STREAM << e << std::endl;
	RETHROW_DEVFAILED(e,
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"PositionCounter::update_last_buffer_value"); 
  }
  catch (...)
  {
	ERROR_STREAM << "Failed to update last buffer dt counter value!" << std::endl;
	THROW_DEVFAILED(
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"PositionCounter::update_last_buffer_value"); 
  }
}

} // namespace PulseCounting_ns

