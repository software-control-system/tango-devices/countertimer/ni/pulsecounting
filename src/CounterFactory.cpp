//=============================================================================
// CounterFactory.cpp
//=============================================================================
// abstraction.......CounterFactory
// class.............CounterFactory
// original author...S.GARA - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include "CounterFactory.h"
#include "EventCounter.h"
#include "PositionCounter.h"
#include "DeltaTimeCounter.h"
#include "PeriodCounter.h"

namespace PulseCounting_ns
{

// ======================================================================
// CounterFactory::instanciate
// ======================================================================
GenericCounterInterface * CounterFactory::instanciate (Tango::DeviceImpl * hostDevice, CounterConfig p_ctr_cfg, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board)
{
  GenericCounterInterface * l_counter;

  switch (p_ctr_cfg.mode)
  {
  case COUNTER_MODE_EVENT:
	  l_counter = new EventCounter(hostDevice, p_acq_mode, p_board);
	  break;
  case COUNTER_MODE_POSITION:
	  l_counter = new PositionCounter(hostDevice, p_acq_mode, p_board);
	  break;
  case COUNTER_MODE_DELTATIME:
	  l_counter = new DeltaTimeCounter(hostDevice, p_acq_mode, p_board);
	  break;
  case COUNTER_MODE_PERIOD:
	  l_counter = new PeriodCounter(hostDevice, p_acq_mode, p_board);
	  break;
  case UNDEFINED_COUNTER_MODE:
	  l_counter = NULL;
	  break;
  default:
	  l_counter = NULL;
	  break;
  }
 
  return l_counter;
}

} // namespace PulseCounting_ns


