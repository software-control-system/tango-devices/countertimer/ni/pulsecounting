//=============================================================================
// ManagerFactory.cpp
//=============================================================================
// abstraction.......ManagerFactory
// class.............ManagerFactory
// original author...S.GARA - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include "ManagerFactory.h"
#include "PulseCountingManagerBuffered.h"
#include "PulseCountingManagerScalar.h"

namespace PulseCounting_ns
{

// ======================================================================
// ManagerFactory::instanciate
// ======================================================================
 PulseCountingManager * ManagerFactory::instanciate (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_mode)
{
  PulseCountingManager * l_manager = NULL;

  switch (p_mode)
  {
    case ACQ_MODE_SCAL:
	    l_manager = new PulseCountingManagerScalar(hostDevice);
	    break;
    case ACQ_MODE_BUFF:
	    l_manager = new PulseCountingManagerBuffered(hostDevice);
	    break;
    case UNDEFINED_ACQ_MODE:
	    l_manager = NULL;
	    break;
    default:
	    l_manager = NULL;
	    break;
  }
 
  return l_manager;
}

} // namespace PulseCounting_ns


