//=============================================================================
// PulseCountingManagerScalar.h
//=============================================================================
// abstraction.......PulseCountingManagerScalar for PulseCounting
// class.............PulseCountingManagerScalar
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _PULSE_COUNTING_MANAGER_SCALAR_H
#define _PULSE_COUNTING_MANAGER_SCALAR_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseCountingManager.h"

namespace PulseCounting_ns
{

	// ============================================================================
	// struct: ECConfig
	// Configuration of the end of clock wait task
	// ============================================================================
	typedef struct ECConfig 
	{
		//- the TANGO host device (for logging)
		Tango::DeviceImpl * hostDevice;

		//- Clock generator
		ClockGenerator * clockGen;

		//- default ctor (set default values)
		ECConfig () :
		hostDevice(NULL),
		clockGen(NULL)
		{
			//-noop ctor
		}

		//- copy ctor 
		ECConfig (const ECConfig & src)
		{
			*this = src;
		}

		//- operator=
		const ECConfig & operator= (const ECConfig & src)
		{
			if (&src == this)
				return *this;
			hostDevice = src.hostDevice;
			clockGen = src.clockGen;
			return *this;
		}

	} ECConfig;

	// ============================================================================
	// class: ECThread
	// ============================================================================
	class ECThread : public yat::Thread, public Tango::LogAdapter
	{
		friend class PulseCountingManagerScalar;

	protected:
		//- ctor ---------------------------------
		ECThread (const ECConfig & cfg, yat::Thread::IOArg ioa);

		//- dtor ---------------------------------
		virtual ~ECThread (void);

		//- thread's entry point
		virtual yat::Thread::IOArg run_undetached (yat::Thread::IOArg ioa);

		//- asks this ECThread to quit
		virtual void exit (void);

	private:
		//- notify end of clock to the CountingManager task
		void end_of_clock (PulseCountingManager * cm_task);

		//- the task config
		ECConfig m_cfg;

		//- thread's ctrl flag
		bool m_goOn;
	};

// ============================================================================
// class: PulseCountingManagerScalar
// ============================================================================
	class PulseCountingManagerScalar : public PulseCountingManager
{

public:

  //- constructor
  PulseCountingManagerScalar (Tango::DeviceImpl * hostDevice);

  //- destructor
  virtual ~PulseCountingManagerScalar ();

  //- init
  void init(BoardArchitecture p_board_arch);

  //- Checks current acquisition configuration
  std::string check_configuration(bool excpt);

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg);

	//- read callback for attr counterX
	void read_counter(yat4tango::DynamicAttributeReadCallbackData & cbd);
	
	//- manage dynamics attribute
	void manage_specific_dyn_attr ();

  //- initialization
  void init_i ();

  //- start
  void start_i ();

  //- stop
  void stop_i ();

  //- periodic job
  void periodic_job_i ();

  //- wait end of finite clock
  void wait_end_clock_i ();

  //- release internal objects (clock & counters)
  void releaseObjects();

  // End of clock waiting task
  ECThread * m_endOfClock_thread;

  // map for counters value storage (common callback function)
  //- key = counter id in device (0 -> N), value = scalar counter value
  std::map<unsigned int, data_t> m_countersVal;

};

} // namespace PulseCounting_ns

#endif // _PULSE_COUNTING_MANAGER_SCALAR_H
