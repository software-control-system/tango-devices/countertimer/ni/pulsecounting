//=============================================================================
// ClockGenerator.cpp
//=============================================================================
// abstraction.......ClockGenerator for PulseCounting
// class.............ClockGenerator
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "ClockGenerator.h"


namespace PulseCounting_ns
{

   //- check counting boerd macro:
#define CHECK_CNT_BOARD() \
	do \
	{ \
	if (! m_counting_board) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the counting board isn't accessible ", \
	"PulseCountingManager::check_counting_board"); \
} while (0)

// ============================================================================
// ClockGenerator::ClockGenerator ()
// ============================================================================ 
ClockGenerator::ClockGenerator (Tango::DeviceImpl * hostDevice, CountingBoardInterface * p_board)
: yat4tango::TangoLogAdapter(hostDevice), m_counting_board(p_board)
{

}

// ============================================================================
// ClockGenerator::~ClockGenerator ()
// ============================================================================ 
ClockGenerator::~ClockGenerator ()
{

}

// ============================================================================
// ClockGenerator::init ()
// ============================================================================ 
void ClockGenerator::init(ClockConfig p_config)
{
	INFO_STREAM << "ClockGenerator::init OK" << endl;
	m_cfg = p_config;

  CHECK_CNT_BOARD();

	try
	{
		m_counting_board->initClock(p_config);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start clock generator", 
			"ClockGenerator::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start clock generator" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to start clock generator", 
			"ClockGenerator::start"); 
	}
}

// ============================================================================
// ClockGenerator::start ()
// ============================================================================ 
void ClockGenerator::start()
{
  CHECK_CNT_BOARD();

	try
	{
		m_counting_board->startClock();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start clock generator", 
			"ClockGenerator::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start clock generator" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to start clock generator", 
			"ClockGenerator::start"); 
	}
}

// ============================================================================
// ClockGenerator::abort ()
// ============================================================================ 
void ClockGenerator::abort()
{
  CHECK_CNT_BOARD();

	try
	{
		m_counting_board->abortClock();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to abort clock generator", 
			"ClockGenerator::abort"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to abort clock generator" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to abort clock generator", 
			"ClockGenerator::abort"); 
	}
}

// ============================================================================
// ClockGenerator::waitEndOfClock ()
// ============================================================================ 
void ClockGenerator::waitEndOfClock()
{
  CHECK_CNT_BOARD();

	try
	{
		m_counting_board->waitEndOfClock();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to waitEndOfClock clock generator", 
			"ClockGenerator::waitEndOfClock"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to waitEndOfClock clock generator" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to waitEndOfClock clock generator", 
			"ClockGenerator::waitEndOfClock"); 
	}
}

// ============================================================================
// ClockGenerator::deleteObject ()
// ============================================================================ 
void ClockGenerator::deleteObject()
{
  CHECK_CNT_BOARD();

	try
	{
		m_counting_board->releaseClock();
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to delete clock object..." << std::endl;
	}
}

} // namespace PulseCounting_ns

