// ============================================================================
// = CONTEXT
//		Flyscan Project - HW to SW Trigger Service
// = File
//		UDPListener.h
// = AUTHOR
//    N.Leclercq - SOLEIL
// ============================================================================

#pragma once 

#include <iostream>
#include <yat/threading/Thread.h>
#include <yat/network/ClientSocket.h>
#include <yat4tango/DeviceTask.h>

namespace udplistener
{
  
//-----------------------------------------------------------------------------
// PSEUDO CONST
//-----------------------------------------------------------------------------
#define UDP_LISTENER_NOTIF_DISABLED 0

//-----------------------------------------------------------------------------
// UDP Listener: FORWARDS THE UDP PACKETS (SENT BY THE SPI BOARD) TO A SPITASK
//-----------------------------------------------------------------------------
class UDPListener : public yat::Thread
{
private:
  //--------------------------------------------------------
  //- how to spawn a udp listener (end of seq. notif. only)
  //--------------------------------------------------------
  //- UDPListener::Config cfg;
  //- cfg.task_to_notify = this;
  //- cfg.udp_addr = 225.0.0.1; //- e.g. obtained from UDPAddress property 
  //- cfg.udp_port = 30001;     //- e.g. obtained from UDPPort property
  //- cfg.eos_notification_msg_id = (yat::FIRST_USER_MSG + 10001)
  //- my_udp_listener = new UDPListener(cfg);
  //- my_udp_listener->start_undetached();
  //- ...
  //- process notifications 
  //- ...
  //- my_udp_listener->exit();
  //--------------------------------------------------------

  //- internal/private running mode
  typedef enum
  {
    UDP_INFINITE,
    UDP_FINITE,
    UDP_STANDBY
  } Mode;
  
public:
  //--------------------------------------------------------
  // Configuration struct
  //--------------------------------------------------------
  typedef struct Config
  {
    //- the udp group address (use host addr if empty)
    std::string udp_addr;
    //- the udp port on which this listener waits for incoming data
    yat::uint32 udp_port;
    //- the udp timeout in ms (to avoid to block the thread till end of time - defaults to 1000 ms)
    yat::uint32 udp_tmo_ms;
    //- the task to which this listener posts incoming data
    yat4tango::DeviceTask * task_to_notify;
    //- id of the message to be posted to the 'task_to_notify' each time a 'UDP event is received'
    //- defaults to UDP_LISTENER_NOTIF_DISABLED which means 'disabled/no notifaction'
    size_t uer_notification_msg_id;
    //- id of the message to be posted at 'end of the sequence' - i.e. when the expected number of UDP events has beeen received
    //- defaults to UDP_LISTENER_NOTIF_DISABLED which means 'disabled/no notifaction'
    size_t eos_notification_msg_id;
    //-----------------------------------
    Config () 
        : udp_addr(),
          udp_port(0),
          udp_tmo_ms(1000),
          task_to_notify(0),
          uer_notification_msg_id(UDP_LISTENER_NOTIF_DISABLED),
          eos_notification_msg_id(UDP_LISTENER_NOTIF_DISABLED)
    {}
    //-----------------------------------
    Config (const Config& src) 
        : udp_addr(src.udp_addr),
          udp_port(src.udp_port),
          udp_tmo_ms(src.udp_tmo_ms),
          task_to_notify(src.task_to_notify),
          uer_notification_msg_id(src.uer_notification_msg_id),
          eos_notification_msg_id(src.eos_notification_msg_id)
    {}
    //-----------------------------------
    void operator= (const Config& src) 
    {
      udp_addr = src.udp_addr;
      udp_port = src.udp_port;
      udp_tmo_ms = src.udp_tmo_ms;
      task_to_notify = src.task_to_notify;
      uer_notification_msg_id = src.uer_notification_msg_id;
      eos_notification_msg_id = src.eos_notification_msg_id;
    }
  } Config; 
  
  //--------------------------------------------------------
  //- once instanciated and configured, the UDPListener must be started by a call to
  //- yat::Thread::start_undetached (inherited method)
  UDPListener (const UDPListener::Config & cfg);
  //--------------------------------------------------------
  void setup_udp_socket (yat::ClientSocket& sock);
  //--------------------------------------------------------
  //- start the UDP listener in FINITE mode (n > 0) or INFINITE mod (n = 0)
  //- in FINITE mode, 'n' is the expected number of UDP events (i.e. sequence length)
  void start (yat::uint32 n);
  //--------------------------------------------------------
  //- stop the UDP listener
  //- any UDP received after a call to stop will be ignored 
  void stop ();
  //--------------------------------------------------------
  //- ask the underlying thread to exit
  virtual void exit ();
  //--------------------------------------------------------
  //- total number of ignored UDP events since last call to UDPListener::start 
  inline yat::uint32 events_ignored () const
    { return m_ignored_events; }
  //--------------------------------------------------------
  //- total number of ignored UDP events since call to yat::Thread::start_undetached 
  inline yat::uint32 total_events_ignored () const
    { return m_total_ignored_events; }
  //--------------------------------------------------------
  //- total number of received UDP events since call to yat::Thread::start 
  inline yat::uint32 total_events () //const
    { 
      yat::AutoMutex<> guard(m_evtsLock);
      return m_total_events; 
    }

protected:
  //--------------------------------------------------------
  //- to not call directly! call 'exit' instead (the undelying impl. will clenup everything for you)
  virtual ~UDPListener ();
  //--------------------------------------------------------
  //- the thread entry point - calle by yat::Thread::start_undetached
  virtual yat::Thread::IOArg run_undetached (yat::Thread::IOArg);

private:
  Mode m_mode;
  bool m_go_on;
  yat::uint32 m_received_events;
  yat::uint32 m_expected_events;
  yat::uint32 m_ignored_events;
  yat::uint32 m_total_ignored_events;
  yat::uint32 m_total_events;
  UDPListener::Config m_cfg;
  yat::Mutex m_evtsLock;
};

} //- namespace as

