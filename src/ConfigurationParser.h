//=============================================================================
// ConfigurationParser.h
//=============================================================================
// abstraction.......Acquisition configuration parser
// class.............ConfigurationParser
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _CONFIG_PARSER_H_
#define _CONFIG_PARSER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseCountingTypesAndConsts.h"
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/LogHelper.h>

namespace PulseCounting_ns
{

//-----------------------------------------------------------------------------
//- Consts and key words for configuration parsing
//-----------------------------------------------------------------------------
static const std::string kKEY_VALUE_SEPARATOR (":");
static const std::string kVALUE_SEPARATOR_CL (":");
static const std::string kVALUE_SEPARATOR_COMA (",");

//- property strings for config
static const std::string kKEY_NAME ("Name");
static const std::string kKEY_MODE ("Mode");
static const std::string kKEY_MODE_EVT ("EVT");
static const std::string kKEY_MODE_POS ("POS");
static const std::string kKEY_MODE_DT ("DT");
static const std::string kKEY_MODE_PERIOD ("PERIOD");
static const std::string kKEY_TRANSFER ("Transfer");
static const std::string kKEY_TRANSFER_DMA ("DMA");
static const std::string kKEY_TRANSFER_ITR ("ITR");
static const std::string kKEY_EDGE ("EdgeType");
static const std::string kKEY_EDGE2 ("2ndEdgeType");
static const std::string kKEY_EDGE_RIS ("Rising");
static const std::string kKEY_EDGE_FAL ("Falling");
static const std::string kKEY_DIRECTION ("Direction");
static const std::string kKEY_DIRECTION_UP ("UP");
static const std::string kKEY_DIRECTION_DOWN ("DOWN");
static const std::string kKEY_DIRECTION_EXT ("EXTERNAL");
static const std::string kKEY_DECODING ("Decoding");
static const std::string kKEY_DECODING_X1 ("X1");
static const std::string kKEY_DECODING_X2 ("X2");
static const std::string kKEY_DECODING_X4 ("X4");
static const std::string kKEY_DECODING_TWO_PULSE ("TWO_PULSE");
static const std::string kKEY_ZINDEX ("Zindex");
static const std::string kKEY_ZINDEX_ENABLED ("Enabled");
static const std::string kKEY_ZINDEX_DISABLED ("Disabled");
static const std::string kKEY_ZINDEXPHASE ("ZindexPhase");
static const std::string kKEY_ZINDEXPHASE_AHBH ("AHighBHigh");
static const std::string kKEY_ZINDEXPHASE_AHBL ("AHighBLow");
static const std::string kKEY_ZINDEXPHASE_ALBH ("ALowBHigh");
static const std::string kKEY_ZINDEXPHASE_ALBL ("ALowBLow");
static const std::string kKEY_ZINDEXVAL ("ZindexVal");
static const std::string kKEY_ENCODERMODE ("EncoderMode");
static const std::string kKEY_ENCODERMODE_LIN ("Linear");
static const std::string kKEY_ENCODERMODE_ANG ("Angular");
static const std::string kKEY_UNITS ("Units");
static const std::string kKEY_UNITS_METER ("Meters");
static const std::string kKEY_UNITS_INCH ("Inches");
static const std::string kKEY_UNITS_DEGREE ("Degrees");
static const std::string kKEY_UNITS_RADIAN ("Radians");
static const std::string kKEY_UNITS_TICK ("Ticks");
static const std::string kKEY_UNITS_SEC ("Seconds");
static const std::string kKEY_DISTANCEPERPULSE ("DistancePerPulse");
static const std::string kKEY_PULSEPERREV ("PulsePerRev");
static const std::string kKEY_PROXY ("Proxy");
static const std::string kKEY_NEXUS ("Nexus"); // OBSOLETE code - to be deleted in NEXT version -
static const std::string kKEY_FORMAT ("Format");

//-----------------------------------------------------------------------------


// ==============================================================================
// class: ConfigurationParser
// This class provides parsing and extracting methods to get data from a CounterX
// property which contains the counter definition.
// ==============================================================================
class ConfigurationParser : public Tango::LogAdapter
{
public:

  //- constructor
  ConfigurationParser(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~ConfigurationParser();
  
  //- Parses a "Config<i>" property and fills a map containing the list of 
  //- <key,value> defining the acquisition.
  //- For channel definition, the method adds the number of the channel
  //- (in definition order) to make a unique key.
  CounterKeys_t parseConfigProperty (std::vector<std::string> config_property);

  //- Extracts name from counter definition.
  std::string extractName (CounterKeys_t ctr_config);

  //- Extracts proxy from counter definition.
  std::string extractProxy (CounterKeys_t ctr_config);

  //- Extracts mode from counter definition.
  E_CounterMode_t extractMode (CounterKeys_t ctr_config);

  //- Extracts mode from counter definition.
  std::string extractFormat (CounterKeys_t ctr_config);

  //- Extracts edge from counter definition.
  //- @param nb edge number: 1st (1) or 2nd (2)
  E_CountingEdge_t extractEdge (CounterKeys_t ctr_config, size_t nb = 1);

  //- Extracts memory transfer from counter definition.
  E_MemoryTransfer_t extractMemoryTransfer (CounterKeys_t ctr_config);

  //- Extracts direction from counter definition.
  E_CountingDirection_t extractDirection (CounterKeys_t ctr_config);

  //- Extracts decoding mode from counter definition.
  E_DecodingMode_t extractDecodingMode (CounterKeys_t ctr_config);

  //- Extracts ZIndexPhase mode from counter definition.
  E_ZIndexPhase_t extractZIndexPhase (CounterKeys_t ctr_config);

  //- Extracts encoder mode from counter definition.
  E_EncoderMode_t extractEncoderMode (CounterKeys_t ctr_config);

  //- Extracts unit from counter definition.
  E_EncoderUnit_t extractUnit (CounterKeys_t ctr_config);

  //- Extracts unit string from counter definition.
  std::string extractUnitStr (CounterKeys_t acq_config);

  //- Extracts z index value from counter definition.
  yat::uint32 extractZIndexValue (CounterKeys_t ctr_config);

  //- Extracts dist per pulse from counter definition.
  double extractDistPerPulse (CounterKeys_t ctr_config);

  //- Extracts pulse per revolution from counter definition.
  yat::uint32 extractPulsePerRev (CounterKeys_t ctr_config);

  //- Extracts z index enabled from counter definition.
  bool extractZIndexEnabled (CounterKeys_t ctr_config);

  //- Extracts nexus from counter definition.
  //- OBSOLETE function - to be deleted in NEXT version -
  bool extractNexus (CounterKeys_t ctr_config);

private:
	//- Splits a line using the specifed separator and returns the 
  //- result in a string vector.
  //- Checks expected minimum & maximum number of tokens if not null.
	std::vector<std::string> splitLine(std::string line, std::string separator, 
                                     yat::uint16 min_token = 0, yat::uint16 max_token = 0);

	//- Gets {key,value} from line using the specified separator and tells if
  //- the key word equals the specified key
  bool testKey(std::string line, std::string separator, std::string key);

	//- Tells if token is "true" or "false".
  //- Sends exception if neither one of those strings.
  //- Not case sensitive.
  bool getBoolean(std::string token);
  
};

} // namespace PulseCounting_ns

#endif // _CONFIG_PARSER_H_
