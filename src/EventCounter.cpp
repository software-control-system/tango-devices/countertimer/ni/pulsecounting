//=============================================================================
// EventCounter.cpp
//=============================================================================
// abstraction.......EventCounter for PulseCounting
// class.............EventCounter
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "EventCounter.h"

namespace PulseCounting_ns
{

	//- check manager macro:
#define CHECK_INTERFACE() \
	do \
	{ \
	if (! m_interface) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the interface isn't accessible ", \
	"EventCounter::check_interface"); \
} while (0)


// ============================================================================
// EventCounter::EventCounter ()
// ============================================================================ 
EventCounter::EventCounter (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board)
: GenericCounterInterface(hostDevice)
{
	m_is_available = true;
	m_state = Tango::STANDBY;
	m_acq_mode = p_acq_mode;
	m_interface = p_board;
	m_value = 0;
}

// ============================================================================
// EventCounter::~EventCounter ()
// ============================================================================ 
EventCounter::~EventCounter ()
{

}

// ============================================================================
// EventCounter::is_available ()
// ============================================================================ 
bool EventCounter::is_available()
{
  // Check if counter0 is enabled with isMaster = true
  // TODO?
  // NI driver throws an exception in this case...

	return m_is_available;
}

// ============================================================================
// EventCounter::init ()
// ============================================================================ 
void EventCounter::init(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->initCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to initialize evt counter!", 
			"EventCounter::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to initialize evt counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to initialize evt counter!", 
			"EventCounter::init"); 
	}
}

// ============================================================================
// EventCounter::configure ()
// ============================================================================ 
void EventCounter::configure(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->configureCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to configure evt counter!", 
			"EventCounter::configure"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to configure evt counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to configure evt counter!", 
			"EventCounter::configure"); 
	}
}

// ============================================================================
// EventCounter::start ()
// ============================================================================ 
void EventCounter::start()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->startCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start evt counter", 
			"EventCounter::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start evt counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to start evt counter", 
			"EventCounter::start"); 
	}
	m_state = Tango::RUNNING;
}

// ============================================================================
// EventCounter::stop ()
// ============================================================================ 
void EventCounter::stop()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->stopCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to stop evt counter", 
			"EventCounter::stop"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to stop evt counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to stop evt counter", 
			"EventCounter::stop"); 
	}
	m_state = Tango::STANDBY;
}

// ============================================================================
// EventCounter::get_state ()
// ============================================================================ 
Tango::DevState EventCounter::get_state()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterState(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get evt counter state!", 
			"EventCounter::get_state"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get evt counter state!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get evt counter state!", 
			"EventCounter::get_state"); 
	}
}

// ============================================================================
// EventCounter::update_scalar_value ()
// ============================================================================ 
void EventCounter::update_scalar_value()
{
  CHECK_INTERFACE();

	try
	{
    yat::AutoMutex<> guard(m_dataLock);
		m_value = m_interface->getCounterScalarValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update scalar evt counter value!", 
			"EventCounter::update_scalar_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update scalar evt counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update scalar evt counter value!", 
			"EventCounter::update_scalar_value"); 
	}
}

// ============================================================================
// EventCounter::update_buffer_value ()
// ============================================================================ 
void EventCounter::update_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
    m_interface->updateCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update buffer evt counter value!", 
			"EventCounter::update_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update buffer evt counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update buffer evt counter value!", 
			"EventCounter::update_buffer_value"); 
	}
}

// ============================================================================
// EventCounter::get_scalar_value ()
// ============================================================================ 
data_t EventCounter::get_scalar_value()
{
  yat::AutoMutex<> guard(m_dataLock);
	return m_value;
}

// ============================================================================
// EventCounter::get_buffer_value ()
// ============================================================================ 
RawData_t EventCounter::get_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get evt counter buffer value", 
			"EventCounter::get_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get evt counter buffer value" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get evt counter buffer value", 
			"EventCounter::get_buffer_value"); 
	}
}

// ============================================================================
// EventCounter::get_name ()
// ============================================================================ 
std::string EventCounter::get_name()
{
	return m_cfg.name;
}

// ============================================================================
// EventCounter::get_config ()
// ============================================================================ 
CounterConfig EventCounter::get_config()
{
	return m_cfg;
}

// ============================================================================
// EventCounter::deleteObject ()
// ============================================================================ 
void EventCounter::deleteObject()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->releaseCounter(m_cfg);
  }
  catch(...)
  {
  }
}

// ============================================================================
// EventCounter::update_last_buffer_value ()
// ============================================================================ 
void EventCounter::update_last_buffer_value()
{
  CHECK_INTERFACE();

  try
  {
    m_interface->updateCounterLastBufferValue(m_cfg);
  }
  catch (Tango::DevFailed &e)
  {
	ERROR_STREAM << e << std::endl;
	RETHROW_DEVFAILED(e,
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"EventCounter::update_last_buffer_value"); 
  }
  catch (...)
  {
	ERROR_STREAM << "Failed to update last buffer dt counter value!" << std::endl;
	THROW_DEVFAILED(
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"EventCounter::update_last_buffer_value"); 
  }
}

} // namespace PulseCounting_ns

