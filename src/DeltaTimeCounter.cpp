//=============================================================================
// DeltaTimeCounter.cpp
//=============================================================================
// abstraction.......DeltaTimeCounter for PulseCounting
// class.............DeltaTimeCounter
// original author...S.Minolli - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "DeltaTimeCounter.h"

namespace PulseCounting_ns
{

	//- check manager macro:
#define CHECK_INTERFACE() \
	do \
	{ \
	if (! m_interface) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the interface isn't accessible ", \
	"DeltaTimeCounter::check_interface"); \
} while (0)


// ============================================================================
// DeltaTimeCounter::DeltaTimeCounter ()
// ============================================================================ 
DeltaTimeCounter::DeltaTimeCounter (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_acq_mode, CountingBoardInterface * p_board)
: GenericCounterInterface(hostDevice)
{
	m_is_available = true;
	m_state = Tango::STANDBY;
	m_acq_mode = p_acq_mode;
	m_interface = p_board;
	m_value = 0;
}

// ============================================================================
// DeltaTimeCounter::~DeltaTimeCounter ()
// ============================================================================ 
DeltaTimeCounter::~DeltaTimeCounter ()
{

}

// ============================================================================
// DeltaTimeCounter::is_available ()
// ============================================================================ 
bool DeltaTimeCounter::is_available()
{
  // cannot use counter 0 with DT counter (does not count...)
  if (m_cfg.number == 0)
    m_is_available = false;

	return m_is_available;
}

// ============================================================================
// DeltaTimeCounter::init ()
// ============================================================================ 
void DeltaTimeCounter::init(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->initCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to initialize dt counter!", 
			"DeltaTimeCounter::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to initialize dt counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to initialize dt counter!", 
			"DeltaTimeCounter::init"); 
	}
}

// ============================================================================
// DeltaTimeCounter::configure ()
// ============================================================================ 
void DeltaTimeCounter::configure(CounterConfig p_cfg)
{
	m_cfg = p_cfg;

  CHECK_INTERFACE();

	try
	{
		m_interface->configureCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to configure dt counter!", 
			"DeltaTimeCounter::configure"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to configure dt counter!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to configure dt counter!", 
			"DeltaTimeCounter::configure"); 
	}
}

// ============================================================================
// DeltaTimeCounter::start ()
// ============================================================================ 
void DeltaTimeCounter::start()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->startCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to start dt counter", 
			"DeltaTimeCounter::start"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to start dt counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to start dt counter", 
			"DeltaTimeCounter::start"); 
	}
	m_state = Tango::RUNNING;
}

// ============================================================================
// DeltaTimeCounter::stop ()
// ============================================================================ 
void DeltaTimeCounter::stop()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->stopCounter(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to stop dt counter", 
			"DeltaTimeCounter::stop"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to stop dt counter" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to stop dt counter", 
			"DeltaTimeCounter::stop"); 
	}
	m_state = Tango::STANDBY;
}

// ============================================================================
// DeltaTimeCounter::get_state ()
// ============================================================================ 
Tango::DevState DeltaTimeCounter::get_state()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterState(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get dt counter state!", 
			"DeltaTimeCounter::get_state"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get dt counter state!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get dt counter state!", 
			"DeltaTimeCounter::get_state"); 
	}
}

// ============================================================================
// DeltaTimeCounter::update_scalar_value ()
// ============================================================================ 
void DeltaTimeCounter::update_scalar_value()
{
  CHECK_INTERFACE();

	try
	{
    yat::AutoMutex<> guard(m_dataLock);
		m_value = m_interface->getCounterScalarValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update scalar dt counter value!", 
			"DeltaTimeCounter::update_scalar_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update scalar dt counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update scalar dt counter value!", 
			"DeltaTimeCounter::update_scalar_value"); 
	}
}

// ============================================================================
// DeltaTimeCounter::update_buffer_value ()
// ============================================================================ 
void DeltaTimeCounter::update_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
    m_interface->updateCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to update buffer dt counter value!", 
			"DeltaTimeCounter::update_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to update buffer dt counter value!" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to update buffer dt counter value!", 
			"DeltaTimeCounter::update_buffer_value"); 
	}
}

// ============================================================================
// DeltaTimeCounter::get_scalar_value ()
// ============================================================================ 
data_t DeltaTimeCounter::get_scalar_value()
{
  yat::AutoMutex<> guard(m_dataLock);
	return m_value;
}

// ============================================================================
// DeltaTimeCounter::get_buffer_value ()
// ============================================================================ 
RawData_t DeltaTimeCounter::get_buffer_value()
{
  CHECK_INTERFACE();

	try
	{
		return m_interface->getCounterBufferValue(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to get dt counter buffer value", 
			"DeltaTimeCounter::get_buffer_value"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get dt counter buffer value" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to get dt counter buffer value", 
			"DeltaTimeCounter::get_buffer_value"); 
	}
}

// ============================================================================
// DeltaTimeCounter::get_name ()
// ============================================================================ 
std::string DeltaTimeCounter::get_name()
{
	return m_cfg.name;
}

// ============================================================================
// DeltaTimeCounter::get_config ()
// ============================================================================ 
CounterConfig DeltaTimeCounter::get_config()
{
	return m_cfg;
}

// ============================================================================
// DeltaTimeCounter::deleteObject ()
// ============================================================================ 
void DeltaTimeCounter::deleteObject()
{
  CHECK_INTERFACE();

	try
	{
		m_interface->releaseCounter(m_cfg);
  }
  catch(...)
  {
  }
}

// ============================================================================
// DeltaTimeCounter::update_last_buffer_value ()
// ============================================================================ 
void DeltaTimeCounter::update_last_buffer_value()
{
  CHECK_INTERFACE();

  try
  {
    m_interface->updateCounterLastBufferValue(m_cfg);
  }
  catch (Tango::DevFailed &e)
  {
	ERROR_STREAM << e << std::endl;
	RETHROW_DEVFAILED(e,
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"DeltaTimeCounter::update_last_buffer_value"); 
  }
  catch (...)
  {
	ERROR_STREAM << "Failed to update last buffer dt counter value!" << std::endl;
	THROW_DEVFAILED(
        "DEVICE_ERROR", 
		"Failed to update last buffer dt counter value!", 
		"DeltaTimeCounter::update_last_buffer_value"); 
  }
}

} // namespace PulseCounting_ns

