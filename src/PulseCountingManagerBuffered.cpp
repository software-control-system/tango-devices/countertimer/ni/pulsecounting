//=============================================================================
// PulseCountingManagerBuffered.cpp
//=============================================================================
// abstraction.......PulseCountingManagerBuffered for PulseCounting
// class.............PulseCountingManagerBuffered
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "PulseCountingManagerBuffered.h"

namespace PulseCounting_ns
{

	//- Check nexus manager macro:
#define CHECK_NX_MANAGER() \
	if (! m_nexus_manager) \
	{ \
	m_status = "Internal error."; \
	THROW_DEVFAILED("INTERNAL_ERROR", \
	"request aborted - the Nexus manager isn't properly initialized", \
	"PulseCountingManagerBuffered::check_nx_manager"); \
} while (0)

	//- check clock generator macro:
#define CHECK_CLK_GEN() \
	do \
	{ \
	if (! m_clock_generator) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the clock generator isn't accessible ", \
	"PulseCountingManagerBuffered::check_clk_gen"); \
} while (0)

	//- check counting boerd macro:
#define CHECK_CNT_BOARD() \
	do \
	{ \
	if (! m_counting_board) \
	THROW_DEVFAILED("DEVICE_ERROR", \
	"request aborted - the counting board isn't accessible ", \
	"PulseCountingManagerBuffered::check_counting_board"); \
} while (0)

// ============================================================================
// PulseCountingManagerBuffered::PulseCountingManagerBuffered ()
// ============================================================================ 
PulseCountingManagerBuffered::PulseCountingManagerBuffered (Tango::DeviceImpl * hostDevice)
: PulseCountingManager(hostDevice)
{
	m_acq_param.acqMode = ACQ_MODE_BUFF;
	m_acq_running = false;
  m_countersVal.clear();
  m_trigListener = NULL;
  m_currentTrigg = 0;
}

// ============================================================================
// PulseCountingManagerBuffered::~PulseCountingManagerBuffered ()
// ============================================================================ 
PulseCountingManagerBuffered::~PulseCountingManagerBuffered ()
{
	set_periodic_msg_period (0xFFFF);
	enable_periodic_msg (false);

  releaseObjects();

  if (m_trigListener)
    m_trigListener->exit();
}

// ============================================================================
// PulseCountingManagerBuffered::init ()
// ============================================================================ 
void PulseCountingManagerBuffered::init(BoardArchitecture p_board_arch)
{
  // Call init from mother class
	PulseCountingManager::init(p_board_arch);

  // initialize local map for counters value storage
  m_countersVal.clear();
  GenericCounterMap_t::iterator l_it;
  RawData_t nullBuff = NULL;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
    m_countersVal.insert(std::pair<unsigned int, RawData_t>(l_it->first, nullBuff));
  }

	try
	{
		manage_specific_dyn_attr();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e,
      "DEVICE_ERROR", 
			"Failed to add specific dynamic attributes", 
			"PulseCountingManagerBuffered::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to add specific dynamic attributes" << std::endl;
		THROW_DEVFAILED(
      "DEVICE_ERROR", 
			"Failed to add specific dynamic attributes", 
			"PulseCountingManagerBuffered::init"); 
	}

	m_counting_board->setAcquisitionTimeout(m_acq_param.timeout);
  m_counting_board->setStartTriggerUse(m_acq_param.startTriggerUse);
}

// ============================================================================
// PulseCountingManagerBuffered::process_message
// ============================================================================
void PulseCountingManagerBuffered::process_message (yat::Message& msg)
{

	//- handle msg
	switch (msg.type())
	{
		//- THREAD_INIT ----------------------
	case yat::TASK_INIT:
		{
			DEBUG_STREAM << "PulseCountingManagerBuffered::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			init_i();
		} 
		break;

		//- THREAD_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "PulseCountingManagerBuffered::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;

		//- THREAD_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			//DEBUG_STREAM << "PulseCountingManagerBuffered::handle_message::THREAD_PERIODIC" << std::endl;
			periodic_job_i();
		}
		break;

		//- THREAD_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
      //- get current trigger nb on timeout
      if (m_trigListener)
        m_currentTrigg = m_trigListener->total_events();
		}
		break;
		//- kSTART_MSG
	case kSTART_MSG:
		{
			start_i();
		}
		break;
		//- kSTOP_MSG
	case kSTOP_MSG:
		{
      {
        yat::AutoMutex<> guard(m_flagLock);
        m_requestedStop = true;
      }

      if (m_acq_running)
			  stop_i();
		}
		break;
		//- kEND_OF_SEQ_MSG
	case kEND_OF_SEQ_MSG:
		{
      DEBUG_STREAM << "PulseCountingManagerBuffered::handle_message::kEND_OF_SEQ_MSG" << std::endl;
      // On end of sequence message, if samples number not a multiple of buffer depth,
      // we have to force the reading of the last incomplete buffer on each counter.
      // If samples number is a multiple of buffer depth, the callback is naturally called
      // at the end of acquisition.
      if ((m_acq_param.samplesNumber % m_acq_param.bufferDepth) != 0)
        get_last_buffer_i();
		}
		break;
		//- kTRIGGER_RECEIVED_MSG
	case kTRIGGER_RECEIVED_MSG:
		{
		  // no more used
		}
		break;
  //- UNHANDLED MSG --------------------
	default:
		DEBUG_STREAM << "PulseCountingManagerBuffered::handle_message::unhanded msg type received" << std::endl;
		break;
	}
}

// ============================================================================
// PulseCountingManagerBuffered::init_i ()
// ============================================================================ 
void PulseCountingManagerBuffered::init_i()
{
  if (m_board_arch.clockGeneration)
  {
    // get memorized value for integrationTime if any
	  std::string prop_name = std::string("__integrationTime");
	  double val = 1.0; 
	  try
	  {
		  val = get_value_as_property<double>(m_board_arch.hostDevice, prop_name);
      set_integration_time(val);
	  }
	  catch(...)
	  {
		  // no value in db, use default value
		  set_integration_time(1.0);
	  }
    m_stored_it = val;
  }
  else
  {
    // set integration time to default value
    set_integration_time(1.0);
	}

  // get memorized value for samples nb
  try
  {
    m_acq_param.samplesNumber = get_value_as_property<long>(m_board_arch.hostDevice, "__samplesNumber");
  }
  catch(...)
  {
    // no value in db, use default value = 1
    m_acq_param.samplesNumber = 1;
  }

  // get memorized value for buffer depth
      try
      {
        m_acq_param.bufferDepth = get_value_as_property<long>(m_board_arch.hostDevice, "__bufferDepth");
      }
      catch(...)
      {
        // no value in db, use default value = 1 x integrationTime
        m_acq_param.bufferDepth = 1;
      }

	// start periodic msg
  set_periodic_msg_period((size_t)m_acq_param.pollingPeriod); //ms
	enable_periodic_msg(true);
}

// ============================================================================
// PulseCountingManagerBuffered::periodic_job_i ()
// ============================================================================ 
void PulseCountingManagerBuffered::periodic_job_i()
{
	std::vector<Tango::DevState> l_vect_state;
	Tango::DevState l_state;
  std::string countersStatus = "";

  yat::AutoMutex<> guard(m_flagLock);

	// evaluate state if manager not already in fault
	if (m_state != Tango::FAULT) 
	{
		if (!m_acq_param.continuousAcquisition)
		{
			l_state = Tango::STANDBY;
			if (m_acq_running)
			{
        m_state = Tango::STANDBY;

				// read current counters value & state from board
				GenericCounterMap_t::iterator l_it;
				for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
				{
					l_state = l_it->second->get_state();
          l_vect_state.push_back(l_state);
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);

#if !defined (USE_CALLBACK)
					l_it->second->update_buffer_value();
#endif
				}

				// Check counters state
				bool acq_error = false;
				for (unsigned int i = 0; i < l_vect_state.size(); ++i)
				{
					l_state = l_vect_state.at(i);

					if (l_state == Tango::FAULT)
					{
						m_status = "One counter in FAULT state, acquisition stopped!\n";
						acq_error = true;
						break;
					}
					else if (l_state == Tango::ALARM)
					{
						m_status = "One counter OVERRUNS, acquisition stopped!\n";
						acq_error = true;
						break;
					  }
					else if (l_state == Tango::RUNNING)
					{
						m_state = Tango::RUNNING;
						m_status = "Acquisition in progress...\n";
					}
					else if (l_state == Tango::DISABLE)
					{
						m_status = "One counter TIMES OUT, acquisition stopped!\n";
						acq_error = true;
						break;
					}
					else
					{
						  // nothing
					}
				}

				// check nexus error
				if (m_nexus_manager->hasStorageError())
				{
				  m_status = "Nexus exception handled, acquisition stopped!\n";
				}

				// if one counter in FAULT or ALARM,
				// or if nexus error, stop counting process
				if (acq_error || (m_nexus_manager->hasStorageError()))
				{
					m_state = Tango::FAULT;
          m_requestedStop = true;
					stop_i();
				}
				else
				{
					// stop acquisition if all acquisitions stopped (for Nexus & clock stuff)
					if (m_state == Tango::STANDBY)
					{
						DEBUG_STREAM << "Stop acquisition 'cause all counters finished!" << std::endl;
            m_status = "Device is up and ready.\n";
						stop_i();
					}
				}
    
        // add detailed states
        m_status += countersStatus;
			}
			else
			{
			  // read current counters state from board
			  GenericCounterMap_t::iterator l_it;
			  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
			  {
          Tango::DevState l_state = l_it->second->get_state();
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
			  }

		    m_state = Tango::STANDBY;
        m_status = "Device is up and ready.\n";
        m_status += countersStatus;
			}
		}
		else
		{
			// Continuous mode
			if (m_acq_running)
			{
        m_state = Tango::STANDBY;

				// read current counters state from board
				GenericCounterMap_t::iterator l_it;
				for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
				{
					l_state = l_it->second->get_state();
					l_vect_state.push_back(l_state);
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
				}

				// Check counters state
				bool acq_error = false;
				for (unsigned int i = 0; i < l_vect_state.size(); ++i)
				{
					l_state = l_vect_state.at(i);

					if (l_state == Tango::FAULT)
					{
						m_status = "One counter in FAULT state, acquisition stopped!\n";
						acq_error = true;
						break;
					}
					else if (l_state == Tango::ALARM)
					{
						m_status = "One counter OVERRUNS, acquisition stopped!\n";
						acq_error = true;
						break;
					}
					else if (l_state == Tango::RUNNING)
					{
						m_state = Tango::RUNNING;
						m_status = "Acquisition in progress...\n";
					}
					else if (l_state == Tango::DISABLE)
					{
						m_status = "One counter TIMES OUT, acquisition stopped!\n";
						acq_error = true;
						break;
					}
					else
					{
						// nothing
					}
				}

				// check nexus error
				if (m_nexus_manager->hasStorageError())
				{
				  m_status = "Nexus exception handled, acquisition stopped!\n";
				}

				// if one counter in FAULT or ALARM, 
				// or if nexus error, stop counting process
				if (acq_error || (m_nexus_manager->hasStorageError()))
				{
					m_state = Tango::FAULT;
          m_requestedStop = true;
					stop_i();
				}
				else
				{
					// stop acquisition if all acquisitions stopped (for Nexus & clock stuff)
					if (m_state == Tango::STANDBY)
					{
            // acquisition done => update local buffers value
				    GenericCounterMap_t::iterator l_it;
				    for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
				    {
              m_countersVal[l_it->first] = l_it->second->get_buffer_value();
				    }

						DEBUG_STREAM << "Stop acquisition 'cause all counters finished!" << std::endl;
            m_status = "Device is up and ready.\n";
						stop_i();
					}
				}

        // add detailed states
        m_status += countersStatus;
			}
			else
			{
			  // read current counters state from board
			  GenericCounterMap_t::iterator l_it;
			  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
			  {
          Tango::DevState l_state = l_it->second->get_state();
          countersStatus += std::string("\n") + l_it->second->get_name() + ": " + std::string(Tango::DevStateName[l_state]);
			  }

				m_state = Tango::STANDBY;
				m_status = "Device is up and ready.\n";
        m_status += countersStatus;
			}
		}
	}
}

// ============================================================================
// PulseCountingManagerBuffered::start_i ()
// ============================================================================ 
void PulseCountingManagerBuffered::start_i()
{
  // Check that buffer depth is not above samples number
  // if not continuous mode and not infinite mode
  if (!m_acq_param.continuousAcquisition &&
      (m_acq_param.samplesNumber != 0) &&
      (m_acq_param.bufferDepth > m_acq_param.samplesNumber))
  {
		ERROR_STREAM << "Start buffered acquisition failed! Buffer depth cannot be above samples number!" << std::endl;
		THROW_DEVFAILED(
			"CONFIGURATION_ERROR", 
      "Start error: buffer depth cannot be above samples number!", 
			"PulseCountingManagerBuffered::start_i"); 	
  }

  // Check that continuous & infinite modes are not set together
  if (m_acq_param.continuousAcquisition &&
      (m_acq_param.samplesNumber == 0))
  {
		ERROR_STREAM << "Start buffered acquisition failed! Continuous and infinite modes are exclusive!" << std::endl;
		THROW_DEVFAILED(
			"CONFIGURATION_ERROR", 
      "Start error: Continuous and infinite modes are exclusive!", 
			"PulseCountingManagerBuffered::start_i"); 	
  }

	// set current acquisition parameters (it, continuous mode, ...)
	CHECK_CNT_BOARD();

	m_counting_board->setIntegrationTime(m_acq_param.integrationTime);
	m_counting_board->setAcquisitionTimeout(m_acq_param.timeout);
	m_counting_board->setContinuousMode(m_acq_param.continuousAcquisition);
	m_counting_board->setBufferDepth(m_acq_param.bufferDepth);
	m_counting_board->setSamplesNumber(m_acq_param.samplesNumber);
  m_counting_board->setStartTriggerUse(m_acq_param.startTriggerUse);
  m_counting_board->setStorageUse(m_acq_param.nexusFileGeneration);

  // clear local data (at first start)
  if (m_firstStart)
  {
    GenericCounterMap_t::iterator l_it;
	  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	  {
      m_countersVal[l_it->first].clear();
    }    
  }

	// then configure new clock (if needed)
  if (m_isHwInitNeeded)
    configure_clock();
  
  if (!m_board_arch.clockGeneration)
  {
    // in case of external clock, 
    // start trigger listener if defined
    m_currentTrigg = 0;
    if (m_trigListener)
      m_trigListener->start((yat::uint32)m_acq_param.samplesNumber);
  }

	// init nexus manager (if needed)
	// do not re-initialize nexus file in continuous acquisition (except in 1st start)
	if (m_acq_param.nexusFileGeneration && m_firstStart)
	{
		// re-build nexus item list
		build_nexus_item_list();

    //start nexus manager
		try
		{
      // check nexus dimension:
      // - if 1D => we store buffers infinitely
      if (m_acq_param.nexusMeasDim == 1)
      {
			  m_nexus_manager->initNexusAcquisition(
				  m_acq_param.nexusTargetPath, 
				  m_acq_param.nexusFileName,
				  0, // infinite storage
				  m_acq_param.nexusNbPerFile,
				  m_nx_items);
      }
      else if ((m_acq_param.nexusMeasDim == 0) &&
               !m_acq_param.continuousAcquisition &&
               (m_acq_param.samplesNumber != 0))
      {
        // - if 0D and classical mode (i.e. not continuous nor infinite)
        //   => we store each value as a single scalar until samples number
			  m_nexus_manager->initNexusAcquisition(
				  m_acq_param.nexusTargetPath, 
				  m_acq_param.nexusFileName,
				  m_acq_param.samplesNumber, // finite storage
				  m_acq_param.nexusNbPerFile,
				  m_nx_items);
      }
      else
      {
        // - if 0D and infinite mode (i.e. continuous or infinite)
        //   => we store each value as a single scalar infinitely
			  m_nexus_manager->initNexusAcquisition(
				  m_acq_param.nexusTargetPath, 
				  m_acq_param.nexusFileName,
				  0, // infinite storage
				  m_acq_param.nexusNbPerFile,
				  m_nx_items);
      }
		}
		catch( Tango::DevFailed &df )
		{
			releaseObjects();
			ERROR_STREAM << "Init Nexus manager failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Init Nexus manager failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
		catch(...)
		{
			releaseObjects();
			ERROR_STREAM << "Init Nexus manager failed: caught[...]" << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"Init Nexus manager failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
	}

  // configure counters
  GenericCounterMap_t::iterator l_it;
  for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
  {
	  GenericCounterInterface * l_int = l_it->second;
	  try
	  {
		  if (l_int)
		  {
        l_int->init(m_counterListConfig[l_int]); 
			  l_int->configure(m_counterListConfig[l_int]);
		  }
	  }
	  catch( Tango::DevFailed &df )
	  {
		  releaseObjects();
      ERROR_STREAM << "Counter configuration failed: " << df << std::endl;
		  RETHROW_DEVFAILED(df,
			  "SOFTWARE_FAILURE", 
			  "Counter configuration failed.", 
			  "PulseCountingManagerBuffered::start_i"); 	
	  }
	  catch(...)
	  {
		  releaseObjects();
      ERROR_STREAM << "Counter configure failed: caught [...]" << std::endl;
		  THROW_DEVFAILED(
			  "SOFTWARE_FAILURE", 
			  "Counter configuration failed.", 
			  "PulseCountingManagerBuffered::start_i"); 	
	  }
  }

	// start counters
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
		try
		{
			l_it->second->start();
		}
		catch( Tango::DevFailed &df )
		{
			releaseObjects();
      ERROR_STREAM << "Counter start failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Counter start failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
		catch(...)
		{
			releaseObjects();
			ERROR_STREAM << "Counter start failed." << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"Counter start failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
	}

	// start clock (if master)
	if (m_board_arch.clockGeneration)
	{
		try
		{
			m_clock_generator->start();
		}
		catch( Tango::DevFailed &df )
		{
			releaseObjects();
			ERROR_STREAM << "Start clock failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Start clock failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
		catch(...)
		{
			releaseObjects();
      ERROR_STREAM << "Start clock failed: caught [...]" << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"Start clock failed.", 
				"PulseCountingManagerBuffered::start_i"); 	
		}
	}

	m_acq_running = true;
}

// ============================================================================
// PulseCountingManagerBuffered::stop_i ()
// ============================================================================ 
void PulseCountingManagerBuffered::stop_i()
{
	// stop clock (if master)
	if (m_board_arch.clockGeneration)
	{
		CHECK_CLK_GEN();
		try
		{
			DEBUG_STREAM << "Trying to stop clock" << endl;
			m_clock_generator->abort();
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Abort clock failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Abort clock failed.", 
				"PulseCountingManagerBuffered::stop_i"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Abort clock failed." << std::endl;
			THROW_DEVFAILED(
				"SOFTWARE_FAILURE", 
				"Abort clock failed.", 
				"PulseCountingManagerBuffered::stop_i"); 	
		}
	}
  else // stop trigger listener (if defined)
  {
    if (m_trigListener)
      m_trigListener->stop();
  }

	// stop counters
	GenericCounterMap_t::iterator l_it;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
		try
		{
			l_it->second->stop();
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Counter stop failed " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Counter stop failed.", 
				"PulseCountingManagerBuffered::stop_i"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Counter stop failed." << std::endl;
			THROW_DEVFAILED("SOFTWARE_FAILURE", 
				"Counter stop failed.", 
				"PulseCountingManagerBuffered::stop_i"); 	
		}
	}

	// Store Nexus data
  if (m_acq_param.nexusFileGeneration)
	{
		for (l_it = m_counterList.begin(); l_it != m_counterList.end(); l_it++)
		{
			if (l_it->second->get_config().nexus)
			{
				RawData_t l_val = l_it->second->get_buffer_value();

        // Data not stored at stop but in handle_xxx_buffer() functions
			}
		}
		// Finalize Nexus file
		if (!m_acq_param.continuousAcquisition ||
       (m_requestedStop && m_acq_param.continuousAcquisition))
		{
			try
			{
				INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
				m_nexus_manager->finalizeNexusGeneration();
			}
			catch (Tango::DevFailed & df)
			{
				ERROR_STREAM << "Stop Nexus failed : " << df << std::endl;
			}
		}
	}

	m_acq_running = false;

	// check continuous mode to restart acquisition if not requested stop
  if (!m_requestedStop && m_acq_param.continuousAcquisition)
	{
		m_firstStart = false;
		start_i();
	}
}

// ============================================================================
// PulseCountingManagerBuffered::manage_dyn_attr ()
// ============================================================================ 
void PulseCountingManagerBuffered::manage_specific_dyn_attr() 
{
	if (! m_dyn_attr_manager)
  {
		THROW_DEVFAILED(
    "DEVICE_ERROR", 
		"The dynamic attribute manager object isn't accessible ", 
		"PulseCountingManagerBuffered::check_manager_dyn_attr_manager"); 
  }

  // Buffered mode associated attributes:
  unsigned int attr_nb = 2;

  if (!m_trigListener)
    attr_nb = 2; // no trigger listener
  else
    attr_nb = 3; // trigger listener available
  std::vector<yat4tango::DynamicAttributeInfo> dai(attr_nb);

  // Buffer depth
	dai[0].dev = m_board_arch.hostDevice;
	dai[0].tai.name = BUFFER_DEPTH;
	dai[0].tai.label = BUFFER_DEPTH_LABEL; 
	//- describe the dyn attr we want...
	dai[0].tai.data_type = Tango::DEV_LONG; 
	dai[0].tai.data_format = Tango::SCALAR;
  dai[0].tai.writable = Tango::READ_WRITE;
	dai[0].tai.disp_level = Tango::OPERATOR;
	dai[0].tai.description = "Intermediate buffer depth, as a multiple of integration time (if defined), or in number of points (if integration time not defined).";
	//- attribute properties:
  if (m_board_arch.clockGeneration)
  {
    // buffer depth is defined as a multiple of integration time
	  dai[0].tai.unit = "x integration time";
	  dai[0].tai.standard_unit = "s";
	  dai[0].tai.display_unit = "x integration time";
  }
  else
  {
    // buffer depth is defined as a number (in this case, it = 1s)
	  dai[0].tai.unit = " ";
	  dai[0].tai.standard_unit = " ";
	  dai[0].tai.display_unit = " ";
  }

	dai[0].tai.format = "%6d";
	dai[0].tai.min_value = "0";

	//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	dai[0].cdb = false;

	//- read callback
	dai[0].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseCountingManagerBuffered::read_buffer_depth);
  //- write callback
  dai[0].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
	  &PulseCountingManagerBuffered::write_buffer_depth);

  // Samples number
	dai[1].dev = m_board_arch.hostDevice;
	dai[1].tai.name = SAMPLES_NUMBER;
	dai[1].tai.label = SAMPLES_NUMBER_LABEL; 
	//- describe the dyn attr we want...
	dai[1].tai.data_type = Tango::DEV_LONG;
	dai[1].tai.data_format = Tango::SCALAR;
	dai[1].tai.writable = Tango::READ_WRITE;
	dai[1].tai.disp_level = Tango::OPERATOR;
	dai[1].tai.description = "Samples number";
	//- attribute properties:
	dai[1].tai.unit = " ";
	dai[1].tai.standard_unit = " ";
	dai[1].tai.display_unit = " ";

	dai[1].tai.format = "%6d";
	dai[1].tai.min_value = "0";

	//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	dai[1].cdb = false;
	//- read callback
	dai[1].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseCountingManagerBuffered::read_samples_number);
	//- write callback
	dai[1].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
		&PulseCountingManagerBuffered::write_samples_number);

  if (m_trigListener)
  {
    // Current trigger number (= current point) 
    // Available in case of trigger listener
	  dai[2].dev = m_board_arch.hostDevice;
	  dai[2].tai.name = CURRENT_POINT;
	  dai[2].tai.label = CURRENT_POINT; 
	  //- describe the dyn attr we want...
	  dai[2].tai.data_type = Tango::DEV_LONG;
	  dai[2].tai.data_format = Tango::SCALAR;
	  dai[2].tai.writable = Tango::READ;
	  dai[2].tai.disp_level = Tango::OPERATOR;
	  dai[2].tai.description = "Current point in acquisition buffer";
	  //- attribute properties:
	  dai[2].tai.unit = " ";
	  dai[2].tai.standard_unit = " ";
	  dai[2].tai.display_unit = " ";
	  dai[2].tai.format = "%6d";

	  //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	  dai[2].cdb = false;
	  //- read callback
	  dai[2].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		  &PulseCountingManagerBuffered::read_current_point);
  }

	try
	{
		//- add all dynamic attributes to the device interface
		m_dyn_attr_manager->add_attributes(dai);
	}
	catch( Tango::DevFailed &df )
	{
		ERROR_STREAM << "Dynamic attribute addition failed: " << df << std::endl;
		RETHROW_DEVFAILED(df,
      "SOFTWARE_FAILURE", 
			"Dynamic attribute addition failed.", 
			"PulseCountingManagerBuffered::manage_specific_dyn_attr"); 	
	}
	catch(...)
	{
		ERROR_STREAM << "Dynamic attribute addition failed." << std::endl;
		THROW_DEVFAILED(
      "SOFTWARE_FAILURE", 
			"Dynamic attribute addition failed.", 
			"PulseCountingManagerBuffered::manage_specific_dyn_attr"); 	
	}

  // Counters associated attributes:
	counterConfigMap_t::iterator l_it;
	for (l_it = m_board_arch.counterConfiguration.begin(); l_it != m_board_arch.counterConfiguration.end(); ++l_it)
	{
		CounterConfig p_cfg = l_it->second;
    yat4tango::DynamicAttributeInfo dai;
    unsigned int l_id = p_cfg.ctNumber; // key = counter ID in device (unique)

		//Counter attribute
		Tango::AttrWriteType rw_mode = Tango::READ;
		int attr_type = Tango::DEV_DOUBLE;
		std::string attrFormat = p_cfg.format;
		std::string attrDescription = "Counter value";
		dai.dev = m_board_arch.hostDevice;
		dai.tai.name = p_cfg.name;
		dai.tai.writable = rw_mode;
		dai.tai.data_format = Tango::SPECTRUM;
		dai.tai.format = attrFormat;
		dai.tai.data_type = attr_type;
		dai.tai.disp_level = Tango::OPERATOR;
		dai.tai.description = attrDescription;
        dai.tai.unit = p_cfg.pos_unitStr;
		dai.tai.max_dim_x = LONG_MAX;
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
			&PulseCountingManagerBuffered::read_counter);
		dai.set_user_data(l_id);

		//- add all dynamic attributes to the device interface
		try
		{
			//- add all dynamic attributes to the device interface
			m_dyn_attr_manager->add_attribute(dai);
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Dynamic attribute addition failed: " << df << std::endl;
			RETHROW_DEVFAILED(df,
        "SOFTWARE_FAILURE", 
				"Dynamic attribute addition failed.", 
				"PulseCountingManagerBuffered::manage_specific_dyn_attr"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Dynamic attribute addition failed." << std::endl;
			THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
				"Dynamic attribute addition failed.", 
				"PulseCountingManagerBuffered::manage_specific_dyn_attr"); 	
		}
	}
}

// ============================================================================
// PulseCountingManagerBuffered::read_counter ()
// ============================================================================ 
void PulseCountingManagerBuffered::read_counter(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  // get counter id
	unsigned int l_id = 0;
	cbd.dya->get_user_data<unsigned int>(l_id);

  // in continuous mode, the local counter value is set by periodic job,
  // at the end of each acquisition
  if (!m_acq_param.continuousAcquisition)
  {
    try
    {
      m_countersVal[l_id] = m_counterList[l_id]->get_buffer_value();
    }
    catch(...)
    {
      m_countersVal[l_id].clear();
    }
  }

	if (m_countersVal[l_id].length() != 0)
	{
	  cbd.tga->set_value(m_countersVal[l_id].base(), m_countersVal[l_id].length());
	}
}

// ============================================================================
// PulseCountingManagerBuffered::read_buffer_depth ()
// ============================================================================ 
void PulseCountingManagerBuffered::read_buffer_depth(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static Tango::DevLong l_attr;

  if (m_acq_param.continuousAcquisition)
  {
    // in this case, buffer depth is not useful
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
	l_attr = static_cast<Tango::DevLong>(m_acq_param.bufferDepth);
	cbd.tga->set_value(&l_attr);
    cbd.tga->set_quality(Tango::ATTR_VALID);
  }
}

// ============================================================================
// PulseCountingManagerBuffered::write_buffer_depth ()
// ============================================================================ 
void PulseCountingManagerBuffered::write_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  if (m_acq_param.continuousAcquisition)
  {
    // in this case, buffer depth is not useful
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
	  Tango::DevLong l_val;
	  cbd.tga->get_write_value(l_val);

  // check device state
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManagerBuffered::write_buffer_depth"); 
  }

  // check if null or negative value
  if (l_val <= 0)
  {
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			"Buffer depth should be a strictly positive value!", 
			"PulseCountingManagerBuffered::write_buffer_depth"); 
  }

  m_acq_param.bufferDepth = l_val;

	// memorize new value
	yat4tango::PropertyHelper::set_property<long>(m_board_arch.hostDevice, "__bufferDepth", m_acq_param.bufferDepth);
}
}

// ============================================================================
// PulseCountingManagerBuffered::read_samples_number ()
// ============================================================================ 
void PulseCountingManagerBuffered::read_samples_number(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static Tango::DevLong l_attr;
	l_attr = static_cast<Tango::DevLong>(m_acq_param.samplesNumber);

	cbd.tga->set_value(&l_attr);
}

// ============================================================================
// PulseCountingManagerBuffered::write_samples_number ()
// ============================================================================ 
void PulseCountingManagerBuffered::write_samples_number(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	Tango::DevLong l_val;
	cbd.tga->get_write_value(l_val);

  // check device state
  if (m_state == Tango::RUNNING)
  {
    THROW_DEVFAILED(
      "DEVICE_ERROR", 
      "Attribute not writable while acquisition is running!", 
      "PulseCountingManagerBuffered::write_samples_number"); 
  }

  // check if negative value (null value = infinite mode)
  if (l_val < 0)
  {
		THROW_DEVFAILED(
      "CONFIGURATION_ERROR", 
			"Samples number should be a strictly positive value!", 
			"PulseCountingManagerBuffered::write_samples_number"); 
  }

	try
	{
		m_counting_board->setSamplesNumber(l_val);
		m_acq_param.samplesNumber = l_val;

		// memorize new value
		yat4tango::PropertyHelper::set_property<long>(m_board_arch.hostDevice, "__samplesNumber", m_acq_param.samplesNumber);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      "DEVICE_ERROR", 
			"Failed to set samples number", 
			"PulseCountingManagerBuffered::write_samples_number"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to set samples number" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to set samples number", 
			"PulseCountingManagerBuffered::write_samples_number"); 
	}
}

// ============================================================================
// PulseCountingManagerBuffered::read_current_point ()
// ============================================================================ 
void PulseCountingManagerBuffered::read_current_point(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static Tango::DevLong l_attr;
	l_attr = static_cast<Tango::DevLong>(m_currentTrigg);

	cbd.tga->set_value(&l_attr);
}

// ============================================================================
// PulseCountingManagerBuffered::releaseObjects
// ============================================================================
void PulseCountingManagerBuffered::releaseObjects()
{
	// release clock
	try
	{
	  if (m_clock_generator)
	  {
		  m_clock_generator->deleteObject();
      m_isHwInitNeeded = true;
	  }
	}
	catch(...)
	{
	}

	// release counters
	GenericCounterMap_t::iterator e_it;
	for (e_it = m_counterList.begin(); e_it != m_counterList.end(); ++e_it)
	{
		GenericCounterInterface * e_cpt = e_it->second;
		try
		{
			if (e_cpt)
			{
				e_cpt->deleteObject();
			}
		}
		catch(...)
		{
		}
	}
}

// ============================================================================
// PulseCountingManagerBuffered::check_configuration
// ============================================================================
std::string PulseCountingManagerBuffered::check_configuration(bool excpt)
{
  yat::OSStream oss;

  // First check if valid configuration
  if ((m_acq_param.continuousAcquisition) &&
      (m_acq_param.samplesNumber == 0))
  {
    oss << "WARNING! Configuration is NOT approved!" << std::endl;
    oss << "Continuous and infinite acquisition cannot be set together!" << std::endl;
    oss << "Disable continuous flag or set total samples number to a strictly positive value." << std::endl;

    if (excpt)
    {
      // send exception
      THROW_DEVFAILED(
        "CONFIGURATION_WARNING", 
        oss.str().c_str(), 
        "PulseCountingManagerBuffered::check_configuration");
    }
  }

  // Check if continuous acquisition
  if (m_acq_param.continuousAcquisition)
  {
    // In this case, buffer depth will be set to samples number automatically,
    // so no problem
    oss << "Configuration is APPROVED!" << std::endl;
    oss << "Continuous acquisition, so buffer depth will be set to samples number:" << std::endl;
    oss << "no intermediate buffers will be received." << std::endl;

    if (m_board_arch.clockGeneration)
    {
      oss << "buffers will be received every " << m_acq_param.samplesNumber * m_acq_param.integrationTime << " seconds." << std::endl;
    }

    // compute the number of needed clock ticks for whole acquisition
    unsigned long nb_clock = m_acq_param.samplesNumber;
    
    // add one tick if start trigger used
    if (m_acq_param.startTriggerUse)
      nb_clock += 1;
    oss << "Number of clock ticks needed for each acquisition is: " << nb_clock << std::endl;
  }
  // Check if infinite acquisition
  else if (m_acq_param.samplesNumber == 0)
  {
    // Infinite acquisition, to be stopped by Stop command
    oss << "Configuration is APPROVED!" << std::endl;
    oss << "Infinite acquisition." << std::endl;

    if (m_board_arch.clockGeneration)
    {
      oss << "Buffers will be received every " << m_acq_param.bufferDepth * m_acq_param.integrationTime << " seconds." << std::endl;
    }

    // compute the number of needed clock ticks for whole acquisition
    unsigned long nb_clock = m_acq_param.bufferDepth;
    
    // add one tick if start trigger used
    if (m_acq_param.startTriggerUse)
      nb_clock += 1;
    oss << "Number of clock ticks needed for each buffer is: " << nb_clock << std::endl;
  }
  else
  {
    // Check if the current value of samples number is a multiple of current buffer depth value
    // If not multiple, if external clock & trigger listener option set, config is approved
    // because last buffer will be read, even if not complete.
    if ( ((m_acq_param.samplesNumber % m_acq_param.bufferDepth) == 0) ||
         ( ((m_acq_param.samplesNumber % m_acq_param.bufferDepth) != 0) &&
           (!m_board_arch.clockGeneration) &&
           (m_board_arch.udp_address.size() != 0) &&
           (m_board_arch.udp_port != 0)) 
        )
    {
      // buffer depth divides samples number, configuration is approved
      oss << "Configuration is APPROVED!" << std::endl;
      if ((m_acq_param.samplesNumber % m_acq_param.bufferDepth) == 0)
        oss << "Buffer depth divides samples number." << std::endl;
      else
        oss << "UDP listener option activated, last incomplete buffer will be received without extra triggers." << std::endl;

      oss << "Intermediate buffers will be received every " << m_acq_param.bufferDepth << " points";

      if (m_board_arch.clockGeneration)
      {
        oss << " (= every " << m_acq_param.bufferDepth * m_acq_param.integrationTime << " seconds)." << std::endl;
        oss << " Whole acquisition will last " << m_acq_param.samplesNumber * m_acq_param.integrationTime << " seconds." << std::endl;
      }
      else
      {
        oss << std::endl;
      }

      // compute the number of needed clock ticks for whole acquisition
      unsigned long nb_clock = m_acq_param.samplesNumber;

      // add one tick if start trigger used
      if (m_acq_param.startTriggerUse)
        nb_clock += 1;
      oss << "Number of clock ticks needed for whole acquisition is: " << nb_clock << std::endl;
    }
    else
    {
      // buffer depth does not divides samples number, configuration is not approved
      oss << "WARNING! Configuration is NOT approved!" << std::endl;
      oss << "Buffer depth does not divide samples number." << std::endl;
      oss << "Intermediate buffers will be received every " << m_acq_param.bufferDepth << " points";

      unsigned long nb_buffers 
        = (unsigned long)((m_acq_param.samplesNumber + m_acq_param.bufferDepth - 1) / m_acq_param.bufferDepth);

      if (m_board_arch.clockGeneration)
      {
        oss << " (= every " << m_acq_param.bufferDepth * m_acq_param.integrationTime << " seconds)." << std::endl;
        oss << " Whole acquisition will last " << nb_buffers * m_acq_param.bufferDepth * m_acq_param.integrationTime << " seconds." << std::endl;
      }
      else
      {
        oss << ". Activate UDP listener option to get last incomplete buffer without extra triggers." << std::endl;
        oss << std::endl;
      }

      // compute the number of needed clock ticks for whole acquisition
      unsigned long nb_clocks = nb_buffers * m_acq_param.bufferDepth;

      // add one tick if start trigger used
      if (m_acq_param.startTriggerUse)
        nb_clocks += 1;
      oss << "Number of clock ticks needed for whole acquisition is: " << nb_clocks << std::endl;

      if (excpt)
      {
        // send exception
        THROW_DEVFAILED(
          "CONFIGURATION_WARNING", 
          oss.str().c_str(), 
          "PulseCountingManagerBuffered::check_configuration");
      }
    }
  }

  return oss.str();
}

// ============================================================================
// PulseCountingManagerBuffered::configure_trigger_listener
// ============================================================================
void PulseCountingManagerBuffered::configure_trigger_listener(std::string addr, yat::uint32 port)
{
  DEBUG_STREAM << "PulseCountingManagerBuffered::configure_trigger_listener() entering..." << std::endl;
  DEBUG_STREAM << " UDP listener on addr: " << addr << " - port: " << port << std::endl;

  // configure UDP listener
  udplistener::UDPListener::Config cfg;
  cfg.task_to_notify = this;
  cfg.udp_addr = addr;
  cfg.udp_port = port;
  cfg.eos_notification_msg_id = kEND_OF_SEQ_MSG;
  m_trigListener = new udplistener::UDPListener(cfg);

  if (!m_trigListener)
  {
    DEBUG_STREAM << "Failed to create UDP listener object! Check UDPaddress & UDPPort properties!" << std::endl;
    // send exception
    THROW_DEVFAILED(
      "CONFIGURATION_FAILURE", 
      "Failed to create UDP listener object! Check UDPaddress & UDPPort properties!", 
      "PulseCountingManagerBuffered::configure_trigger_listener");
  }

  m_trigListener->start_undetached();

  // enable timeout msg for trigger counter
  // (better to ask on timeout the current trigger number
  // than manage the ues messages when acquisition freq 
  // is high : avoid task message queue overflow)
	set_timeout_msg_period (kTIMEOUT_MSG_PERIOD_MS);
	enable_timeout_msg (true);

  DEBUG_STREAM << "PulseCountingManagerBuffered::configure_trigger_listener() ok" << std::endl;
}

// ============================================================================
// PulseCountingManagerBuffered::get_last_buffer_i ()
// ============================================================================ 
void PulseCountingManagerBuffered::get_last_buffer_i()
{
  DEBUG_STREAM << "PulseCountingManagerBuffered::get_last_buffer_i() entering..." << std::endl;

  // force reading last buffer for each counter
	GenericCounterMap_t::iterator l_it;
	for (l_it = m_counterList.begin(); l_it != m_counterList.end(); ++l_it)
	{
		try
		{
			l_it->second->update_last_buffer_value();
		}
		catch( Tango::DevFailed &df )
		{
			ERROR_STREAM << "Counter get last buffer failed " << df << std::endl;
			RETHROW_DEVFAILED(df,
				"SOFTWARE_FAILURE", 
				"Counter get last buffer failed.", 
				"PulseCountingManagerBuffered::get_last_buffer_i"); 	
		}
		catch(...)
		{
			ERROR_STREAM << "Counter get last buffer failed." << std::endl;
			THROW_DEVFAILED(
        "SOFTWARE_FAILURE", 
				"Counter get last buffer failed.", 
				"PulseCountingManagerBuffered::get_last_buffer_i"); 	
		}
	}
}

} // namespace PulseCounting_ns

