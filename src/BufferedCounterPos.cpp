//=============================================================================
// BufferedCounterPos.cpp
//=============================================================================
// abstraction.......BufferedCounterPos for PulseCounting
// class.............BufferedCounterPos
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "BufferedCounterPos.h"

namespace PulseCounting_ns
{

// ============================================================================
// BufferedCounterPos::BufferedCounterPos ()
// ============================================================================ 
BufferedCounterPos::BufferedCounterPos (BCEconfig p_conf, NexusManager * storage)
: yat4tango::TangoLogAdapter(p_conf.hostDevice)
{
	m_bufferNbToReceive = 0;
	m_currentBufferNb = 0;
	m_firstPtReceivedBuff = 0;
	m_lastReceivedValue = 0.0;
	m_acquisitionDone = true;
	m_dataBuffer_thread = NULL;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;
	m_data.clear();
	m_cfg = p_conf;
  m_storage_mgr = storage;
}

// ============================================================================
// BufferedCounterPos::~BufferedCounterPos ()
// ============================================================================ 
BufferedCounterPos::~BufferedCounterPos ()
{
	
}

// ============================================================================
// BufferedCounterPos::updateScaledBuffer
// ============================================================================
void BufferedCounterPos::updateScaledBuffer()
{
	// Delete "buffer recover" thread if exists and if data recovery is over
	if (m_dataBuffer_thread)
	{
		if (m_dataBuffer_thread->updateDone())
		{
			yat::Thread::IOArg ioa;
			m_dataBuffer_thread->exit();
			m_dataBuffer_thread->join(&ioa);
			//std::cout << "BufferedCounterPos::updateScaledBuffer - BR thread exited for counter " 
			//  << m_cfg.cnt.number << std::endl;
			m_dataBuffer_thread = NULL; 
		}
	}

	// Task the buffer reception if acquisition is on & if previous buffer recovery is over
	if ((!m_acquisitionDone) && 
		  (m_dataBuffer_thread == NULL))
	{
		m_dataBuffer_thread = new BRPThread(m_cfg.hostDevice, static_cast<yat::Thread::IOArg>(this));
		DEBUG_STREAM << "BufferedCounterPos Thread created ..." << endl;
		if (m_dataBuffer_thread == NULL)
		{
			ERROR_STREAM << "Buffer recovery cannot be tasked for counter " 
				<< m_cfg.cnt.number << std::endl;
			THROW_DEVFAILED(
        "DEVICE_ERROR",
        "Failed to start buffer recover task!",
        "BufferedCounterPos::updateScaledBuffer");    
		}
		//std::cout << "BufferedCounterPos::updateScaledBuffer - starting Buffer recovery thread for counter " 
		//  << m_cfg.cnt.number << std::endl;
		m_dataBuffer_thread->start_undetached();   
	}
}

// ============================================================================
// BufferedCounterPos::getScaledBuffer
// ============================================================================
void BufferedCounterPos::getScaledBuffer()
{
	// call "get scaled buffer" to compose the whole buffer.
	// In "polling" mode, the client has to poll to compose the acquisition buffer.
	if (!m_acquisitionDone)
	{
		DEBUG_STREAM << "BufferedCounterPos::getScaledBuffer ..." << endl;
		//- from NI660Xsl - calls ni660Xsl::handle_xxx methods
		try
		{
			ni660Xsl::BufferedPositionMeasurement::get_scaled_buffer(); // the driver waits 'buffer depth' seconds...
		}
		catch(ni660Xsl::DAQException & nie)
		{
			throw_devfailed(nie);
		}
	}
}

// ============================================================================
// BufferedCounterPos::handle_timeout ()
// ============================================================================ 
void BufferedCounterPos::handle_timeout()
{
	ERROR_STREAM << "BufferedCounterPos::handle_timeout() entering..." << endl;
  m_cntTimedout = true;
}

// ============================================================================
// BufferedCounterPos::handle_data_lost ()
// ============================================================================ 
void BufferedCounterPos::handle_data_lost()
{
	ERROR_STREAM << "BufferedCounterPos::handle_data_lost() entering..." << endl;
  m_cntOverrun = true;
}

// ============================================================================
// BufferedCounterPos::handle_raw_buffer ()
// ============================================================================ 
void BufferedCounterPos::handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read)
{
	//- Event values are not raw !!!
	delete buffer;
}

// ============================================================================
// BufferedCounterPos::handle_scaled_buffer ()
// ============================================================================ 
void BufferedCounterPos::handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read)
{	
	DEBUG_STREAM << "BufferedCounterPos::handle_scaled_buffer() entering for counter: " << m_cfg.cnt.name << " ... " 
	             << " - samples to read = " << _samples_read << std::endl;

  // check samples number is > 0
  if (_samples_read <= 0)
  {
    ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> samples to read negative or null value!" << std::endl;
    stopCnt();
	return;
  }

  yat::AutoMutex<> guard(m_buffLock);

	ni660Xsl::InScaledBuffer& buf = *buffer;
  RawData_t & pos = m_data;
  bool data_to_be_stored = (m_cfg.acq.nexusFileGeneration && m_cfg.cnt.nexus);

	//- Continuous acquisition: only one buffer is received !!
	if (m_cfg.acq.continuousAcquisition)
	{
    DEBUG_STREAM << "continuous acquisition: nb buffer to receive = 1!" << std::endl;

    // Take initial position into account for 1st point when requested
    if (m_cfg.cnt.applyInitPos) 
      pos[0] = (m_cfg.cnt.absInitPos + buf[0]) / 2;
    else
		  pos[0] = buf[0];

		//- Compute the mean between 2 points
		//- mean = (X(n-1) + Xn ) / 2 
		for (unsigned long idx = 1; idx < (unsigned long)_samples_read; idx++)
		{
			pos[idx] = (buf[idx-1] + buf[idx] ) / 2;
		}

    // store received buffer in Nexus, if nx storage enabled for this counter
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &pos[0], (unsigned long)_samples_read);
      }
      catch (Tango::DevFailed & df)
      {
	      ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
	      ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }

    // stop acquisition
		stopCnt();
	}
  else if (m_bufferNbToReceive == 0) //- infinite mode
  {
    DEBUG_STREAM << "one buffer received." << std::endl;

		unsigned long nb_to_copy;
    nb_to_copy = (unsigned long)_samples_read;
		DEBUG_STREAM << "NB to copy : " << nb_to_copy << endl;

    // tempo buffer for storage
    RawData_t bufNx;
    if (data_to_be_stored)
    {
      bufNx.capacity(nb_to_copy);
      bufNx.force_length(nb_to_copy);
    }

    //- Compute the mean between 2 points
    //- mean = (X(n-1) + Xn ) / 2 
    for (unsigned long idx = 1; idx < nb_to_copy; idx++)
    {
      pos[idx] = (buf[idx-1] + buf[idx]) / 2;
      if (data_to_be_stored)
        bufNx[idx] = pos[idx];
    }

    //- Compute 1st point:
    //- at start, m_firstPtReceivedBuff and m_lastReceivedValue equal 0.
    //- m_firstPtReceivedBuff is the position of the 1st point of the buffer
    //- m_lastReceivedValue is the last value of the n-1 buffer
    if (m_firstPtReceivedBuff == 0)
    {
      // Take initial position into account for 1st point when requested
      if (m_cfg.cnt.applyInitPos) 
        pos[0] = (m_cfg.cnt.absInitPos + buf[0]) / 2;
      else
	    pos[0] = buf[0];

      m_firstPtReceivedBuff = 1; // Apply this just for the 1st point of the 1st buffer
    }
    else
    {
	    pos[0] = (buf[0] + m_lastReceivedValue) / 2;
    }

    if (data_to_be_stored)
      bufNx[0] = pos[0];

    //- memorize the last value of buf (will be used for the next buf)
    m_lastReceivedValue = buf[nb_to_copy - 1];


    // store received buffer in Nexus, if nx storage enabled for this counter
    if (data_to_be_stored && m_storage_mgr)
    {
      try
      {
        m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &bufNx[0], nb_to_copy);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
      catch (...)
      {
        ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caugth unknown exception!" << std::endl;
        // We should stop acquisition if nx problem!
        m_storageError = true;
        stopCnt();
      }
    }
  }
	else //- not continuous nor infinite
	{
		//- All buffers have not been yet received
		if (m_currentBufferNb < m_bufferNbToReceive)
		{
      DEBUG_STREAM << "buffer currently received for counter " << m_cfg.cnt.name << " = " << m_currentBufferNb + 1 
	               << " on " << m_bufferNbToReceive << std::endl;
			unsigned long nb_to_copy;

			if (((unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff) < (unsigned long)_samples_read)
			{
				nb_to_copy = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
			}
			else
			{
				nb_to_copy = (unsigned long)_samples_read;
			}
			DEBUG_STREAM << "NB to copy for counter " << m_cfg.cnt.name << ": " << nb_to_copy << endl;

			// add number to copy to current data
			pos.capacity(pos.length() + nb_to_copy, true);
			pos.force_length(pos.length() + nb_to_copy);
			
      // tempo buffer for storage
      RawData_t bufNx;
      if (data_to_be_stored)
      {
        bufNx.capacity(nb_to_copy);
        bufNx.force_length(nb_to_copy);
      }

		  //- Compute the mean between 2 points
		  //- mean = (X(n-1) + Xn ) / 2 
		  for (unsigned long idx = 1; idx < nb_to_copy; idx++)
		  {
			  pos[idx + m_firstPtReceivedBuff] = (buf[idx-1] + buf[idx]) / 2;
        if (data_to_be_stored)
          bufNx[idx] = pos[idx + m_firstPtReceivedBuff];
		  }

      //- Compute 1st point:
			//- at start, m_firstPtReceivedBuff and m_lastReceivedValue equal 0.
			//- m_firstPtReceivedBuff is the position of the 1st point of the buffer
			//- m_lastReceivedValue is the last value of the n-1 buffer
      if (m_firstPtReceivedBuff == 0)
      {
        // Take initial position into account for 1st point when requested
        if (m_cfg.cnt.applyInitPos) 
          pos[m_firstPtReceivedBuff] = (m_cfg.cnt.absInitPos + buf[0]) / 2;
        else
		      pos[m_firstPtReceivedBuff] = buf[0];
      }
      else
      {
			  pos[m_firstPtReceivedBuff] = (buf[0] + m_lastReceivedValue) / 2;
      }

      if (data_to_be_stored)
        bufNx[0] = pos[m_firstPtReceivedBuff];

			//- memorize the last value of buf (will be used for the next buf)
			m_lastReceivedValue = buf[nb_to_copy - 1];

			m_currentBufferNb++;
			m_firstPtReceivedBuff += nb_to_copy;

      // store received buffer in Nexus, if nx storage enabled for this counter
      if (data_to_be_stored && m_storage_mgr)
      {
        try
        {
          m_storage_mgr->pushNexusData(m_cfg.cnt.dataset, &bufNx[0], nb_to_copy);
		  DEBUG_STREAM << "Push data ok for counter: " << m_cfg.cnt.name << endl;
        }
        catch (Tango::DevFailed & df)
        {
	        ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caught DevFailed: " << df << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
        catch (...)
        {
	        ERROR_STREAM << "BufferedCounterPos::handle_scaled_buffer-> pushNexusData caugth unknown exception!" << std::endl;
          // We should stop acquisition if nx problem!
          m_storageError = true;
          stopCnt();
        }
      }
		}

		// if all requested buffers are received 
		if (m_currentBufferNb == m_bufferNbToReceive)
		{	
      // Stop acquisition
			stopCnt();
		}
	}

  delete buffer;
}

// ============================================================================
// BufferedCounterPos::startCnt
// ============================================================================
void BufferedCounterPos::startCnt()
{
  DEBUG_STREAM << "BufferedCounterPos::startCnt() entering..." << endl;
  m_cntOverrun = false;
  m_cntTimedout = false;
  m_storageError = false;

	{
		yat::AutoMutex<> guard(m_buffLock);

    // reset current counters & buffers
		m_currentBufferNb = 0;
		m_acquisitionDone = false;
		m_firstPtReceivedBuff = 0;
		m_lastReceivedValue = 0;

    // check if infinite mode
    if (m_cfg.acq.samplesNumber == 0)
    {
      // set data length & capacity to buffer depth
	    m_data.capacity(m_cfg.acq.bufferDepth);
	    m_data.force_length(m_cfg.acq.bufferDepth);
    }
    else
    {
      // check if continuous mode
      if (m_cfg.acq.continuousAcquisition)
      {
        // set data length & capacity to samples nber
	      m_data.capacity(m_cfg.acq.samplesNumber);
	      m_data.force_length(m_cfg.acq.samplesNumber);
      }
      else
      {
        // set data capacity & length to 0
	      m_data.capacity(0);
	      m_data.force_length(0);
      }
    }
		m_data.fill(yat::IEEE_NAN);

		unsigned long buffer_depth_nbpts = 0;

		if (m_cfg.acq.continuousAcquisition)
		{
			buffer_depth_nbpts = m_cfg.acq.samplesNumber;
		}
		else
		{
			buffer_depth_nbpts = static_cast<unsigned long>(m_cfg.acq.bufferDepth);
		}

		m_bufferNbToReceive = (m_cfg.acq.samplesNumber + buffer_depth_nbpts - 1) /buffer_depth_nbpts;
		DEBUG_STREAM << "Buffer to receive : " << m_bufferNbToReceive << endl;
	}

	//- from NI660Xsl
	try
	{
		ni660Xsl::BufferedPositionMeasurement::start();
	}
	catch(ni660Xsl::DAQException & nie)
	{
		throw_devfailed(nie);
	}

#if !defined (USE_CALLBACK) 
	// start the data buffer waiting task for the 1st time
	updateScaledBuffer();
#endif
}

// ============================================================================
// BufferedCounterPos::stopCnt
// ============================================================================
void BufferedCounterPos::stopCnt()
{
	DEBUG_STREAM << "BufferedCounterPos::stopCnt() entering..." << endl;
	m_acquisitionDone = true;

	try
	{
		//- from NI660Xsl
    // we use abort to stop counting immediately
    // sets an UNKNOWN state in NI660Xsl lib !
    ni660Xsl::BufferedPositionMeasurement::abort_and_release();
	}
  catch(ni660Xsl::DAQException & nie)
  {
    if (nie.errors[0].code > 0)
    {
      WARN_STREAM << "BufferedCounterPos::stopCnt() - Trying to abort during buffer acquisition, retry once..." << std::endl;
	    ni660Xsl::BufferedPositionMeasurement::abort_and_release();
    }
    else
    {
      // Bug in DAQmx 9.x: Aborting counter generates exception 
      // An effective workaround is to catch and clear the error 
    }
  }
  catch(...)
	{
      // One more time:
      // Bug in DAQmx 9.x: Aborting counter generates exception 
      // An effective workaround is to catch and clear the error 
	}
}

// ============================================================================
// BufferedCounterPos::get_value ()
// ============================================================================ 
RawData_t & BufferedCounterPos::get_value()
{
  yat::AutoMutex<> guard(m_buffLock);
	return m_data;
}

// ============================================================================
// BufferedCounterPos::updateLastScaledBuffer
// ============================================================================
void BufferedCounterPos::updateLastScaledBuffer()
{
  // call "get last scaled buffer" to compose the whole buffer.
  // With trigger listener option, the device has to force the reading of the last
  // incomplete buffer to get the final data.
  DEBUG_STREAM << "BufferedCounterPos::updateLastScaledBuffer ..." << endl;

  // wait a little to be sure that the driver has received the last point
  yat::Thread::sleep(kLAST_BUFFER_DELAY_MS);

  //- from NI660Xsl - calls ni660Xsl::handle_xxx methods
  try
  {
    unsigned long expected_samples_nb = (unsigned long)m_cfg.acq.samplesNumber - m_firstPtReceivedBuff;
	ni660Xsl::BufferedPositionMeasurement::get_last_scaled_buffer(expected_samples_nb);
  }
  catch(ni660Xsl::DAQException & nie)
  {
    throw_devfailed(nie);
  }
}


//*****************************************************************************
// BRPThread
//*****************************************************************************
// ============================================================================
// BRPThread::BRPThread
// ============================================================================
BRPThread::BRPThread (Tango::DeviceImpl * hostDevice, yat::Thread::IOArg ioa)
: yat::Thread(ioa),
Tango::LogAdapter (hostDevice),
m_goOn(true),
m_isUpdateDone(false)
{
	//- noop ctor
}

// ============================================================================
// BRPThread::~BRPThread
// ============================================================================
BRPThread::~BRPThread (void)
{
	//- noop dtor
}

// ============================================================================
// BRPThread::run_undetached
// ============================================================================
yat::Thread::IOArg BRPThread::run_undetached (yat::Thread::IOArg ioa)
{
	DEBUG_STREAM << "BRPThread::run_undetached() entering... " << std::endl;

	m_isUpdateDone = false;
	//- get ref. to out our parent task
	BufferedCounterPos * cm_task = reinterpret_cast<BufferedCounterPos *>(ioa);

	//std::cout << "**Wait for buffer value to be set from board..." << std::endl;
	cm_task->getScaledBuffer(); 

	// when function returns, the buffer is handled (or timeout if no data)
	//std::cout << "**Buffer update done." << std::endl;
	m_isUpdateDone = true;

	return 0;
}

// ============================================================================
// BRPThread::exit
// ============================================================================
void BRPThread::exit (void)
{
	m_goOn = false;
}

} // namespace PulseCounting_ns

