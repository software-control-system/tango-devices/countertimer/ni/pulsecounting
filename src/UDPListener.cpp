// ============================================================================
// = CONTEXT
//		Flyscan Project - HW to SW Trigger Service
// = File
//		UDPListener.h
// = AUTHOR
//    N.Leclercq - SOLEIL
// ============================================================================

#include <yat/utils/StringTokenizer.h>
#include "UDPListener.h"

namespace udplistener
{
  
//=============================================================================
// UDPListener::UDPListener
//=============================================================================
UDPListener::UDPListener (const UDPListener::Config & cfg)
    : Thread (),
      m_mode(UDP_STANDBY),
      m_go_on(true),
      m_received_events(0),
      m_expected_events(0),
      m_ignored_events(0),
      m_total_ignored_events(0),
      m_total_events(0),
      m_cfg(cfg)
{
  //- noop
}

//=============================================================================
// UDPListener::~UDPListener
//=============================================================================
UDPListener::~UDPListener ()
{
  //- noop
}

//=============================================================================
// UDPListener::start
//=============================================================================
void UDPListener::start (yat::uint32 n)
{
  m_received_events = 0;
  m_ignored_events = 0;
  m_total_events = 0;
  m_expected_events = n;
  m_mode = n ? UDP_FINITE : UDP_INFINITE;
}

//=============================================================================
// UDPListener::stop
//=============================================================================
void UDPListener::stop ()
{
  m_mode = UDP_STANDBY;
}

//=============================================================================
// UDPListener::run_undetached
//=============================================================================
void UDPListener::exit ()
{  
  m_go_on = false;
  yat::Thread::IOArg tmp = 0;
  join(&tmp);
}

//=============================================================================
// UDPListener::setup_udp_socket
//=============================================================================
void UDPListener::setup_udp_socket(yat::ClientSocket& sock)
{  
  yat::StringTokenizer st(m_cfg.udp_addr, ".");

  if ( st.count_tokens() != 4 )
  {
    yat::OSStream oss;
    oss << "invalid UDP address specified: " << m_cfg.udp_addr;
    THROW_DEVFAILED("INVALID_ARGUMENT", oss.str().c_str(), "UDPListener::send_udp_info_i");
  }
  //- first byte of the udp addr
  yat::uint8 b = static_cast<yat::uint8>(st.next_long_token());

  //- addresses in the range 224.0.0.1 to 239.255.255.255 identifies a multicast group
  bool multicast = (b >= 224 && b <= 239 )
                 ? true
                 : false;

  //- unicast case...
  if ( ! multicast )
  {
    //-  just bind to udp port, then return
    sock.bind(m_cfg.udp_port);
    return;
  }
  
  //- multicast case...
  
  //- enable SOCK_OPT_REUSE_ADDRESS to allow multiple instances of this 
  //- device to receive copies of the multicast datagrams
  sock.set_option(yat::Socket::SOCK_OPT_REUSE_ADDRESS, 1);
    
  //- bind to the udp port
  sock.bind(m_cfg.udp_port);
  
  //- join the multicast group
  yat::Address multicast_grp_addr(m_cfg.udp_addr, 0);
  sock.join_multicast_group(multicast_grp_addr);
}

//=============================================================================
// UDPListener::run_undetached
//=============================================================================
yat::Thread::IOArg UDPListener::run_undetached (yat::Thread::IOArg)
{
  m_go_on = true;
  
  //- instanciate the udp socket
  yat::ClientSocket sock(yat::Socket::UDP_PROTOCOL);
    
  //- setup our udp socket
  setup_udp_socket(sock);

  //- input data buffer
  yat::Socket::Data ib(4);
  
  //- (almost) infinite reading loop     
  while ( m_go_on )
  {
    //- wait for some input data
    if ( sock.wait_input_data(m_cfg.udp_tmo_ms, false) )
    {
      //- read input data
      yat::uint32  rb = sock.receive_from(ib);
      if ( rb )
      {
        //- ignore UDP events if in STANDBY mode
        if ( m_mode == UDP_STANDBY )
        {
          ++m_ignored_events;
          ++m_total_ignored_events;
          continue;
        }
        //- post data to the SpiTask 
        if ( m_cfg.task_to_notify )
        {
          //- extract UDP event number (identifier) from the UDP packet
          //- this is set by SpiUdpTimebase (i.e. the UDP event emitter)
          yat::uint32 udp_evt_number = *(reinterpret_cast<yat::uint32*>(ib.base()));
          {
            yat::AutoMutex<> guard(m_evtsLock);
            ++m_total_events;
          }

          //- post UDP notification to the 'task_to_notify'?
          if ( m_cfg.uer_notification_msg_id > UDP_LISTENER_NOTIF_DISABLED )
          {
            //- post a 'uer_notification_msg_id' msg to the 'task_to_notify'
            m_cfg.task_to_notify->post(m_cfg.uer_notification_msg_id, udp_evt_number, 500);
          }
          //- end of sequence...
          if ( (m_mode == UDP_FINITE) && (udp_evt_number == (m_expected_events + 1)) )
          {
            //- done, post a 'end of sequence' to the 'task_to_notify'
            if ( m_cfg.eos_notification_msg_id > UDP_LISTENER_NOTIF_DISABLED )
            {
              m_cfg.task_to_notify->post(m_cfg.eos_notification_msg_id, 500);
            }
            //- done, swicth to STANDBY mode
            m_mode = UDP_STANDBY;
          }
        }
      }
    }
  }

  return 0;
}

} //- namespace as

