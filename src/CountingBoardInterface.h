//=============================================================================
// CountingBoardInterface.h
//=============================================================================
// abstraction.......CountingBoardInterface for CountingManager
// class.............CountingBoardInterface
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COUNTING_BOARD_INTERFACE_H
#define _COUNTING_BOARD_INTERFACE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/LogHelper.h>
#include "PulseCountingTypesAndConsts.h"
#include <yat4tango/DynamicAttribute.h>
#include <yat4tango/DynamicAttributeManager.h>
#include <omnithread.h>

#include "NexusManager.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: CountingBoardInterface
// ============================================================================
class CountingBoardInterface : public yat4tango::TangoLogAdapter
{

public:

  //- constructor
  CountingBoardInterface (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t acqMode,
    NexusManager * storage): yat4tango::TangoLogAdapter(hostDevice){
      m_acq_mode = acqMode;
      m_device_host = hostDevice;
      m_storage = storage;
  };

  //- destructor
  virtual ~CountingBoardInterface (){};

  //- init counter
  virtual void initCounter(CounterConfig p_cfg)  = 0;

  //- init clock
  virtual void initClock(ClockConfig p_cfg)  = 0;

  //- configure counter
  virtual void configureCounter(CounterConfig p_cfg) = 0;

  //- start counter
  virtual void startCounter(CounterConfig p_cfg) = 0;

  //- stop counter
  virtual void stopCounter(CounterConfig p_cfg) = 0;

  //- start clock
  virtual void startClock() = 0;

  //- abort clock
  virtual void abortClock() = 0;

  //- wait end of clock
  virtual void waitEndOfClock() = 0;

  //- delete counter object
  virtual void releaseCounter(CounterConfig p_cfg) = 0;

  //- delete clock object
  virtual void releaseClock() = 0;

  //- getDriverVersion
  virtual std::string getDriverVersion() = 0;

  //- get max counter number
  virtual unsigned int getMaxCounterNumber() = 0;

  //- set integration time
  virtual void setIntegrationTime(double p_time) = 0;

  //- set buffer depth
  virtual void setBufferDepth(long p_size) = 0;

  //- set samples number
  virtual void setSamplesNumber(unsigned int p_number) = 0;

  //- set continuous mode
  virtual void setContinuousMode(bool p_continuous) = 0;

  //- set acquisition timeout
  virtual void setAcquisitionTimeout(double p_time) = 0;

  //- set start trigger use
  virtual void setStartTriggerUse(bool p_use) = 0;

  //- set nexus storage use
  virtual void setStorageUse(bool p_use) = 0;

  //- gets state
  virtual Tango::DevState getCounterState(CounterConfig p_cfg) = 0;
  
  //- get counter scalar value
  virtual data_t getCounterScalarValue(CounterConfig p_cfg) = 0;

  //- get counter Buffer value
  virtual RawData_t & getCounterBufferValue(CounterConfig p_cfg) = 0;

  //- update counter Buffer value (used in "polling" mode)
  virtual void updateCounterBufferValue(CounterConfig p_cfg) = 0;
    
  //- update last counter Buffer value (used with trigger listener option)
  virtual void updateCounterLastBufferValue(CounterConfig p_cfg) = 0;

protected:
	// acquisition mode
	E_AcquisitionMode_t m_acq_mode;

	// clock config
	ClockConfig m_clk_cfg;

	// acquisition parameter
	AcquisitionDefinition m_cfg;

	// tango device
	Tango::DeviceImpl * m_device_host;

  // storage service
  NexusManager * m_storage;

};

} // namespace PulseCounting_ns

#endif // _COUNTING_BOARD_INTERFACE_H
