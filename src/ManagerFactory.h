//=============================================================================
// ManagerFactory.h
//=============================================================================
// abstraction.......ManagerFactory
// class.............ManagerFactory
// original author...S.GARA - Nexeya
//=============================================================================

#ifndef _MANAGER_FACTORY_H_
#define _MANAGER_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseCountingManager.h"

namespace PulseCounting_ns
{

// ============================================================================
// class: ManagerFactory
// ============================================================================
class ManagerFactory
{
public: 
  //- instanciate a specialized GenericManagerInterface
  static PulseCountingManager * instanciate (Tango::DeviceImpl * hostDevice, E_AcquisitionMode_t p_mode);
  	
private:
  ManagerFactory ();
  ~ManagerFactory ();
};

} // namespace PulseCounting_ns

#endif // _MANAGER_FACTORY_H_
