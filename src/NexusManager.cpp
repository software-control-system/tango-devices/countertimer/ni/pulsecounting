//=============================================================================
// NexusManager.cpp
//=============================================================================
// abstraction.......Nexus storage manager implementation
// class.............NexusManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "NexusManager.h"

namespace PulseCounting_ns
{

// ======================================================================
// NexusManager::NexusManager
// ======================================================================
NexusManager::NexusManager (Tango::DeviceImpl * host_device)
: Tango::LogAdapter(host_device),
  m_pAcqWriter(NULL),
  m_finalizeDone(true),
  m_finiteStorage(false),
  m_excptOccured(false)
#if defined (USE_NX_DS_FINALIZER)
  ,
  m_NexusDataStreamerFinalizer(NULL)
#endif
{
#if defined (USE_NX_DS_FINALIZER)
  m_NexusDataStreamerFinalizer = new nxcpp::NexusDataStreamerFinalizer();

  if (m_NexusDataStreamerFinalizer)
  {
    try
    {
      m_NexusDataStreamerFinalizer->start();
    }
    catch(...)
    {
      ERROR_STREAM << "NexusManager::NexusManager failed to start nexus finalizer" << std::endl;
      delete m_NexusDataStreamerFinalizer;
      m_NexusDataStreamerFinalizer = NULL;
    }
  }
  else
  {
    ERROR_STREAM << "NexusManager::NexusManager failed to create nexus finalizer" << std::endl;
  }
#endif
}

// ======================================================================
// NexusManager::~NexusManager
// ======================================================================
NexusManager::~NexusManager ()
{
  try
  {
    // delete data buffer if previously allocated
    if (m_pAcqWriter)
    {
      delete m_pAcqWriter;
      m_pAcqWriter = NULL;
    }
  }
  catch (nxcpp::NexusException &n)
  {
    ERROR_STREAM << "NexusManager::~NexusManager caught NexusException: "
      << n.to_string()
      << std::endl;
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::~NexusManager caught [...]!" << std::endl;
  }

#if defined (USE_NX_DS_FINALIZER)
  try
  {
    if (m_NexusDataStreamerFinalizer)
      m_NexusDataStreamerFinalizer->stop();

      delete m_NexusDataStreamerFinalizer;
      m_NexusDataStreamerFinalizer = NULL;
  }
  catch (...)
  {
  }
#endif
}

// ======================================================================
// NexusManager::initNexusAcquisition
// ======================================================================
void NexusManager::initNexusAcquisition(std::string nexus_file_path, 
                                        std::string nexus_file_name,                            
                                        yat::uint32 acquisition_size,
                                        yat::uint16 nx_number_per_file,
                                        nxItemList_t item_list)
{
  m_finalizeDone = false;
  m_excptOccured = false;

  try
  {
    //---- delete data buffer if previously allocated
    if (m_pAcqWriter )
    {
       delete m_pAcqWriter;
       m_pAcqWriter = NULL;
    }
  
    //---- create data buffer for new acquisition
    if (!m_pAcqWriter)
    {    
      //- check nx_number_per_file value
      if (0 == nx_number_per_file)
      {
        ERROR_STREAM << "Number of nx storage per file should not be null!" << std::endl;
        THROW_DEVFAILED("CONFIGURATION_ERROR", 
                        "Number of nx storage per file should not be null!", 
                        "NexusManager::initNexusAcquisition");
      }

      //- finite nx generation file
      if (acquisition_size != 0)
			{
        m_finiteStorage = true;

        //- check that acquisition_size >= m_nxNumberPerFile
        if (acquisition_size >= nx_number_per_file)
        {
          m_pAcqWriter = new nxcpp::DataStreamer(nexus_file_name, acquisition_size, nx_number_per_file);
        }
        else
        {
          ERROR_STREAM << "Number of acquisition must be greater than number of nx storage per file" << std::endl;
          THROW_DEVFAILED("CONFIGURATION_ERROR", 
                          "Nexus file configuration error - see logs for details", 
                          "NexusManager::initNexusAcquisition");
        }
      }
      else //- infinite nx generation file
      {
        m_finiteStorage = false;
        m_pAcqWriter = new nxcpp::DataStreamer(nexus_file_name, nx_number_per_file);			  
      }
    }
    else
    {
      // de-allocation pb ==> exception
      ERROR_STREAM << "Memory problem during nexus buffer de-allocation" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR", 
                      "Memory problem during nexus buffer de-allocation", 
                      "NexusManager::initNexusAcquisition");
    }
  }
  catch(nxcpp::NexusException &n)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition -> caught NexusException:" 
      << n.to_string()
      << std::endl;
    
    if (m_pAcqWriter)
      m_pAcqWriter->Abort();

    ERROR_STREAM << "Nexus storage ABORTED." << std::endl;

    Tango::DevFailed df = nexusToTangoException(n);
    throw df;
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught DevFailed : " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      "DEVICE_ERROR",
                      "Failed to prepare Nexus storage (caught DevFailed)!",
                      "NexusManager::initNexusAcquisition");
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
                    "Failed to prepare Nexus storage (caught [...])!", 
                    "NexusManager::initNexusAcquisition");
  }

  DEBUG_STREAM << "Store Nexus files in: " << nexus_file_path << std::endl;


  //---- initialize nexus acquisition
  m_itemList = item_list;

  try
  {
    if (m_pAcqWriter)
    {
      bool cleanFolders = true;
      m_pAcqWriter->Initialize(nexus_file_path, "");

      // set exception handler
      m_pAcqWriter->SetExceptionHandler(this);

      // parse the list of items to store in nexus file
      for (size_t idx = 0; idx < m_itemList.size(); idx++)
      {
        std::string itemName = m_itemList.at(idx).name;
        E_NxDataStorageType_t storageType = m_itemList.at(idx).storageType;
        int dim1 = m_itemList.at(idx).dim1;
        int dim2 = m_itemList.at(idx).dim2;
        
        switch (storageType)
        {
          case NX_DATA_TYPE_0D:
            m_pAcqWriter->AddDataItem0D(itemName);
            break;
          case NX_DATA_TYPE_1D:
            m_pAcqWriter->AddDataItem1D(itemName, dim1);
            break;
          case NX_DATA_TYPE_2D:
            m_pAcqWriter->AddDataItem2D(itemName, dim1, dim2);
            break;
          default:
            // not supported type => fatal error
            ERROR_STREAM << "Bad data type for nexus storage" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Bad data type for nexus storage", 
                            "NexusManager::initNexusAcquisition");
            break;
        }
      }
    }
    else
    {
      // allocation pb ==> exception
      ERROR_STREAM << "Memory problem during nexus buffer allocation" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR", 
                      "Memory problem during nexus buffer allocation", 
                      "NexusManager::initNexusAcquisition");
    }
  }
  catch(nxcpp::NexusException &n4te)
  {    
    ERROR_STREAM << "NexusManager::initNexusAcquisition -> caught NexusException:"
      << n4te.to_string()
      << std::endl;

    Tango::DevFailed df = nexusToTangoException(n4te);
    throw df;
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught DevFailed : " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      "DEVICE_ERROR",
                      "Failed to initialize Nexus storage (caught DevFailed)!",
                      "NexusManager::initNexusAcquisition");
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
                    "Failed to initialize Nexus storage (caught [...])!", 
                    "NexusManager::initNexusAcquisition");
  }	
}

// ======================================================================
// NexusManager::finalizeNxGeneration
// ======================================================================
void NexusManager::finalizeNexusGeneration()
{
  //- Ends nexus generation file.
  //- This method can be called from two ways
  //- in the same time, and in some cases can
  //- cause a CRASH !!!

  if (m_pAcqWriter)
  {
    if (!m_finalizeDone)
    {
      yat::AutoMutex<> guard(m_finalizeNxLock);

      try
      {
        //- if infinite Nx generation, call Stop before Finalize !!!
        if (!m_finiteStorage)
          m_pAcqWriter->Stop();

        //- in all cases call Finalize
#if defined (USE_NX_DS_FINALIZER)
        if (m_NexusDataStreamerFinalizer) 
        {
          DEBUG_STREAM << "NexusManager::finalizeNexusGeneration -> Passing DataStreamer to the NexusDataStreamerFinalizer" << std::endl;
          nxcpp::NexusDataStreamerFinalizer::Entry * e = new nxcpp::NexusDataStreamerFinalizer::Entry();
          e->data_streamer = m_pAcqWriter;
          m_pAcqWriter = 0;
          m_NexusDataStreamerFinalizer->push(e);
        }
        else
#endif
        {
          DEBUG_STREAM <<"NexusManager::finalizeNexusGeneration -> use DataStreamer::Finalize" << std::endl;
          m_pAcqWriter->Finalize(); 
        }

        //- now Nx file generation is done !
        m_finalizeDone = true;
      }
      catch(nxcpp::NexusException &n)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> Nexus4TangoException caught: "
          << n.to_string()
          << std::endl;

        if (m_pAcqWriter)
          m_pAcqWriter->Abort();
        
        ERROR_STREAM << "Nexus storage ABORTED." << std::endl;
        Tango::DevFailed df = nexusToTangoException(n);
        throw df;
      }
      catch(yat::Exception &n)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> yat::Exception caught: "
          << n.to_string()
          << std::endl;
        manageNexusAbort();
        ERROR_STREAM << "Nexus storage ABORTED." << std::endl;
      }
      catch(...)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> caught [...] Exception" << std::endl;
        manageNexusAbort();
        ERROR_STREAM << "Nexus stotage ABORTED." << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Failed to finilize Nexus storage (caught [...])!", 
                        "NexusManager::finalizeNxGeneration");
      }
    }
  }
}

// ======================================================================
// NexusManager::manageNexusAbort
// ======================================================================
void NexusManager::manageNexusAbort()
{
  if (m_pAcqWriter)
  {
    ERROR_STREAM << "NexusManager::manageNexusAbort <- ..." << std::endl;
    
    m_pAcqWriter->Abort();
    
    m_pAcqWriter->Finalize();

    delete m_pAcqWriter;    
    m_pAcqWriter = 0;

    ERROR_STREAM << "NexusManager::manageNexusAbort ->" << std::endl;
  }
}

// ======================================================================
// NexusManager::nexusToTangoException
// ======================================================================
Tango::DevFailed NexusManager::nexusToTangoException(const nxcpp::NexusException &nxte)
{
  Tango::DevErrorList error_list(nxte.errors.size());
  error_list.length(nxte.errors.size());
	
  for (unsigned int i = 0; i < nxte.errors.size(); i++)
  {
    error_list[i].reason = CORBA::string_dup(nxte.errors[i].reason.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> REASON = " << error_list[i].reason << std::endl;
		
    error_list[i].desc = CORBA::string_dup(nxte.errors[i].desc.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> DESC = " << error_list[i].desc << std::endl;
		
    error_list[i].origin = CORBA::string_dup(nxte.errors[i].origin.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> ORIGIN = " << error_list[i].origin << std::endl;

    error_list[i].severity = Tango::ERR;
  }

  return Tango::DevFailed(error_list);
}


} // namespace PulseCounting_ns


