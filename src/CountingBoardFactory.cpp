//=============================================================================
// CountingBoardFactory.cpp
//=============================================================================
// abstraction.......CountingBoardFactory
// class.............CountingBoardFactory
// original author...S.GARA - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include "CountingBoardFactory.h"
#include "NI6602_Interface.h"

namespace PulseCounting_ns
{

// ======================================================================
// CountingBoardFactory::instanciate
// ======================================================================
CountingBoardInterface * CountingBoardFactory::instanciate (
  Tango::DeviceImpl * hostDevice, 
  E_BoardType_t p_board, 
  E_AcquisitionMode_t p_acqMode,
  NexusManager * p_storage)
{
  CountingBoardInterface * l_board;

  switch (p_board)
  {
  case BT_PXI_6602:
	  l_board = new NI6602_Interface(hostDevice, p_acqMode, p_storage); 
	  break;
  default:
	  l_board = NULL;
	  break;
  }
 
  return l_board;
}

} // namespace PulseCounting_ns


