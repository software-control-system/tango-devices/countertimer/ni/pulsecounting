//=============================================================================
// NI6602TypesAndConsts.h
//=============================================================================
// abstraction.......NI6602 
// class.............NI6602TypesAndConsts
// original author.... S.Gara - NEXEYA
//=============================================================================

#ifndef _NI6602_TYPES_AND_CONSTS_H_
#define _NI6602_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace PulseCounting_ns
{
// ============================================================================
// MAX NUMBER OF COUNTERS FOR NI6602 BOARD
// ============================================================================
const size_t k_NI66602_MAX_NUMBER_OF_COUNTERS   = 8;

//- ni660Xsl::Exception -> Tango::DevFailed
static void throw_devfailed( ni660Xsl::DAQException& daq_ex )
{
	Tango::DevFailed df;
	const ni660Xsl::ErrorList& daq_errors = daq_ex.errors;
	df.errors.length( daq_errors.size() );
	for (size_t i = 0; i < daq_errors.size(); i++)
	{
		df.errors[i].reason   = CORBA::string_dup( daq_errors[i].reason.c_str() );
		df.errors[i].desc     = CORBA::string_dup( daq_errors[i].desc.c_str()   );
		df.errors[i].origin   = CORBA::string_dup( daq_errors[i].origin.c_str() );
		df.errors[i].severity = static_cast<Tango::ErrSeverity>(daq_errors[i].severity);
	}
	throw df;
}
} // namespace PulseCounting_ns

#endif // _NI6602_TYPES_AND_CONSTS_H_