//=============================================================================
// PulseCountingManagerBuffered.h
//=============================================================================
// abstraction.......PulseCountingManagerBuffered for PulseCounting
// class.............PulseCountingManagerBuffered
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _PULSE_COUNTING_MANAGER_BUFFERED_H
#define _PULSE_COUNTING_MANAGER_BUFFERED_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseCountingManager.h"
#include "UDPListener.h"

// consts
#define kTIMEOUT_MSG_PERIOD_MS 1000 // timeout msg period in ms

namespace PulseCounting_ns
{

// ============================================================================
// class: PulseCountingManagerBuffered
// ============================================================================
	class PulseCountingManagerBuffered : public PulseCountingManager
{

public:

  //- constructor
  PulseCountingManagerBuffered (Tango::DeviceImpl * hostDevice);

  //- destructor
  virtual ~PulseCountingManagerBuffered ();

  //- init
  void init(BoardArchitecture p_board_arch);

  //- Checks current acquisition configuration
  std::string check_configuration(bool excpt);

protected:

	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg);

	//- read callback for attr counterX
	void read_counter(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- read callback for attr bufferDepth
	void read_buffer_depth(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr bufferDepth
	void write_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData & cbd);
	//- read callback for attr sampleNumber
	void read_samples_number(yat4tango::DynamicAttributeReadCallbackData & cbd);
	//- write callback for attr bufferDepth
	void write_samples_number(yat4tango::DynamicAttributeWriteCallbackData & cbd);
	//- read callback for attr currentPoint
	void read_current_point(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- manage dynamics attribute
	void manage_specific_dyn_attr ();

	//- start
	void start_i ();

	//- stop
	void stop_i ();

	//- initialization
	void init_i ();

  //- periodic job
  void periodic_job_i ();

  //- get last incomplete buffer (for trigger listener option)
  void get_last_buffer_i();

  //- release internal objects (clock & counters)
  void releaseObjects();

  //- Configure trigger listener (for external clock)
  //- use address & port specified in parameters
  void configure_trigger_listener(std::string addr, yat::uint32 port);

  // map for counters value storage (common callback function)
  //- key = counter id in device (0 -> N), value = buffered counter value
  std::map<unsigned int, RawData_t> m_countersVal;

private:
  // UDP event listener (to track trigger signals)
  udplistener::UDPListener * m_trigListener;

  // current trigger number
  yat::uint32 m_currentTrigg;

};

} // namespace PulseCounting_ns

#endif // _PULSE_COUNTING_MANAGER_BUFFERED_H
