from conan import ConanFile

class PulseCountingRecipe(ConanFile):
    name = "pulsecounting"
    executable = "ds_PulseCounting"
    version = "2.1.5"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "<Put your name here> <And your email here>"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/countertimer/ni/pulsecounting.git"
    description = "PulseCounting device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("nidaqmx/[>=1.0]@soleil/stable")
        self.requires("ni660xsl/[>=1.0]@soleil/stable")
        self.requires("ace/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("nexuscpp/[>=3 <4]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
